#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
// #include <Vector>


using namespace ns3;

NodeContainer c;

// static void
// CourseChange (std::string foo, Ptr<const MobilityModel> mobility)
// {
//   Vector pos = mobility->GetPosition ();
//   Vector vel = mobility->GetVelocity ();
//   std::cout << Simulator::Now () << ", model=" << mobility << ", POS: x=" << pos.x << ", y=" << pos.y
//             << ", z=" << pos.z << "; VEL:" << vel.x << ", y=" << vel.y
//             << ", z=" << vel.z << std::endl;
// }

void changeState() {
  // const Vector<double> position(1, 2);
  // const Vector<double> speed(100, 200);

  const Vector pos(
    100 * Simulator::Now().GetSeconds(),
    200 * Simulator::Now().GetSeconds(),
    300 * Simulator::Now().GetSeconds()
  );
  const Vector s(
    100 * Simulator::Now().GetSeconds(),
    200,
    300
  );
  // s.x = 100 * Simulator::Now().GetSeconds();
  // s.y = 200 * Simulator::Now().GetSeconds();
  // s.z = 300 * Simulator::Now().GetSeconds();
  //
  // s.x = 100;
  // s.y = 200;
  // s.z = 300;

  // pos.x = 1 * Simulator::Now();
  // pos.y = 2 * Simulator::Now();
  // pos.z = 3 * Simulator::Now();

  std::cout << "speed: " << c.Get(1)->GetObject<MobilityModel>()->GetVelocity().x << std::endl;
  std::cout << "real position: " << pos << std::endl;
  std::cout << "position: " << c.Get(1)->GetObject<MobilityModel>()->GetPosition().x << std::endl;
  // std::cout << Simulator::Now().GetSeconds() << std::endl;
  c.Get(1)->GetObject<MobilityModel>()->SetPosition( pos );
  c.Get(1)->GetObject<ConstantVelocityMobilityModel>()->SetVelocity( s );
  // c.Get(1)->GetObject<MobilityHelper>();

  Simulator::Schedule(Seconds (1), &changeState);
}

int main (int argc, char *argv[])
{
  Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Mode", StringValue ("Time"));
  Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Time", StringValue ("2s"));
  // Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Speed", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"));
  Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Bounds", StringValue ("0|200|0|200"));

  CommandLine cmd;
  cmd.Parse (argc, argv);

  c.Create (100);

  MobilityHelper mobility;
  // mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
  //                                "X", StringValue ("100.0"),
  //                                "Y", StringValue ("100.0"),
  //                                "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=30]"));
  // mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
  //                            "Mode", StringValue ("Time"),
  //                            "Time", StringValue ("2s"),
  //                            "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"),
  //                            "Bounds", StringValue ("0|200|0|200"));

  mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");


  mobility.InstallAll ();
  // Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange", MakeCallback (&CourseChange));

  Simulator::Stop (Seconds (100.0));
  Simulator::Schedule(Seconds (1), &changeState);
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
