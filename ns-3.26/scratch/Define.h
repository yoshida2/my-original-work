// ns3 -----
#define NUM_OF_NODES 10
#define SIMULATION_TIME 100
#define SYNC_INTERVAL 1



// wifi -----
std::string m_lossModelName = "ns3::TwoRayGroundPropagationLossModel";
std::string m_phyMode = "OfdmRate6MbpsBW10MHz";
double freq = 5.9e9;
#define TXPOWER 20
#define TRANSMISSION_RANGE 500
#define BEACON_INTERVAL 1.0

// socket -----
#define PORT 8080
#define BUFFER_SIZE 1024

// socket for Ns3 -----
#define SEND_PORT 8000
#define RECV_PORT 8001
#define TRANSMISSION_RATE 0.1

// api command -----
std::string HELLO = "Hello from client";
std::string NEXT_STEP = "next step";
std::string LOADED_VEHICLES = "loaded vehicles";
std::string GET_VEHICLE_SPEED = "get vehicle speed";
std::string GET_VEHICLE_ACCELE = "get vehicle accele";
std::string GET_VEHICLE_POSITION = "get vehicle position";
std::string UPDATE_VEHICLE_SPEED = "update vehicle speed";

std::string NO_EXISTANCE = "NO_EXISTANCE";
std::string NO_LOADED_VEHICLE = "NO_LOADED_VEHICLE";
