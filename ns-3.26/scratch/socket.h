#include "common_header.h"

class Server {
public:
  // int fd;
  // int fd2;
  // int async_time;
  // // struct servent *serv;
  // struct sockaddr_in addr;
  // socklen_t len = sizeof(struct sockaddr_in);
  // struct sockaddr_in from_addr;

  int server_fd, new_socket, valread;
  struct sockaddr_in address;
  int opt = 1;
  int addrlen = sizeof(address);
  char buffer[1024] = {0};
  char hello[2048] = "Hello from server";

  int SetConnection(int recv_port);
  int WaitMessage();
  int CloseConnection();
};
