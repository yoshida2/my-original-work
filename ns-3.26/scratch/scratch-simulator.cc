/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3_header.h"
#include "common_header.h"
#include "socket.c"
#include "client.c"

#define PORT 8080

using namespace ns3;
#define SIMULATION_STOP_TIME 100000000
#define SEND_PORT 50000

Server s;
Client c;

int sock = 0, valread;
char hello[2048] = "Hello from client";
char buffer[1024] = {0};

NS_LOG_COMPONENT_DEFINE ("ScratchSimulator");


void SyncSumo () {
  std::cout << "\n" << std::endl;
  std::cout << Simulator::Now().GetSeconds() << std::endl;
  // if (c.IsConnected() == false) {
  //   c.Connection();
  // }

  send(sock , hello , strlen(hello) , 0 );
  printf("Hello message sent\n");
  valread = read( sock , buffer, 1024);
  printf("%s\n",buffer );

  // c.SendMessage();
  Simulator::Schedule (Seconds (1.0), &SyncSumo);
}

int
main (int argc, char *argv[])
{
  // NS_LOG_UNCOND ("Scratch Simulator");
  // c.SetConnection(SEND_PORT);

  // struct sockaddr_in address;
  // int sock = 0, valread;
  struct sockaddr_in serv_addr;
  // char hello[2048] = "Hello from client";
  // char buffer[1024] = {0};
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
      printf("\n Socket creation error \n");
      return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  struct hostent *info;
  info = gethostbyname("localhost");
  if ( !info ){
      fprintf( stdout, "gethostbyname error\n" );
      std::cout << "gethostbyname error\n" << std::endl;
      return -1;
  }
  std::cout << info << std::endl;

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);
  serv_addr.sin_addr = *(struct in_addr *)(info->h_addr_list[0]);

  // // Convert IPv4 and IPv6 addresses from text to binary form
  // if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
  // {
  //     printf("\nInvalid address/ Address not supported \n");
  //     return -1;
  // }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
      printf("\nConnection Failed \n");
      return -1;
  }

  Simulator::Schedule (Seconds (1.0), &SyncSumo);
  Simulator::Stop (Seconds (SIMULATION_STOP_TIME));
  Simulator::Run ();
  Simulator::Destroy ();

  // s.CloseConnection();
  c.CloseConnection();
}
