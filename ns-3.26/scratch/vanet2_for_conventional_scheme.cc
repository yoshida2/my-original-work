 /* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 North Carolina State University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Scott E. Carpenter <scarpen@ncsu.edu>
 *
 */

/*
 * This example program allows one to run vehicular ad hoc
 * network (VANET) simulation scenarios in ns-3 to assess
 * performance by evaluating different 802.11p MAC/PHY
 * characteristics, propagation loss models (e.g. Friss,
 * Two-Ray Ground, or ITU R-P.1411), and application traffic
 * (e.g. Basic Safety Message) and/or routing traffic (e.g.
 * DSDV, AODV, OLSR, or DSR) under either a synthetic highway
 * scenario (i.e. a random waypoint mobility model) or by
 * playing back mobility trace files (i.e. ns-2 movement files).
 *
 * The script draws from several ns-3 examples, including:
 * /examples/routing/manet-routing-compare.cc
 * /src/propagation/model/itu-r-1411-los-propagation-loss-model.cc
 * /src/mobility/examples/ns2-mobility-trace.cc
 * /src/wave/examples/wave-simple-80211p.cc
 *
 * The script allows many parameters to be modified and
 * includes two predefined scenarios.  By default
 * scenario=1 runs for 10 simulated seconds with 40 nodes
 * (i.e. vehicles) moving according to RandomWaypointMobilityModel
 * with a speed of 20 m/s and no pause time within a 300x1500 m
 * region.  The WiFi is 802.11p with continuous access to a 10 MHz
 * Control Channel (CH) for all traffic.  All nodes transmit a
 * 200-byte safety message 10 times per second at 6 Mbps.
 * Additionally, all nodes (optionally) attempt to
 * continuously route 64-byte packets at an application
 * rate of 2.048 Kbps to one of 10 other nodes,
 * selected as sink nodes. The default routing protocol is AODV
 * and the Two-Ray Ground loss model is used.
 * The transmit power is set to 20 dBm and the transmission range
 * for safety message packet delivery is 145 m.
 *
 * Scenario 2 plays back vehicular trace files in
 * ns-2 movement format, and are taken from:
 * http://www.lst.inf.ethz.ch/research/ad-hoc/car-traces/
 * This scenario is 300 simulation seconds of 99
 * vehicles respectively within the Unterstrass
 * section of Zurich Switzerland that travel based on
 * models derived from real traffic data.  Note that these
 * scenarios can require a lot of clock time to complete.
 *
 * All parameters can be changed from their defaults (see
 * --help) and changing simulation parameters can have dramatic
 * impact on network performance.
 *
 * Several items can be output:
 * - a CSV file of data reception statistics, output once per
 *   second
 * - final statistics, in a CSV file
 * - dump of routing tables at 5 seconds into the simulation
 * - ASCII trace file
 * - PCAP trace files for each node
 *
 * Simulation scenarios can be defined and configuration
 * settings can be saved using config-store (raw text)
 * which can they be replayed again.  This is an easy way
 * to define and save the settings for a scenario, and then
 * re-execute the same scenario exactly, or to set up
 * several different simulation scenarios.
 * For example, to set up a scenario and save the configuration
 * as "scenario1.txt":
 *   ./waf --run "vanet-routing-compare --scenario=1 --saveconfig=scenario1.txt"
 * Then, to re-play the scenario using the save configuration
 * settings:
 *   ./waf --run "vanet-routing-compare --loadconfig=scenario1.txt"
 *
 * Class Diagram:
 *   main()
 *     +--uses-- VanetRoutingExperiment
 *                 +--is_a--- WifiApp
 *                 +--uses--- ConfigStoreHelper
 *                 +--has_a-- WaveBsmHelper
 *                 |            +--has_a-- WaveBsmStats
 *                 +--has_a-- RoutingHelper
 *                 |            +--has_a--RoutingStats
 *                 +--has_a-- WifiPhyStats
 *
 */

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>
#include <unordered_map>
#include <sstream>
#include <vector>
#include <cmath>
#include <map>
#include <functional>
#include <limits>
#include <algorithm>
#include "ns3/netanim-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/aodv-module.h"
#include "ns3/olsr-module.h"
#include "ns3/dsdv-module.h"
#include "ns3/dsr-module.h"
#include "ns3/applications-module.h"
#include "ns3/itu-r-1411-los-propagation-loss-model.h"
#include "ns3/ocb-wifi-mac.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/config-store-module.h"
#include "ns3/integer.h"
#include "ns3/wave-bsm-helper.h"
#include "ns3/wave-helper.h"
// future #include "ns3/topology.h"

using namespace ns3;
using namespace dsr;

#define ONLY_OBSEREVED_NODE_HAVE_TO_BE_COLLECTED 0

#define EXECUTION_MODE 1//0: 従来手法 1:提案手法 2: ただ取得 3: 従来手法の一つ前
#define ACCIDENT_MODE 0 //0: 無事故　1:事故　トレーズファイルを変更する
#define ATTACK_PATTERN 0
/*
0: normal
1: intelligent
2: randam
3: 事故時
4: 閾値を超えない攻撃
5: 良性車両を陥れる攻撃
*/

double  RELATIVE_SPEED_THRESHOLD = 15;
double  THRESHOLD_NUM = 7;
double  MALLICIAS_RATE = 20; //リシャスノードの割合(0 ~ 100)
#define ATTACKERS_PRIORITY 1 /* ０:フロウ優先，１：車両密度優先 ２：スピード優先*/
int     FLOW_RATE_OF_MALLICIAS = 90; //マリシャスノードがフローを変える割合(10 ~ 100)
#define DENSITY_RATE_OF_MALLICIOUS 70 //マリシャスノードが密度を変える割合(10 ~ 100)
#define SPEED_RATE_OF_MALLICIOUS 95

#define RATE 1.0 // 0.0 ~ 1.0

#define NUM_OF_NODES 10 // 40 + 100
int     START_TIME = NUM_OF_NODES * 5;
int     START_ATTACK_TIME = START_TIME + 1;
int     long_simulation = (
  ACCIDENT_MODE == 1 ||
  ATTACK_PATTERN == 4 ||
  ATTACK_PATTERN == 5
  ) ? true : false;
int     M_TOTAL_SIM_TIME = START_ATTACK_TIME + (long_simulation ? START_TIME : 10);

int     THRESHOLD_SPEED = 100000;

double  TRANSMISSION_RANGE = 500;
double  COLLISION_AVOIDANCE_RANGE = 100;
#define T_VALUE 3.045 //a = 0.01 自由度 7+7-2=12
#define M_MOBILITY 1
#define INTER_ARRIVAL_RATE 1
#define MAX_PACKETSIZE_OF_SPEED 10
#define MAX_PACKETSIZE_OF_FLOW  11
#define MAX_PACKETSIZE_OF_DENSITY 11
#define MAX_PACKETSIZE_OF_FLOW_AVE 11
#define MAX_PACKETSIZE_OF_POSITION 11
#define MAX_PACKETSIZE_OF_HOP_FLAG 1
#define TRANSMISSION_RATE 0.1
#define CONVERT_Ms_KMh 3.6
#define CONVERT_M_KM 0.001
#define TXPOWER 20

int IS_MALICIOUS = 0;
int IS_VICIOUS = 1;
int IS_OBSERVED = 2;

double  ACCELE_THRESHOLD = 100;
double  DENGER_SLOPE = - RELATIVE_SPEED_THRESHOLD / ACCELE_THRESHOLD;
double  THRESHOLD = 500; //RELATIVE_SPEED_THRESHOLD * 250.0 / 4.0;  // RELATIVE_SPEED_THRESHOLD * Kj / 4
int     NODE_OBSERVED_FOR_FLOW_AVE = (int)(NUM_OF_NODES / 2);

unsigned int T_SAMPLES = (ONLY_OBSEREVED_NODE_HAVE_TO_BE_COLLECTED) ? 7 : 7 * NUM_OF_NODES;
#define VALUE_RATE 1.0
#define DENSITY_DIFF 2
int     TRUST_SD_CALC = NUM_OF_NODES; //分散値として信頼できる値（主観的な値）
// int     TRUST_SD_VIECLE = T_SAMPLES; //分散値として信頼できる値（主観的な値）
int     TRUST_SD_VIECLE = NUM_OF_NODES; //分散値として信頼できる値（主観的な値）
NodeContainer global_node_container;
int time_count[NUM_OF_NODES] = {0};

// 出力用のファイルを作成
FILE* make_result();
// 事故時のフロウの変化を記載()
void  make_result_accident();
// 観測データを出力
FILE* print_observed_data(FILE *fp);
// 自分で用意したメモリを食うものはここで空にする
void mem_free ();
// true_positeveとfalse_positeveをハッシュで返す
std::unordered_map<std::string, double> return_hash_true_positive_false_positive();

struct data_front{
  double flow_ave;
  double speed_ave;
  double position;
  int num_node;
};
struct own_data {
  uint32_t id = 0;
  uint32_t id_judger = 0;
  int calc_density_flag = 1;
  double log_time_first = 1;
  double flow_own = 0;
  double flow_recv = 0;
  double speed_ave = 0;
  double density_calc = 0;
  double Vf = 25.29093517;
  double Kj = 101.5159708;
  double t = 0;
  double sq2_viecle = 0;
  double sq2_calc = 0;
  std::vector<double> speed_past;
  std::vector<double> density_past;
};

// 統計用に車のデータ構造を定義
struct calc_data {
  double received_time;
  double flow_ave;
  double speed;
};
struct viecle_data {
  double received_time;
  double flow_ave;
  double speed;
};
struct observed_node {
  int is_observed_flag;
  uint32_t id;
  Ipv4Address observed_address;
  double flow_ave;
  double speed;
  double received_time;
  std::vector<viecle_data> tmp_v;
  std::vector<calc_data> tmp_c;
};
struct received_message{
  double received_time;
  double position;
  double acceleration;
  double density;
  double speed;
  double flow_ave;
  double flow;
  int    hop_flag = 0; //0:無事故　1:範囲内の事故　2:範囲外の事故
};
struct varification{
  int    varificationer_id;
  double received_time;
  double effective_time;
  int    varification_flag;
};

struct observed_node_chain{
  int is_observed_flag;
  uint32_t id;
  uint32_t itr_sequence;
  struct received_message observed_node;
  std::vector<uint32_t> pos_sequence;
  std::unordered_map<uint32_t, received_message> received_message_at_accident;
};
struct observed_flow_ave{
  double time;
  double flow_ave;
};
struct low_flow_ave_node{
  uint32_t sender_id;
  double   sender_position;
  double   sender_speed;
};
struct own_data my_data[NUM_OF_NODES];
std::vector<observed_node> observed_nodes[NUM_OF_NODES];
std::vector<observed_flow_ave> observed_flow_aves;

// 観測用データフロントを保存
std::vector<struct data_front> tmp[NUM_OF_NODES];
std::vector<uint32_t> tmp_f[NUM_OF_NODES];
std::vector<uint32_t> tmp_b[NUM_OF_NODES];

// 第一引数に自分のid, 第二引数に送信者のidを入れて管理, 自分のidには自分の情報を送信時に格納
std::unordered_map<uint32_t, received_message> own_received_messages[NUM_OF_NODES];
std::vector<observed_node_chain> observed_node_chains[NUM_OF_NODES];
std::unordered_map<uint32_t, varification> varifications[NUM_OF_NODES];
std::unordered_map<uint32_t, low_flow_ave_node> low_flow_nodes[NUM_OF_NODES];

// 実際の悪性ノードと出力の悪性ノード
std::unordered_map<std::string, int> real_mallicias;
std::unordered_map<std::string, int> result_mallicias;
// 種々の値決定に使用
std::unordered_map<std::string, double> access_log[NUM_OF_NODES];
// INTER_ARRIVAL_RATEから密度を計算するために使用
std::unordered_map<std::string, double> access_log_for_IAR[NUM_OF_NODES];
std::unordered_map<std::string, double> access_speed_log[NUM_OF_NODES];
std::unordered_map<std::string, double> access_flow_log[NUM_OF_NODES];
std::unordered_map<std::string, double> access_density_log[NUM_OF_NODES];
// アドレスとノードIDの対応
std::unordered_map<std::string, uint32_t> id_from_Ipv4adress;
double now_in_simulation = 0;

NS_LOG_COMPONENT_DEFINE ("vanet-routing-compare");

std::unordered_map<int, int> flow_ave_frequency;
std::unordered_map<int, int> flow_own_frequency;
void save_frequency(double flow, std::unordered_map<int, int> &frequency);
void make_frequency_csv();

std::unordered_map<std::string, double> return_hash_true_positive_false_positive(){
  std::string true_positive = "true_positive";
  std::string false_positive = "false_positive";
  std::string accuracy = "accuracy";
  std::unordered_map<std::string, double> result;
  int num_malicias = real_mallicias.size();
  int num_normal = NUM_OF_NODES - num_malicias;
  int num_tp = 0;
  int num_fp = 0;

  for(auto itr = id_from_Ipv4adress.begin(); itr != id_from_Ipv4adress.end(); ++itr){
    if((real_mallicias.find(itr->first) != real_mallicias.end()) && (result_mallicias.find(itr->first) != result_mallicias.end())){
      num_tp++;
    } else if ((real_mallicias.find(itr->first) == real_mallicias.end()) && (result_mallicias.find(itr->first) != result_mallicias.end()))
      num_fp++;
  }

  if(num_malicias == 0){
    result[true_positive] = 0;
  } else {
    result[true_positive] = (double)num_tp / (double)num_malicias * 100;
  }

  if(num_normal == 0){
    result[false_positive] = 0;
  } else {
    result[false_positive] = (double)num_fp / (double)num_normal * 100;
  }

  if ((num_malicias + num_normal) == 0) {
    result[accuracy] = 0;
  } else {
    result[accuracy] = (double)(num_tp + (num_normal - num_fp)) / (double)(num_malicias + num_normal) * 100;
  }

  return result;
}
FILE* print_observed_data(FILE *fp){
  fprintf(fp, "\n");
  fprintf(fp, "mistakenly judged\n");

  int over255 = 0;
  int pointer = 0;
  // int real_flag = 0;
  // int result_flag = 0;
  std::string address_head = "10.1.";
  std::string string_pointer;
  std::string string_over255;
  std::string address_string;
  std::vector<viecle_data> tmp_viecle_dates;
  std::vector<calc_data> tmp_calc_dates;

  for(int i = 1; i <= NUM_OF_NODES; i++){
    // 桁の繰上げ
    // real_flag = 0;
    // result_flag = 0;
    pointer = i;
    over255 = 0;
    if(255 < pointer){
      over255++;
      pointer -= 255;
    }
    string_pointer = std::to_string(pointer);
    string_over255 = std::to_string(over255);
    address_string = address_head + string_over255 + "." + string_pointer;
    auto itr = id_from_Ipv4adress.find(address_string);
    uint32_t id_judger = my_data[itr->second].id_judger;

    // if(real_mallicias.find(itr->first) != real_mallicias.end()){
    //   real_flag = 1;
    // }
    // if(result_mallicias.find(itr->first) != result_mallicias.end()){
    //   result_flag = 1;
    // }
    // 誤検知した場合には観測データを出力
    if(/*result_flag == 1*/ true){
      fprintf(fp, "I'm = %s\n", itr->first.c_str());
      for(auto itr_observed = observed_nodes[id_judger].begin(); itr_observed != observed_nodes[id_judger].end(); ++itr_observed){
        if(itr_observed->id == itr->second){
          tmp_viecle_dates = itr_observed->tmp_v;
          tmp_calc_dates = itr_observed->tmp_c;

          fprintf(fp, "viecle_data");
          for(auto itr_vd = tmp_viecle_dates.begin(); itr_vd != tmp_viecle_dates.end(); ++itr_vd) {
            fprintf(fp, ", %d", (int)itr_vd->flow_ave);
            if((itr_vd - tmp_viecle_dates.begin()) == (signed int)(tmp_viecle_dates.size() - 1)){
              break;
            }
          }
          fprintf(fp, "\n");

          fprintf(fp, "calc_data");
          for(auto itr_cd = tmp_calc_dates.begin(); itr_cd != tmp_calc_dates.end(); ++itr_cd) {
            fprintf(fp, ", %d", (int)itr_cd->flow_ave);
            if((itr_cd - tmp_calc_dates.begin()) == (signed int)(tmp_calc_dates.size() - 1)){
              break;
            }
          }
          fprintf(fp, "\n");
          fprintf(fp, "\n");
          break;
        }
      }
    }
  }

  return fp;
}

void save_frequency(double flow, std::unordered_map<int, int> &frequency){

  // 十刻みint
  int tmp = ((int)flow / 10) * 10;
  frequency[tmp] = frequency[tmp] + 1;
}

void make_frequency_csv(){

  char filepath[256];
  char file_name[256];

  sprintf(file_name, "flow_aves_frequency_mallicious_rate_%lf", MALLICIAS_RATE);
  sprintf(filepath, "./result/%s", file_name);
  FILE *fp;
  fp = fopen(filepath, "w");

  fprintf(fp, "flow_ave, times\n");
  // グローバルパラメータ
  fprintf(fp, "\n");
  fprintf(fp, "flow_ave(global)\n");
  for(auto itr = flow_ave_frequency.begin(); itr != flow_ave_frequency.end(); itr++){
    fprintf(fp, "%d, %d\n", itr->first, itr->second);
  }
  // 改行
  fprintf(fp, "\n");
  // ローカルパラメータ
  fprintf(fp, "\n");
  fprintf(fp, "flow_own(local)\n");
  for (auto itr = flow_own_frequency.begin(); itr != flow_own_frequency.end(); itr++){
    fprintf(fp, "%d, %d\n", itr->first, itr->second);
  }

  fclose(fp);
}

void make_result_accident(){

  char filepath[256];
  char file_name[256];

  sprintf(file_name, "flow_aves_time_series_EXECUTION_MODE_%d", EXECUTION_MODE);
  sprintf(filepath, "./result/%s", file_name);
  FILE *fp;
  fp = fopen(filepath, "w");

  fprintf(fp, "time, flow_ave\n");
  for(auto itr = observed_flow_aves.begin(); itr != observed_flow_aves.end(); itr++){
    fprintf(fp, "%lf, %lf\n", itr->time, itr->flow_ave);
  }

  fclose(fp);
}

FILE* make_result (){
  // ラベルをつける
  char filepath[256];
  char file_name[256];
  int real_flag = 0;
  int result_flag = 0;
  int over255 = 0;
  int pointer = 0;
  std::string address_head = "10.1.";
  std::string string_pointer;
  std::string string_over255;
  std::string address_string;
  std::unordered_map<std::string, double> result = return_hash_true_positive_false_positive();

  sprintf(file_name, "accuracy_%lf_tpr_%lf_fpr_%lf_node_%d_mallicias_rate_%lf_START_TIME_%d_attack_rate_%d.csv", result["accuracy"],
  result["true_positive"], result["false_positive"], NUM_OF_NODES ,MALLICIAS_RATE ,START_TIME, FLOW_RATE_OF_MALLICIAS);
  sprintf(filepath, "./result/%s", file_name);
  FILE *fp;
  fp = fopen(filepath, "w");

  fprintf(fp, "NUM_OF_NODES, %d, MALLICIAS_RATE, %lf\n",NUM_OF_NODES, MALLICIAS_RATE);
  fprintf(fp, "address, real, result, sd_viecle, sd_calc, Vf, Kj, flow_ave, flow_own, t, judger\n");

  for(int i = 1; i <= NUM_OF_NODES; i++){
    // 桁の繰上げ
    pointer = i;
    over255 = 0;
    if(255 < pointer){
      over255++;
      pointer -= 255;
    }
    string_pointer = std::to_string(pointer);
    string_over255 = std::to_string(over255);
    address_string = address_head + string_over255 + "." + string_pointer;
    auto itr = id_from_Ipv4adress.find(address_string);
    real_flag = 0;
    result_flag = 0;

    if(real_mallicias.find(itr->first) != real_mallicias.end()){
      real_flag = 1;
    }
    if(result_mallicias.find(itr->first) != result_mallicias.end()){
      result_flag = 1;
    }
    fprintf(fp, "%s, %d, %d, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %d\n",
        itr->first.c_str(),
        real_flag,
        result_flag,
        my_data[itr->second].sq2_viecle,
        my_data[itr->second].sq2_calc,
        my_data[itr->second].Vf,
        my_data[itr->second].Kj,
        my_data[itr->second].flow_recv,
        my_data[itr->second].flow_own,
        my_data[itr->second].t,
        my_data[itr->second].id_judger
    );
  }
  fprintf(fp, "\n");
  if(EXECUTION_MODE == 0 || EXECUTION_MODE == 3){
    print_observed_data(fp);
  }
  fclose(fp);

  std::cout << " file_name = " << file_name << std::endl;

  return fp;

}

void mem_free (){
  real_mallicias.clear();
  result_mallicias.clear();
  id_from_Ipv4adress.clear();
  for (int i = 0; i < NUM_OF_NODES; i++){

    my_data[i].id = 0;
    my_data[i].calc_density_flag = 1;
    my_data[i].log_time_first = 1;
    my_data[i].flow_own = 0;
    my_data[i].flow_recv = 0;
    my_data[i].speed_ave = 0;
    my_data[i].density_calc = 0;
    my_data[i].Vf = 100;
    my_data[i].Kj = 10;
    my_data[i].speed_past.clear();
    my_data[i].density_past.clear();

    observed_nodes[i].clear();
    access_log[i].clear();
    access_speed_log[i].clear();
    access_flow_log[i].clear();
    access_density_log[i].clear();
  }
}

/**
 * \ingroup wave
 * \brief The RoutingStats class manages collects statistics
 * on routing data (application-data packet and byte counts)
 * for the vehicular network
 */
class RoutingStats
{
public:
  /**
   * \brief Constructor
   * \return none
   */
  RoutingStats ();

  /**
   * \brief Returns the number of bytes received
   * \return the number of bytes received
   */
  uint32_t GetRxBytes ();

  /**
   * \brief Returns the cumulative number of bytes received
   * \return the cumulative number of bytes received
   */
  uint32_t GetCumulativeRxBytes ();

  /**
   * \brief Returns the count of packets received
   * \return the count of packets received
   */
  uint32_t GetRxPkts ();

  /**
   * \brief Returns the cumulative count of packets received
   * \return the cumulative count of packets received
   */
  uint32_t GetCumulativeRxPkts ();

  /**
   * \brief Increments the number of (application-data)
   * bytes received, not including MAC/PHY overhead
   * \param rxBytes the number of bytes received
   * \return none
   */
  void IncRxBytes (uint32_t rxBytes);

  /**
   * \brief Increments the count of packets received
   * \return none
   */
  void IncRxPkts ();

  /**
   * \brief Sets the number of bytes received.
   * \param rxBytes the number of bytes received
   * \return none
   */
  void SetRxBytes (uint32_t rxBytes);

  /**
   * \brief Sets the number of packets received
   * \param rxPkts the number of packets received
   * \return none
   */
  void SetRxPkts (uint32_t rxPkts);

  /**
   * \brief Returns the number of bytes transmitted
   * \return the number of bytes transmitted
   */
  uint32_t GetTxBytes ();

  /**
   * \brief Returns the cumulative number of bytes transmitted
   * \param socket the receiving socket
   * \return none
   */
  uint32_t GetCumulativeTxBytes ();

  /**
   * \brief Returns the number of packets transmitted
   * \return the number of packets transmitted
   */
  uint32_t GetTxPkts ();

  /**
   * \brief Returns the cumulative number of packets transmitted
   * \return the cumulative number of packets transmitted
   */
  uint32_t GetCumulativeTxPkts ();

  /**
   * \brief Increment the number of bytes transmitted
   * \param txBytes the number of addtional bytes transmitted
   * \return none
   */
  void IncTxBytes (uint32_t txBytes);

  /**
   * \brief Increment the count of packets transmitted
   * \return none
   */
  void IncTxPkts ();

  /**
   * \brief Sets the number of bytes transmitted
   * \param txBytes the number of bytes transmitted
   * \return none
   */
  void SetTxBytes (uint32_t txBytes);

  /**
   * \brief Sets the number of packets transmitted
   * \param txPkts the number of packets transmitted
   * \return none
   */
  void SetTxPkts (uint32_t txPkts);

private:
  uint32_t m_RxBytes;
  uint32_t m_cumulativeRxBytes;
  uint32_t m_RxPkts;
  uint32_t m_cumulativeRxPkts;
  uint32_t m_TxBytes;
  uint32_t m_cumulativeTxBytes;
  uint32_t m_TxPkts;
  uint32_t m_cumulativeTxPkts;
};

RoutingStats::RoutingStats ()
  : m_RxBytes (0),
    m_cumulativeRxBytes (0),
    m_RxPkts (0),
    m_cumulativeRxPkts (0),
    m_TxBytes (0),
    m_cumulativeTxBytes (0),
    m_TxPkts (0),
    m_cumulativeTxPkts (0)
{
}

uint32_t
RoutingStats::GetRxBytes ()
{
  return m_RxBytes;
}

uint32_t
RoutingStats::GetCumulativeRxBytes ()
{
  return m_cumulativeRxBytes;
}

uint32_t
RoutingStats::GetRxPkts ()
{
  return m_RxPkts;
}

uint32_t
RoutingStats::GetCumulativeRxPkts ()
{
  return m_cumulativeRxPkts;
}

void
RoutingStats::IncRxBytes (uint32_t rxBytes)
{
  m_RxBytes += rxBytes;
  m_cumulativeRxBytes += rxBytes;
}

void
RoutingStats::IncRxPkts ()
{
  m_RxPkts++;
  m_cumulativeRxPkts++;
}

void
RoutingStats::SetRxBytes (uint32_t rxBytes)
{
  m_RxBytes = rxBytes;
}

void
RoutingStats::SetRxPkts (uint32_t rxPkts)
{
  m_RxPkts = rxPkts;
}

uint32_t
RoutingStats::GetTxBytes ()
{
  return m_TxBytes;
}

uint32_t
RoutingStats::GetCumulativeTxBytes ()
{
  return m_cumulativeTxBytes;
}

uint32_t
RoutingStats::GetTxPkts ()
{
  return m_TxPkts;
}

uint32_t
RoutingStats::GetCumulativeTxPkts ()
{
  return m_cumulativeTxPkts;
}

void
RoutingStats::IncTxBytes (uint32_t txBytes)
{
  m_TxBytes += txBytes;
  m_cumulativeTxBytes += txBytes;
}

void
RoutingStats::IncTxPkts ()
{
  m_TxPkts++;
  m_cumulativeTxPkts++;
}

void
RoutingStats::SetTxBytes (uint32_t txBytes)
{
  m_TxBytes = txBytes;
}

void
RoutingStats::SetTxPkts (uint32_t txPkts)
{
  m_TxPkts = txPkts;
}

/**
 * \ingroup wave
 * \brief The RoutingHelper class generates routing data between
 * nodes (vehicles) and uses the RoutingStats class to collect statistics
 * on routing data (application-data packet and byte counts).
 * A routing protocol is configured, and all nodes attempt to send
 * (i.e. route) small packets to another node, which acts as
 * data sinks.  Not all nodes act as data sinks.
 * for the vehicular network
 */
class RoutingHelper : public Object
{
public:
  /**
   * \brief Get class TypeId
   * \return the TypeId for the class
   */
  static TypeId GetTypeId (void);

  /**
   * \brief Constructor
   * \return none
   */
  RoutingHelper ();

  /**
   * \brief Destructor
   * \return none
   */
  virtual ~RoutingHelper ();

  void GenerateTraffic(Ptr<Socket> socket, InetSocketAddress broad_addr, InetSocketAddress local);

  /**
   * \brief Installs routing funcationality on nodes and their
   * devices and interfaces.
   * \param c node container
   * \param d net device container
   * \param i IPv4 interface container
   * \param totalTime the total time that nodes should attempt to
   * route data
   * \param protocol the routing protocol (1=OLSR;2=AODV;3=DSDV;4=DSR)
   * \param nSinks the number of nodes which will act as data sinks
   * \param routingTables dump routing tables at t=5 seconds (0=no;1=yes)
   * \return none
   */
  void Install (NodeContainer & c,
                NetDeviceContainer & d,
                Ipv4InterfaceContainer & i,
                double totalTime,
                int protocol,
                uint32_t nSinks,
                int routingTables);

  /**
   * \brief Trace the receipt of an on-off-application generated packet
   * \param context this object
   * \param packet a received packet
   * \return none
   */
  void OnOffTrace (std::string context, Ptr<const Packet> packet);

  /**
   * \brief Returns the RoutingStats instance
   * \return the RoutingStats instance
   */
  RoutingStats & GetRoutingStats ();

  /**
   * \brief Enable/disable logging
   * \param log non-zero to enable logging
   * \return none
   */
  void SetLogging (int log);

private:
  /**
   * \brief Sets up the protocol protocol on the nodes
   * \param c node container
   * \return none
   */
  void SetupRoutingProtocol (NodeContainer & c);

  /**
   * \brief Assigns IPv4 addresses to net devices and their interfaces
   * \param d net device container
   * \param adhocTxInterfaces IPv4 interface container
   * \return none
   */
  void AssignIpAddresses (NetDeviceContainer & d,
                          Ipv4InterfaceContainer & adhocTxInterfaces);
  // マリシャスフラグの作成
  void BuildMaliciasFlag ();
  // ノードIDとIpアドレスの対応を作成
  void MakeRelationIdAndIp(NetDeviceContainer & d,
                          Ipv4InterfaceContainer & adhocTxInterfaces);

  /**
   * \brief Sets up routing messages on the nodes and their interfaces
   * \param c node container
   * \param adhocTxInterfaces IPv4 interface container
   * \return none
   */
  void SetupRoutingMessages (NodeContainer & c,
                             Ipv4InterfaceContainer & adhocTxInterfaces);

  /**
   * \brief Sets up a routing packet for tranmission
   * \param addr destination address
   * \parm node source node
   * \return Socket to be used for sending/receiving a routed data packet
   */
  Ptr<Socket> SetupRoutingPacketReceive (Ipv4Address addr, Ptr<Node> node, uint32_t div_for_port);

  /**
   * \brief Process a received routing packet
   * \param socket the receiving socket
   * \return none
   */
  void ReceiveRoutingPacket (Ptr<Socket> socket);
  void memo_Unicast();

  uint32_t Return_Id_From_Ipv4(Ipv4Address sender_address);
  std::string Return_Ipv4_string_From_Id (uint32_t id);
  //マリシャスノードのIDを返す．正規であれば-1を返す
  int ConventionalScheme(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position);
  int ReadyForConventional(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now);
  int ConventionalAlgorithm(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now);
  void verifies_similarity(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now);
  bool is_matched_model(double sender_speed, double sender_density, double sender_flow);
  int ProposedScheme(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag);
  double UpdateByInterAreaRate(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed);
  double UpdateSpeedAVG(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed);
  int UpdateFlowAVG(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed);
  void DecideThreshold(uint32_t reciever_id);
  int UpdateVfAndKj(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed);
  bool StaticAnalisis(std::vector<viecle_data> &ob_v_dates, std::vector<calc_data> &ob_c_dates, uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed);
  bool FilterProperly(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position);
  bool IsObserved(uint32_t id_sender, uint32_t reciever_id);
  int PushAsObservedNode(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now);
  int ChainReaction(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now);
  bool Probability_Legitimacy(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now);
  bool DeciderProposed(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now);
  int AcceptData(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_flow_ave, double sender_speed,double sender_density, double now, double sender_position);
  int AcceptAsObserved(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_flow_ave, double sender_speed, double sender_density, double now, double sender_position);
  int SendHopFlag(uint32_t id_sender);
  std::vector<uint32_t> ReturnPosSequence(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_flow_ave, double sender_speed, double sender_density, double now, double sender_position);
  std::vector<uint32_t> ReturnSequenceFromFront(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::unordered_map<uint32_t, received_message> &tmp_data);
  std::vector<uint32_t> ReturnSequenceFromBack(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::unordered_map<uint32_t, received_message> &tmp_data);
  // int PushDataForObservedNode(auto itr_observed, uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed,double sender_density, double sender_flow_ave, double now);
  bool DecidePushDataForObservedNode (uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double now);
  bool DecidePushDataForCalcdNode (uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double now);
  double ReturnMalliciasFlowInterigent(uint32_t reciever_id);
  double ReturnMalliciasFlow(uint32_t reciever_id, double flow_ave);
  double ReturnMalliciasDensity(uint32_t reciever_id, double density, double flow);
  double ReturnCalculateDensity(uint32_t receiver_id, double flow, double density);
  double ReturnMalliciasSpeed(uint32_t reciever_id, double flow_ave, double density);
  std::string GenerateMessage(int flag, Ptr<Socket> socket, InetSocketAddress local);
  std::string GenerateMessagePart(int max_size, double value);
  double ReturnflowAve(uint32_t node_id_sender);
  double ReturnDensitySend(uint32_t node_id_sender);

  bool IsDenger(double related_speed, double accele, double sender_position, uint32_t reciever_id);
  bool DecideToReceiveAcceleration(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now);
  int  DecideToReceiveSpeed(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now);
  int  DecideToReceiveDensity(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now);
  int  DecideToReceiveFlowAve(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now);
  bool ProposedAlgorthm(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now);
  int  ReturnMessageState(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now);
  int  ApproveFlowAve(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::vector<uint32_t> &sequence_from_front, std::vector<uint32_t> &sequence_from_back, std::unordered_map<uint32_t, received_message> tmp_data);
  int  ApproveDensity(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::vector<uint32_t> &sequence_from_front, std::vector<uint32_t> &sequence_from_back, std::unordered_map<uint32_t, received_message> tmp_data);
  int  ApproveDivDensity(std::vector<data_front> &datas_front, uint32_t reciever_id, double sender_position, double sender_speed, uint32_t id_sender, int at_sender);
  int  ReturnDensityState(std::vector<data_front> &datas_front, uint32_t reciever_id, double sender_position, double sender_speed, uint32_t id_sender, int at_sender);
  int  DecideLowFlowAve(uint32_t reciever_id);
  int  SaveFlowAve(uint32_t reciever_id, double now);

  double m_TotalSimTime;        // seconds
  uint32_t m_protocol;       // routing protocol; 0=NONE, 1=OLSR, 2=AODV, 3=DSDV, 4=DSR
  uint32_t m_port;
  uint32_t m_nSinks;              // number of sink nodes (< all nodes)
  int m_routingTables;      // dump routing table (at t=5 sec).  0=No, 1=Yes
  RoutingStats routingStats;
  std::string m_protocolName;
  int m_log;
};

NS_OBJECT_ENSURE_REGISTERED (RoutingHelper);

TypeId
RoutingHelper::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::RoutingHelper")
    .SetParent<Object> ()
    .AddConstructor<RoutingHelper> ();
  return tid;
}

RoutingHelper::RoutingHelper ()
  : m_TotalSimTime (300.01),
    m_protocol (0),
    m_port (9),
    m_nSinks (0),
    m_routingTables (0),
    m_log (0)
{
}

RoutingHelper::~RoutingHelper ()
{
}

void
RoutingHelper::Install (NodeContainer & c,
                        NetDeviceContainer & d,
                        Ipv4InterfaceContainer & i,
                        double totalTime,
                        int protocol,
                        uint32_t nSinks,
                        int routingTables)
{
  m_TotalSimTime = totalTime;
  m_protocol = protocol;
  m_nSinks = NUM_OF_NODES;
  m_routingTables = routingTables;

  SetupRoutingProtocol (c);
  AssignIpAddresses (d, i);
  SetupRoutingMessages (c, i);
}

Ptr<Socket>
RoutingHelper::SetupRoutingPacketReceive (Ipv4Address addr, Ptr<Node> node, uint32_t div_for_port)
{
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> sink = Socket::CreateSocket (node, tid);
  InetSocketAddress local = InetSocketAddress (addr, m_port + div_for_port);
  sink->Bind (local);
  sink->SetRecvCallback (MakeCallback (&RoutingHelper::ReceiveRoutingPacket, this));

  return sink;
}

void
RoutingHelper::SetupRoutingProtocol (NodeContainer & c)
{
  AodvHelper aodv;
  OlsrHelper olsr;
  DsdvHelper dsdv;
  DsrHelper dsr;
  DsrMainHelper dsrMain;
  Ipv4ListRoutingHelper list;
  InternetStackHelper internet;

  Time rtt = Time (5.0);
  AsciiTraceHelper ascii;
  Ptr<OutputStreamWrapper> rtw = ascii.CreateFileStream ("routing_table");

  switch (m_protocol)
    {
    case 0:
      m_protocolName = "NONE";
      break;
    case 1:
      if (m_routingTables != 0)
        {
          olsr.PrintRoutingTableAllAt (rtt, rtw);
        }
      list.Add (olsr, 100);
      m_protocolName = "OLSR";
      break;
    case 2:
      if (m_routingTables != 0)
        {
          aodv.PrintRoutingTableAllAt (rtt, rtw);
        }
      list.Add (aodv, 100);
      m_protocolName = "AODV";
      break;
    case 3:
      if (m_routingTables != 0)
        {
          dsdv.PrintRoutingTableAllAt (rtt, rtw);
        }
      list.Add (dsdv, 100);
      m_protocolName = "DSDV";
      break;
    case 4:
      // setup is later
      m_protocolName = "DSR";
      break;
    default:
      NS_FATAL_ERROR ("No such protocol:" << m_protocol);
      break;
    }

  if (m_protocol < 4)
    {
      internet.SetRoutingHelper (list);
      internet.Install (c);
    }
  else if (m_protocol == 4)
    {
      internet.Install (c);
      dsrMain.Install (dsr, c);
    }

  if (m_log != 0)
    {
      NS_LOG_UNCOND ("Routing Setup for " << m_protocolName);
    }
}

void
RoutingHelper::AssignIpAddresses (NetDeviceContainer & d,
                                  Ipv4InterfaceContainer & adhocTxInterfaces)
{
  NS_LOG_INFO ("Assigning IP addresses");
  Ipv4AddressHelper addressAdhoc;
  // マリシャスフラグを立てる
  RoutingHelper::BuildMaliciasFlag ();
  // we may have a lot of nodes, and want them all
  // in same subnet, to support broadcast
  addressAdhoc.SetBase ("10.1.0.0", "255.255.0.0");
  adhocTxInterfaces = addressAdhoc.Assign (d);
  // アドレスとノードIDの対応を作成
  RoutingHelper::MakeRelationIdAndIp (d, adhocTxInterfaces);
}

void
RoutingHelper::MakeRelationIdAndIp(NetDeviceContainer & d,
                                  Ipv4InterfaceContainer & adhocTxInterfaces)
{
  uint32_t nDevices = d.GetN ();
  Ptr<Node> node;
  Ptr<NetDevice> p;
  std::string addr_string;

  for (uint32_t i = 0; i < nDevices; ++i)
  {
    std::stringstream ss;
    p = d.Get (i);
    node = p->GetNode();
    Ptr<Ipv4> ipv4 = node->GetObject<Ipv4>();
    Ipv4InterfaceAddress iaddr = ipv4->GetAddress (1,0);
    Ipv4Address addri = iaddr.GetLocal ();
    ss << addri;
    addr_string = ss.str();
    id_from_Ipv4adress[addr_string] = node->GetId();
    // std::cout << " id = " << node->GetId() << " adress = " << addr_string << std::endl;
  }
}

void
RoutingHelper::BuildMaliciasFlag ()
{
  if(MALLICIAS_RATE != 0){
    double mallicias_pointer = 1;
    double mallicias_diff = (double)100 / (double)MALLICIAS_RATE;
    int count = (int)(NUM_OF_NODES * ((double)MALLICIAS_RATE / (double)100));
    int over255 = 0;
    std::string address_head = "10.1.";
    std::string string_mallicias_pointer;
    std::string string_over255;
    std::string address_mallicias;
    for(;0 < count; count--){
      string_mallicias_pointer = std::to_string((int)mallicias_pointer);
      string_over255 = std::to_string(over255);
      address_mallicias = address_head + string_over255 + "." + string_mallicias_pointer;
      real_mallicias[address_mallicias] = 1;
      mallicias_pointer += mallicias_diff;
      // 255を超えるノード数の場合はipアドレスの繰上げを行う
      if(255 < mallicias_pointer){
        over255++;
        mallicias_pointer -= 255;
      }
      std::cout << address_mallicias << std::endl;
    }
  }
}

void
RoutingHelper::SetupRoutingMessages (NodeContainer & c,
                                     Ipv4InterfaceContainer & adhocTxInterfaces)
{
  Ptr<UniformRandomVariable> var = CreateObject<UniformRandomVariable> ();
  int64_t stream = 2;
  var->SetStream (stream);
  for (uint32_t i = 0; i < m_nSinks; i++)
    {
      // protocol == 0 means no routing data, WAVE BSM only
      // so do not set up sink
      if (m_protocol != 0)
        {
          Ptr<Socket> sink = SetupRoutingPacketReceive (adhocTxInterfaces.GetAddress (i), c.Get (i), 0);
          Ptr<Socket> sink_for_recv = SetupRoutingPacketReceive (adhocTxInterfaces.GetAddress (i), c.Get (i), 1);
        }

      // // Setup routing transmissions
      // OnOffHelper onoff1 ("ns3::UdpSocketFactory",Address ());
      // onoff1.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"));
      // onoff1.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));
      // //ビーコンの実装
      // AddressValue remoteAddress (InetSocketAddress (Ipv4Address ("255.255.255.255"), m_port));
      // onoff1.SetAttribute ("Remote", remoteAddress);
      // ApplicationContainer temp = onoff1.Install (c.Get (i));
      // temp.Start (Seconds (var->GetValue (1.0,2.0)));
      // temp.Stop (Seconds (m_TotalSimTime));

      //前のコード
      // //ビーコンの設定
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      Ptr<Socket> source = Socket::CreateSocket (c.Get (i), tid);
      InetSocketAddress broad_addr = InetSocketAddress (Ipv4Address ("255.255.255.255"), m_port);
      InetSocketAddress local = InetSocketAddress (adhocTxInterfaces.GetAddress (i) , m_port);
      source->Bind (local);
      //１秒ごとにビーコンを出す
      Simulator::ScheduleWithContext(i, Seconds (var->GetValue (1.0,2.0)), &RoutingHelper::GenerateTraffic, this, source, broad_addr, local/*, packetSize, numPackets, interPacketInterval*/);

      //オリジナルコード
      // AddressValue remoteAddress (InetSocketAddress (/*adhocTxInterfaces.GetAddress (i)*/ Ipv4Address::GetAny (), m_port + 1));
      // onoff1.SetAttribute ("Remote", remoteAddress);
      // ApplicationContainer temp = onoff1.Install (c.Get (/*i + m_nSinks*/ i));
      // temp.Start (Seconds (var->GetValue (1.0,2.0)));
      // temp.Stop (Seconds (m_TotalSimTime));
    }
}

//ビーコンからブロードキャスト信号を生成
void
RoutingHelper::GenerateTraffic(Ptr<Socket> source, InetSocketAddress broad_addr, InetSocketAddress local){

  //ブロードキャスト信号か無線信号かのフラグを立てる
  uint8_t  buf[1024];
  std::string message = RoutingHelper::GenerateMessage(1, source, local);
  uint16_t bufsize = message.size();
  memcpy(buf, message.c_str(),bufsize);
  Ptr<Packet> beacon_packet = Create<Packet> (buf, bufsize);

  source->SetAllowBroadcast(true);
  source->Connect (broad_addr);
  source->Send (beacon_packet);

  Simulator::Schedule (Seconds (TRANSMISSION_RATE), &RoutingHelper::GenerateTraffic, this, source, broad_addr, local/*, pktSize,pktCount-1, pktInterval*/);
}

static inline std::string
PrintReceivedRoutingPacket (Ptr<Socket> socket, Ptr<Packet> packet, Address srcAddress)
{
  std::ostringstream oss;

  oss << Simulator::Now ().GetSeconds () << " " << socket->GetNode ()->GetId ();

  if (InetSocketAddress::IsMatchingType (srcAddress))
    {
      // InetSocketAddress addr = InetSocketAddress::ConvertFrom (srcAddress);
      // oss << " received one packet from " << addr.GetIpv4 ();
    }
  else
    {
      oss << " received one packet!";
    }
  return oss.str ();
}

void
RoutingHelper::ReceiveRoutingPacket (Ptr<Socket> socket)
{
  Ptr<Packet> packet;
  Address srcAddress;
  uint32_t id_reviever = socket->GetNode()->GetId();
  Address local_addr;
  socket->GetSockName(local_addr);
  InetSocketAddress l_addr = InetSocketAddress::ConvertFrom (local_addr);

  //バッファ処理用の配列
  uint8_t buf[1024];
  memset(buf, 0, sizeof(buf));
  while ((packet = socket->RecvFrom (srcAddress)))
    {
      if (packet->GetSize () > 0) {
          // 1000で割ってフラグだけ取り出す
          packet->CopyData(buf, 1024);
          // 速度とフロウを取得
          std::string message_string = std::string((const char *)buf);
          std::string speed_recv = message_string.substr(1, MAX_PACKETSIZE_OF_SPEED);
          std::string flow_recv = message_string.substr(1 + MAX_PACKETSIZE_OF_SPEED, MAX_PACKETSIZE_OF_FLOW);
          std::string density_recv = message_string.substr(1 + MAX_PACKETSIZE_OF_SPEED + MAX_PACKETSIZE_OF_FLOW, MAX_PACKETSIZE_OF_DENSITY);
          std::string flow_ave_recv = message_string.substr(1 + MAX_PACKETSIZE_OF_SPEED + MAX_PACKETSIZE_OF_FLOW + MAX_PACKETSIZE_OF_DENSITY, MAX_PACKETSIZE_OF_FLOW_AVE);
          std::string position_recv = message_string.substr(1 + MAX_PACKETSIZE_OF_SPEED + MAX_PACKETSIZE_OF_FLOW + MAX_PACKETSIZE_OF_DENSITY + MAX_PACKETSIZE_OF_FLOW_AVE, MAX_PACKETSIZE_OF_POSITION);
          std::string hop_flag_recv = message_string.substr(1 + MAX_PACKETSIZE_OF_SPEED + MAX_PACKETSIZE_OF_FLOW + MAX_PACKETSIZE_OF_DENSITY + MAX_PACKETSIZE_OF_FLOW_AVE + MAX_PACKETSIZE_OF_POSITION, MAX_PACKETSIZE_OF_HOP_FLAG);
          double sender_speed = std::stod(speed_recv);
          double sender_flow = std::stod(flow_recv);
          double sender_density = std::stod(density_recv);
          double sender_flow_ave = std::stod(flow_ave_recv);
          double sender_position = std::stod(position_recv);
          int    sender_hop_flag = std::stoi(hop_flag_recv);

          InetSocketAddress r_addr = InetSocketAddress::ConvertFrom (srcAddress);
          // 従来手法で判定 マリシャスと判定されたデータは無視
          std::stringstream ss;
          std::stringstream ss_reciever_stream;
          ss << r_addr.GetIpv4 ();
          ss_reciever_stream << l_addr.GetIpv4();
          std::string addr = ss.str();
          std::string addr_reciever_string =  ss_reciever_stream.str();
          // 実行モードの判定
          switch (EXECUTION_MODE) {
            case 0:
            case 3:
              RoutingHelper::ConventionalScheme(id_reviever, r_addr.GetIpv4 (), sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position);
              break;

            case 1:
            case 2:
              RoutingHelper::ProposedScheme(id_reviever, r_addr.GetIpv4 (), sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag);
              break;
          }

          auto itr = result_mallicias.find(addr);
          // 送信者がすでに検知済みの場合，もしくは受信者が攻撃ノードである場合は受信しない
          if((itr == result_mallicias.end())){
            std::cout << "This socket received Unicast revieved packet is "<< message_string << std::endl;
          }
      }

      //application data, for goodput
      uint32_t RxRoutingBytes = packet->GetSize ();
      GetRoutingStats ().IncRxBytes (RxRoutingBytes);
      GetRoutingStats ().IncRxPkts ();
      if (m_log != 0)
        {
          NS_LOG_UNCOND (m_protocolName + " " + PrintReceivedRoutingPacket (socket, packet, srcAddress));
        }
    }
}

void
RoutingHelper::memo_Unicast(){
  // ユニキャストだったが使わないのでメモ化
  // int flag_recv = atoi((const char *)buf) / 1000;
  // // int32_t node_id_recv = socket->GetNode()->GetId();
  // // バッファの値が1ならブロードキャスト，０なら返信パケット
  // // 事前計算フラグが立っていなければ返信
  // if(/*flag_recv == 1 && my_data[node_id_recv].calc_density_flag != 1*/ false){
  //   std::cout << "flag = " << flag_recv << std::endl;
  //   char t[10];
  //   sprintf(t,"%8.5f",Simulator::Now ().GetSeconds ());
  //   std::cout << "This socket received Broadcast" << std::endl;
  //
  //   // パケットを作成
  //   std::string message = RoutingHelper::GenerateMessage(0, socket);
  //   uint16_t size_of_message = message.size();
  //   uint8_t  buffer[1024];
  //   memcpy(buffer , message.c_str(), size_of_message);
  //   Ptr<Packet> packet_for_recv = Create<Packet> (buffer, size_of_message);
  //
  //   //新しく返信用のソケットを作り返信
  //   uint32_t div_local_port = 1;
  //   uint32_t port_for_recv = m_port + div_local_port;
  //   TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  //   Ptr<Socket> source = Socket::CreateSocket (socket->GetNode(), tid);
  //   //バインド
  //   Address local_addr;
  //   socket->GetSockName(local_addr);
  //   InetSocketAddress l_addr = InetSocketAddress::ConvertFrom (local_addr);
  //   InetSocketAddress local = InetSocketAddress (l_addr.GetIpv4 (), port_for_recv);
  //   source->Bind (local);
  //   //コネクト
  //   InetSocketAddress re_addr = InetSocketAddress::ConvertFrom (srcAddress);
  //   InetSocketAddress recv_addr = InetSocketAddress (re_addr.GetIpv4 (), port_for_recv);
  //   source->Connect (recv_addr);
  //   //送信
  //   source->SetAllowBroadcast(false);
  //   source->Send (packet_for_recv);
  // }
}

int
RoutingHelper::SendHopFlag(uint32_t id_sender)
{ // varification_flagを参照
  bool has_varification = (varifications[id_sender].find(id_sender) != varifications[id_sender].end()) ? true : false;
  double effective_time = varifications[id_sender][id_sender].effective_time;

  if(has_varification && effective_time <= Simulator::Now ().GetSeconds ()){
    return varifications[id_sender][id_sender].varification_flag;
  } else {
    return 2;
  }
}

double
RoutingHelper::ReturnCalculateDensity(uint32_t receiver_id, double flow, double density)
{  // return calculated flow which is smaller one
  double Vf = my_data[receiver_id].Vf;
  double Kj = my_data[receiver_id].Kj;

  // 割り算可能かで返り値を変更
  if (Kj != 0 && (2 * Vf / Kj) != 0){
    double density_1 = (Vf + sqrt(Vf * Vf - 4 * Vf / Kj * flow)) / (2 * Vf / Kj);
    double density_2 = (Vf - sqrt(Vf * Vf - 4 * Vf / Kj * flow)) / (2 * Vf / Kj);
    return (density_1 < density_2) ? density_1 : density_2;
  } else {
    return density;
  }

  return density;
}

double RoutingHelper::ReturnMalliciasDensity(uint32_t reciever_id, double density, double flow){
  if (ACCIDENT_MODE == 0) {
    if (ATTACKERS_PRIORITY == 0) {
      switch (ATTACK_PATTERN) {
        case 1:
        case 4:
        case 5:
          return RoutingHelper::ReturnCalculateDensity(reciever_id, flow, density);
          break;

        default:
          return density;
          break;
      }
    } else if (ATTACKERS_PRIORITY == 1) {
      return  my_data[reciever_id].Kj - ((my_data[reciever_id].Kj - density) * ((double)DENSITY_RATE_OF_MALLICIOUS / 100.0));
    } else {
      return density;
    }
  } else if (ACCIDENT_MODE == 1) {
    return density;
  } else {
    return density;
  }

  return density;
}

double RoutingHelper::ReturnMalliciasSpeed(uint32_t reciever_id, double flow_ave, double density){
  return flow_ave / density - (RELATIVE_SPEED_THRESHOLD / 4.0);
}

double RoutingHelper::ReturnMalliciasFlowInterigent(uint32_t reciever_id)
{
    double max_flow_ave = 0.0;
    double now = Simulator::Now ().GetSeconds ();
    int at = 0;
    std::vector<double> diff_from_speed_ave;

    switch (EXECUTION_MODE) {
      case 0:
      case 3:
        {
          std::unordered_map<std::string, double> access_log_tmp = access_log[reciever_id];
          std::unordered_map<std::string, double> access_flow_log_tmp = access_flow_log[reciever_id];
          // １秒以内にログがあったデータかつ値が０以外ならば速度に加算
          for(auto itr = access_log_tmp.begin(); itr != access_log_tmp.end(); ++itr) {
            if((now - itr->second <= INTER_ARRIVAL_RATE) && (access_flow_log_tmp[itr->first] != 0)){
              // 受信履歴の最大値を取得
              diff_from_speed_ave.push_back(fabs(access_flow_log_tmp[itr->first] - my_data[reciever_id].flow_recv));
            }
          }
        }
        break;

      case 1:
      case 2:
        {
          std::unordered_map<uint32_t, struct received_message> tmp_received_message = own_received_messages[reciever_id];
          // １秒以内にログがあったデータかつ値が０以外ならば速度に加算
          for(auto itr = tmp_received_message.begin(); itr != tmp_received_message.end(); ++itr) {
            if((now - itr->second.received_time <= INTER_ARRIVAL_RATE)){
              // 受信履歴の最大値を取得
              max_flow_ave = (max_flow_ave < itr->second.flow) ? itr->second.flow : max_flow_ave;
              diff_from_speed_ave.push_back(fabs(itr->second.flow - my_data[reciever_id].flow_recv));
            }
          }
        }
        break;
    }

    std::sort(diff_from_speed_ave.begin(), diff_from_speed_ave.end());
    at = (int)((diff_from_speed_ave.size() - 1) * 0.75);
    // 検知されないような値を返す
    return my_data[reciever_id].density_calc * my_data[reciever_id].speed_ave - diff_from_speed_ave.at(at) * RATE;
}

double RoutingHelper::ReturnMalliciasFlow(uint32_t reciever_id, double flow_ave)
{
  // 道路状況で場合分け
  switch (ACCIDENT_MODE) {
    case 0:
      // 攻撃方法で場合分け
      switch (ATTACK_PATTERN) {
        case 0:
          return my_data[reciever_id].flow_own * (FLOW_RATE_OF_MALLICIAS / 100.0);
          break;

        case 1:
          return RoutingHelper::ReturnMalliciasFlowInterigent(reciever_id);
          break;

        case 4:
          return my_data[reciever_id].flow_recv - (THRESHOLD / 2.0);
          break;

        case 5:
          return my_data[reciever_id].flow_recv - (THRESHOLD / 2.0);
          break;

        default:
          return my_data[reciever_id].flow_own * (FLOW_RATE_OF_MALLICIAS / 100.0);
          break;
      }
      break;

    case 1:
      return 1200;
      break;
  }

  return 0;
}

std::string
RoutingHelper::GenerateMessagePart(int max_size, double value){
  std::string message_part = std::to_string(fabs(value));
  uint16_t size_for_value = message_part.size();
  for (uint16_t i = 0; i < (max_size - size_for_value); i++){
    message_part = "0" + message_part;
  }
  return message_part;
}

double
RoutingHelper::ReturnDensitySend(uint32_t node_id_sender)
{
  switch (EXECUTION_MODE) {
    case 1:
    case 2:
      return own_received_messages[node_id_sender][node_id_sender].density;
      break;

    default:
      return my_data[node_id_sender].density_calc;
      break;
  }

  return 0;
}

double
RoutingHelper::ReturnflowAve(uint32_t node_id_sender)
{
  switch (EXECUTION_MODE) {
    case 0:
      return my_data[node_id_sender].flow_recv;
      break;

    case 1:
    case 2:
      return own_received_messages[node_id_sender][node_id_sender].flow_ave;
      break;

    case 3:
      // Flow_ownを送信
      return my_data[node_id_sender].speed_ave * my_data[node_id_sender].density_calc;
      break;

    default:
      return my_data[node_id_sender].flow_recv;
      break;
  }

  return 0;
}

std::string
RoutingHelper::GenerateMessage(int flag, Ptr<Socket> socket, InetSocketAddress local)
{
  int32_t node_id_sender = socket->GetNode()->GetId();
  // Ipアドレスの取得
  std::stringstream ss;
  ss << local.GetIpv4();
  std::string addr_string = ss.str();
  double now = Simulator::Now ().GetSeconds ();
  double my_speed = socket->GetNode()->GetObject<MobilityModel>()->GetVelocity().x * CONVERT_Ms_KMh;
  double position = socket->GetNode()->GetObject<MobilityModel>()->GetPosition().x * CONVERT_M_KM;
  double density = RoutingHelper::ReturnDensitySend(node_id_sender);
  // my_data[node_id_sender].flow_own = my_data[node_id_sender].speed_ave * my_data[node_id_sender].density_calc;
  double flow = my_data[node_id_sender].flow_own;
  double flow_ave = RoutingHelper::ReturnflowAve(node_id_sender);
  int    hop_flag = RoutingHelper::SendHopFlag(node_id_sender);

  // 自分のデータを保存
  RoutingHelper::AcceptData(node_id_sender, local.GetIpv4(), flow, flow_ave, my_speed, density ,now, position);

  // マリシャスフラグが立っておりかつ攻撃開始時間なら攻撃開始
  if((real_mallicias.find(addr_string) != real_mallicias.end()) && (now > START_ATTACK_TIME)){
    // if the attacker's oriotirty equals to 1, then the order of the attacker's values are, flow, speed, density.
    if (ATTACKERS_PRIORITY == 0){
      flow = RoutingHelper::ReturnMalliciasFlow(node_id_sender, flow_ave);
      density = RoutingHelper::ReturnMalliciasDensity(node_id_sender, density, flow);
      my_speed = RoutingHelper::ReturnMalliciasSpeed(node_id_sender, flow, density);
    } else if (ATTACKERS_PRIORITY == 1) {
      density = RoutingHelper::ReturnMalliciasDensity(node_id_sender, density, flow);
      my_speed = RoutingHelper::ReturnMalliciasSpeed(node_id_sender, flow, density);
      flow = my_speed * density;
    } else if (ATTACKERS_PRIORITY == 2) {
      my_speed = my_speed * (double)SPEED_RATE_OF_MALLICIOUS / 100.0;
      density = my_data[node_id_sender].Kj * (1.0 - my_speed / my_data[node_id_sender].Vf);
      flow = my_speed * density;
    }
  }

  // メッセージの作成
  std::string flag_string = std::to_string(flag);
  std::string speed = RoutingHelper::GenerateMessagePart((int)MAX_PACKETSIZE_OF_SPEED, my_speed);
  std::string flow_s = RoutingHelper::GenerateMessagePart((int)MAX_PACKETSIZE_OF_FLOW, flow);
  std::string density_s = RoutingHelper::GenerateMessagePart((int)MAX_PACKETSIZE_OF_DENSITY, density);
  std::string flow_ave_s = RoutingHelper::GenerateMessagePart((int)MAX_PACKETSIZE_OF_FLOW_AVE, flow_ave);
  std::string position_s = RoutingHelper::GenerateMessagePart((int)MAX_PACKETSIZE_OF_POSITION, position);
  std::string hop_flag_s = std::to_string(hop_flag);
  std::string message = flag_string + speed + flow_s + density_s + flow_ave_s + position_s + hop_flag_s;
  return message;
}

std::string
RoutingHelper::Return_Ipv4_string_From_Id (uint32_t id)
{
  std::string addr;
  for (auto itr = id_from_Ipv4adress.begin(); itr != id_from_Ipv4adress.end(); itr++){
    if(itr->second == id){
      addr = itr->first;
      break;
    }
  }

  return addr;
}

uint32_t
RoutingHelper::Return_Id_From_Ipv4(Ipv4Address sender_address)
{
  std::stringstream ss_sender;
  ss_sender << sender_address;
  std::string sender_string = ss_sender.str();
  return id_from_Ipv4adress[sender_string];
}

int
RoutingHelper::ChainReaction(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now){
  return 0;
}

bool
RoutingHelper::Probability_Legitimacy(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now){
  return true;
}

bool
RoutingHelper::DeciderProposed(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now)
{
  uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
  double diff_time = now - own_received_messages[reciever_id][id_sender].received_time;
  double diff_speed = sender_speed - own_received_messages[reciever_id][id_sender].speed;
  double diff_flow_ave = sender_flow_ave - own_received_messages[reciever_id][id_sender].flow_ave;
  bool decider = true;

  // 開始時間で場合分け
  if(now < START_TIME){
    decider = (diff_time != 0) ? true : false;
  } else if(THRESHOLD_SPEED < diff_speed){
    RoutingHelper::AcceptAsObserved(reciever_id, sender_address, sender_flow, sender_flow_ave, sender_speed, sender_density ,now, sender_position);
    decider = false;
  } else if(THRESHOLD < diff_flow_ave){
    decider = RoutingHelper::Probability_Legitimacy(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, now);
  }

  return decider;
}
bool
RoutingHelper::IsDenger(double related_speed, double accele, double sender_position, uint32_t reciever_id)
{
  // 加速度の検証
  if(ACCELE_THRESHOLD < fabs(accele)) {
    return true;
  }
  // 速度の検証
  if(fabs(sender_position - own_received_messages[reciever_id][reciever_id].position) < fabs(related_speed) * (0.75 / 3.6 + fabs(related_speed) / (254 * 0.7))){
    return true;
  }

  return false;
}

bool
RoutingHelper::DecideToReceiveAcceleration(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now)
{
  uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
  double reciever_position = own_received_messages[reciever_id][reciever_id].position;
  double form_speed = own_received_messages[reciever_id][id_sender].speed;
  double form_time = own_received_messages[reciever_id][id_sender].received_time;
  double diff_time = now - form_time;
  double diff_speed = sender_speed - form_speed;
  double accele = diff_speed / diff_time;
  double speed_ave = my_data[reciever_id].speed_ave;
  double real_related_speed = global_node_container.Get(reciever_id)->GetObject<MobilityModel>()->GetRelativeSpeed(global_node_container.Get(id_sender)->GetObject<MobilityModel>()) * CONVERT_Ms_KMh;
  struct varification tmp_varification;

  // 加速度が危険ではない場合は受理し，危険な場合は棄却する
  if(IsDenger(fabs(speed_ave - sender_speed), fabs(accele), sender_position, reciever_id) == false){
    return true;
  } else { // 位置が近い場合は実際の相対速度で計算を行う
    if(fabs(sender_position - reciever_position) <= COLLISION_AVOIDANCE_RANGE / 1000.0){
      // 検証データに保存
      tmp_varification.varificationer_id = reciever_id;
      tmp_varification.received_time = now;
      tmp_varification.effective_time = fabs(sender_position - own_received_messages[reciever_id][reciever_id].position) / own_received_messages[reciever_id][reciever_id].speed * 3600;

      // 再度判定
      if(IsDenger(fabs(real_related_speed), fabs(accele), sender_position, reciever_id) == false){
        tmp_varification.varification_flag = 0;
        return false;
      } else{
        tmp_varification.varification_flag = 1;
        return true;
      }

      varifications[reciever_id][reciever_id] = tmp_varification;
    } else {
      // スタックする

      return true;
    }
  }

  return true;
}

int
RoutingHelper::DecideToReceiveDensity(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now)
{
  uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
  double form_speed = own_received_messages[reciever_id][id_sender].speed;
  double form_time = own_received_messages[reciever_id][id_sender].received_time;
  double diff_time = now - form_time;
  double diff_speed = sender_speed - form_speed;

  std::unordered_map<uint32_t, received_message> tmp_data = own_received_messages[reciever_id];
  tmp_data[id_sender].received_time = now;
  tmp_data[id_sender].position = sender_position;
  tmp_data[id_sender].acceleration = diff_speed / diff_time;
  tmp_data[id_sender].density = sender_density;
  tmp_data[id_sender].speed = sender_speed;
  tmp_data[id_sender].flow_ave = sender_flow_ave;

  std::vector<uint32_t> sequence_from_front = RoutingHelper::ReturnSequenceFromFront(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now, tmp_data);
  std::vector<uint32_t> sequence_from_back = RoutingHelper::ReturnSequenceFromBack(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now, tmp_data);

  tmp_f[id_sender] = sequence_from_front;
  tmp_b[id_sender] = sequence_from_back;

  return RoutingHelper::ApproveDensity(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now, sequence_from_front, sequence_from_back, tmp_data);
}

int
RoutingHelper::DecideToReceiveSpeed(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now)
{
  std::vector<double> velocities;
  double iqr;
  double third_quartile;
  double first_quartile;
  double calc_speed = -(my_data[reciever_id].Vf / my_data[reciever_id].Kj) * sender_density + my_data[reciever_id].Vf;

  for (uint32_t id_node = 0; id_node < NUM_OF_NODES; id_node++){
    velocities.push_back(own_received_messages[reciever_id][id_node].speed);
  }
  std::sort(velocities.begin(), velocities.end());
  third_quartile = velocities.at((int)(velocities.size() * 0.75));
  first_quartile = velocities.at((int)(velocities.size() * 0.25));
  iqr = third_quartile - first_quartile;

  bool is_in_median = (first_quartile - 1.5 * iqr < sender_speed && sender_speed < third_quartile + 1.5 * iqr) ? true : false;
  bool is_out_of_related_speed = (fabs(sender_speed - calc_speed) > RELATIVE_SPEED_THRESHOLD) ? true : false;
  is_in_median = true;

  if(is_in_median && !is_out_of_related_speed){
    return IS_VICIOUS;

  } else if(is_in_median && is_out_of_related_speed){
    // return IS_MALICIOUS;
    return IS_MALICIOUS;

  } else if(!is_in_median && is_out_of_related_speed){
    return IS_MALICIOUS;

  } else if(!is_in_median && !is_out_of_related_speed){
    return IS_OBSERVED;
    // return IS_MALICIOUS;
  }

  return IS_VICIOUS;
}

std::vector<uint32_t>
RoutingHelper::ReturnSequenceFromFront(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::unordered_map<uint32_t, received_message> &tmp_data)
{
  std::vector<uint32_t> sequence_from_front;
  uint32_t id_max = reciever_id;
  uint32_t id_min = reciever_id;
  uint32_t id_pivot = reciever_id;

  // 一番先頭を算出と最後を算出
  for (auto itr = tmp_data.begin(); itr !=  tmp_data.end(); itr++){
    // 時間以内である場合
    if(now - itr->second.received_time < TRANSMISSION_RATE){
      if (tmp_data[id_max].position < itr->second.position){
        id_max = itr->first;
      }
      if (itr->second.position < tmp_data[id_min].position){
        id_min = itr->first;
      }
    }
  }
  // 順に挿入
  sequence_from_front.push_back(id_max);
  while(id_max != id_min){
    id_pivot = id_min;

    for (auto itr = tmp_data.begin(); itr !=  tmp_data.end(); itr++){
      // 時間以内である場合
      if(now - itr->second.received_time < TRANSMISSION_RATE){
        if(tmp_data[id_pivot].position <= itr->second.position && itr->second.position < tmp_data[id_max].position){
          id_pivot = itr->first;
        }
      }
    }
    // 挿入し上限値の更新
    sequence_from_front.push_back(id_pivot);
    id_max = id_pivot;
  }

  return sequence_from_front;
}

std::vector<uint32_t>
RoutingHelper::ReturnSequenceFromBack(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::unordered_map<uint32_t, received_message> &tmp_data)
{
  std::vector<uint32_t> sequence_from_back;
  uint32_t id_max = reciever_id;
  uint32_t id_min = reciever_id;
  uint32_t id_pivot = reciever_id;

  // 一番先頭を算出と最後を算出
  for (auto itr = tmp_data.begin(); itr !=  tmp_data.end(); itr++){
    // 時間以内である場合
    if(now - itr->second.received_time < TRANSMISSION_RATE){
      if (tmp_data[id_max].position < itr->second.position){
        id_max = itr->first;
      }
      if (itr->second.position < tmp_data[id_min].position){
        id_min = itr->first;
      }
    }
  }
  // 順に挿入
  sequence_from_back.push_back(id_min);
  while(id_max != id_min){
    id_pivot = id_max;

    for (auto itr = tmp_data.begin(); itr !=  tmp_data.end(); itr++){
      // 時間以内である場合
      if(now - itr->second.received_time < TRANSMISSION_RATE){
        if(tmp_data[id_min].position < itr->second.position && itr->second.position <= tmp_data[id_pivot].position){
          id_pivot = itr->first;
        }
      }
    }
    // 挿入し上限値の更新
    sequence_from_back.push_back(id_pivot);
    id_min = id_pivot;
  }

  return sequence_from_back;
}
int
RoutingHelper::DecideLowFlowAve(uint32_t reciever_id)
{
  uint32_t id_judger;
  double distance = TRANSMISSION_RANGE / 1000.0;
  double dist_tmp;

  // それっぽい値を検証していく
  for(auto itr = low_flow_nodes[reciever_id].begin(); itr != low_flow_nodes[reciever_id].end(); itr++){
    // 自分が観測者よりも前の場合に実行
    if(itr->second.sender_position > own_received_messages[reciever_id][reciever_id].position && own_received_messages[reciever_id][reciever_id].position + TRANSMISSION_RANGE / 1000.0 > itr->second.sender_position){
      for(auto itr_message = own_received_messages[reciever_id].begin(); itr_message != own_received_messages[reciever_id].end(); itr_message++){
        dist_tmp = fabs(itr->second.sender_position - itr_message->second.position);
        // 距離が最も近いものを判定
        if(dist_tmp < distance){
          id_judger = itr_message->first;
          distance = dist_tmp;
        }
      }
      // 距離が一番近いもので検証
      if(fabs(itr->second.sender_speed - own_received_messages[reciever_id][id_judger].speed) > RELATIVE_SPEED_THRESHOLD){
        result_mallicias[RoutingHelper::Return_Ipv4_string_From_Id(itr->first)] = 1;
      }
    }
  }
  return 0;
}

int
RoutingHelper::ReturnDensityState(std::vector<data_front> &datas_front, uint32_t reciever_id, double sender_position, double sender_speed, uint32_t id_sender, int at_sender)
{
  int num_ahead = 0;
  int num_back = 100;
  int sender_num;
  bool is_ahead_above;
  bool is_at;
  bool is_through_at_sender = false;
  bool is_under_density;
  bool is_abnormal = false;

  for (auto itr = datas_front.begin(); itr !=  datas_front.end(); itr++){
    is_ahead_above = (num_ahead <= itr->num_node) ? true : false;
    is_at = (itr - datas_front.begin() == at_sender) ? true : false;

    if (!is_through_at_sender) {
      if (!is_at && !is_ahead_above) {
        continue;

      } else if (!is_at && is_ahead_above) {
        num_ahead = itr->num_node;

      } else if (is_at && is_ahead_above){
        is_through_at_sender = true;
        is_under_density = false;
        sender_num = itr->num_node;

      } else if (is_at && !is_ahead_above) {
        is_through_at_sender = true;
        is_under_density = true;
        sender_num = itr->num_node;

      }
    } else {
      if (is_under_density && is_ahead_above){
        // num_back = itr->num_node - 2;
        // num_ahead = num_ahead - 2;
        // is_abnormal = (sender_num < num_ahead || num_back < sender_num) ? true : false;
        break;

      } else if (is_under_density && !is_ahead_above){
        continue;

      } else if(!is_under_density && is_ahead_above){
        num_back = itr->num_node;
        num_ahead = num_ahead;
        is_abnormal = (sender_num < num_ahead - 6 || num_back + 6 < sender_num) ? true : false;
        break;

      } else if(!is_under_density && !is_ahead_above){
        continue;

      }
    }
  }

  return (!is_abnormal) ? IS_VICIOUS : IS_MALICIOUS;
}

int
RoutingHelper::ApproveDivDensity(std::vector<data_front> &datas_front, uint32_t reciever_id, double sender_position, double sender_speed, uint32_t id_sender, int at_sender)
{
  int data_left = datas_front.size();
  // データがない時は検証できないので返す
  bool size_is_zero = (data_left == 0) ? true : false;

  if (size_is_zero){
    return IS_VICIOUS;
  } else {
    return RoutingHelper::ReturnDensityState(datas_front, reciever_id, sender_position, sender_speed, id_sender, at_sender);
  }

  return IS_VICIOUS;
}

int
RoutingHelper::ApproveDensity(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::vector<uint32_t> &sequence_from_front, std::vector<uint32_t> &sequence_from_back, std::unordered_map<uint32_t, received_message> tmp_data)
{
  std::vector<data_front> datas_front;
  std::vector<data_front> for_rornt;
  std::string addr_string;
  struct data_front tmp_data_front;
  double sum_flow_ave = 0;
  double flows = 0;
  double position_own = tmp_data[reciever_id].position;
  double front_range = position_own + (double)(TRANSMISSION_RANGE / 1000);
  double back_range = position_own - (double)(TRANSMISSION_RANGE / 1000);
  int num_node = 0;
  int count_malliciaus = 0;
  int count = 0;
  int at_sender;
  uint32_t id_sender = RoutingHelper::Return_Id_From_Ipv4(sender_address);

  int density_result = IS_VICIOUS;
  int flow_result = IS_VICIOUS;
  double density_est = 0.0;
  double speed_est = 0.0;
  double speeds = 0.0;
  double sum_speed = 0.0;

  //送信者が自分より前の場合
  if(position_own < sender_position){
    for (auto itr_f = sequence_from_front.begin(); itr_f != sequence_from_front.end(); itr_f++){
      count = 0;
      sum_flow_ave = 0;
      count_malliciaus = 0;
      sum_speed = 0;

      // // 送信者の時だけ実行
      // if(*itr_f != id_sender){
      //   continue;
      // }
      // 送信者の時だけ実行
      if(*itr_f == id_sender){
        at_sender = itr_f - sequence_from_front.begin();
      }

      // 位置が自分より後ろになったらブレイクする
      if(tmp_data[*itr_f].position < position_own){
        break;
      }

      for (auto itr = sequence_from_front.begin(); itr != sequence_from_front.end(); itr++){

        if(tmp_data[*itr_f].position - (double)(TRANSMISSION_RANGE / 1000) <= tmp_data[*itr].position && tmp_data[*itr].position <= front_range){
          addr_string = RoutingHelper::Return_Ipv4_string_From_Id(*itr);
          count++;
          sum_flow_ave += tmp_data[*itr].density * tmp_data[*itr].speed;
          sum_speed +=  tmp_data[*itr].speed;
        }
      }
      num_node = tmp_data[*itr_f].density + 1 - count;
      flows = (tmp_data[*itr_f].density + 1 - count_malliciaus) * tmp_data[*itr_f].flow_ave - sum_flow_ave;
      speeds = tmp_data[*itr_f].flow;
      tmp_data_front.flow_ave = (num_node != 0) ? flows / num_node : flows;
      tmp_data_front.num_node = num_node;
      tmp_data_front.position = tmp_data[*itr_f].position + (double)(TRANSMISSION_RANGE / 1000);
      for_rornt.push_back(tmp_data_front);

      // 送信者の時だけ実行
      if(*itr_f == id_sender){
        if (num_node > THRESHOLD_NUM) {
          // 速度の見積もり
          if (num_node != 0) {
            speed_est = (speeds - sum_speed) / num_node;
          } else {
            speed_est = 0;
          }
          // 車両密度の見積もり
          if (0.3 < fabs(sender_position - position_own) && fabs(sender_position - position_own) < 0.5) {
            density_est = num_node / fabs(sender_position - position_own);
            if (density_est < 200) {
              std::cout
                << "speed" << speed_est
                << "num_node " << num_node
                << "diff " << fabs(sender_position - position_own)
              << std::endl;
            }
          } else {
            density_est = 0;
          }
        } else {
          speed_est = 0;
          density_est = 0;
        }
      }

    }
  } else {  //送信者が自分より後ろの場合
    for (auto itr_b = sequence_from_back.begin(); itr_b !=  sequence_from_back.end(); itr_b++){
      count = 0;
      sum_flow_ave = 0;
      count_malliciaus = 0;
      sum_speed = 0;

      // // 送信者の時だけ実行
      // if(*itr_b != id_sender){
      //   continue;
      // }
      // 送信者の時だけ実行
      if(*itr_b == id_sender){
        at_sender = itr_b - sequence_from_back.begin();
      }

      // 位置が自分より後ろになったらブレイクする
      if(position_own < tmp_data[*itr_b].position){
        break;
      }

      for (auto itr = sequence_from_back.begin(); itr != sequence_from_back.end(); itr++){
        if(back_range <= tmp_data[*itr].position && tmp_data[*itr].position <= tmp_data[*itr_b].position + (double)(TRANSMISSION_RANGE / 1000)){
          addr_string = RoutingHelper::Return_Ipv4_string_From_Id(*itr);
          count++;
          sum_flow_ave += tmp_data[*itr].density * tmp_data[*itr].speed;
          sum_speed +=  tmp_data[*itr].speed;
        }
      }
      num_node = tmp_data[*itr_b].density + 1 - count;
      flows = (tmp_data[*itr_b].density + 1 - count_malliciaus) * tmp_data[*itr_b].flow_ave - sum_flow_ave;
      speeds = tmp_data[*itr_b].flow;
      tmp_data_front.flow_ave = (num_node != 0) ? flows / num_node : flows;
      tmp_data_front.num_node = num_node;
      tmp_data_front.position = tmp_data[*itr_b].position - (double)(TRANSMISSION_RANGE / 1000);
      for_rornt.push_back(tmp_data_front);

      // 送信者の時だけ実行
      if(*itr_b == id_sender){
        if (num_node > THRESHOLD_NUM) {
          // 速度の見積もり
          if (num_node != 0) {
            speed_est = (speeds - sum_speed) / num_node;
          } else {
            speed_est = 0;
          }
          // 車両密度の見積もり
          if (0.25 < fabs(sender_position - position_own) && fabs(sender_position - position_own) < 0.5) {
            density_est = num_node / fabs(sender_position - position_own);
            if (density_est < 200) {
              std::cout
                << "speed" << speed_est
                << "num_node " << num_node
                << "diff " << fabs(sender_position - position_own)
              << std::endl;
            }
          } else {
            density_est = 0;
          }
        } else {
          speed_est = 0;
          density_est = 0;
        }
      }

    }
  }
  // 順番を逆にしてあげる．
  for(int tmp_p = for_rornt.size() - 1; 0 <= tmp_p; tmp_p-- ){
      datas_front.push_back(for_rornt.at(tmp_p));
  }
  // 逆順に沿って位置を変える．
  at_sender = datas_front.size() - 1 -  at_sender;
  tmp[id_sender] = datas_front;

  density_result = RoutingHelper::ApproveDivDensity(datas_front, reciever_id, sender_position, sender_speed, id_sender, at_sender);

  // 車両密度を優先して結果を返す．
  if (density_result == IS_VICIOUS) {
    // return IS_VICIOUS;
    if ((int)speed_est != 0 && (int)density_est != 0) {
      flow_result = RoutingHelper::DecideToReceiveSpeed(reciever_id, sender_address, sender_flow, speed_est, density_est, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now);
      if (flow_result == IS_MALICIOUS) {
        std::cout
          << "speed " << speed_est
          << "density_est " << density_est
        << std::endl;
        // return IS_VICIOUS;
      }
    } else {
      flow_result = IS_VICIOUS;
    }
    return flow_result;
  } else if (density_result == IS_OBSERVED) {
    return IS_OBSERVED;
  } else if (density_result == IS_MALICIOUS) {
    return IS_MALICIOUS;
  } else {
    return IS_VICIOUS;
  }

  return IS_VICIOUS;
}

int
RoutingHelper::ApproveFlowAve(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now, std::vector<uint32_t> &sequence_from_front, std::vector<uint32_t> &sequence_from_back, std::unordered_map<uint32_t, received_message> tmp_data)
{
  // calculate an average of speed in estimate realm;
  std::vector<data_front> datas_front;
  std::vector<data_front> for_rornt;
  std::string addr_string;
  struct data_front tmp_data_front;
  double sum_flow_ave = 0;
  double sum_speed = 0;
  double density_est = 0;
  double speed_ave_est = 0;
  // double flows = 0;
  double speeds = 0;
  double position_own = tmp_data[reciever_id].position;
  // double front_range = position_own + (double)(TRANSMISSION_RANGE / 1000);
  // double back_range = position_own - (double)(TRANSMISSION_RANGE / 1000);
  int num_node = 0;
  // int count_malliciaus = 0;
  int count = 0;
  uint32_t id_sender = RoutingHelper::Return_Id_From_Ipv4(sender_address);

  //送信者が自分より前の場合
  if(position_own < sender_position){
    for (auto itr_f = sequence_from_front.begin(); itr_f != sequence_from_front.end(); itr_f++){
      count = 0;
      sum_flow_ave = 0;
      // count_malliciaus = 0;
      sum_speed = 0;

      // 送信者の時だけ実行
      if(*itr_f != id_sender){
        continue;
      }
      // // 送信者の時だけ実行
      // if(*itr_f == id_sender){
      //   at_sender = itr_f - sequence_from_front.begin();
      // }

      // 位置が自分より後ろになったらブレイクする
      if(tmp_data[*itr_f].position < position_own){
        break;
      }

      for (auto itr = sequence_from_front.begin(); itr != sequence_from_front.end(); itr++){
        if((tmp_data[*itr_f].position - ((double)TRANSMISSION_RANGE / 1000.0)) <= tmp_data[*itr].position && tmp_data[*itr].position <= tmp_data[*itr_f].position){
          addr_string = RoutingHelper::Return_Ipv4_string_From_Id(*itr);
          // std::cout << "mal_sp" << tmp_data[*itr].speed << std::endl << std::endl;
          count++;
          sum_flow_ave += tmp_data[*itr].density * tmp_data[*itr].speed;
          sum_speed += tmp_data[*itr].speed;
        }
      }
      num_node = tmp_data[*itr_f].density + 1 - count;
      // flows = (tmp_data[*itr_f].density + 1 - count_malliciaus) * tmp_data[*itr_f].flow_ave - sum_flow_ave;
      speeds = tmp_data[*itr_f].flow * (double)(TRANSMISSION_RANGE * 2.0 / 1000.0); // this means a sum of speed;
      // if(real_mallicias.find(addr_reciever_string) != real_mallicias.end()){
      //   if (ATTACKERS_PRIORITY == 0) {
      //     speeds = speeds * (double)FLOW_RATE_OF_MALLICIAS / 100.0;
      //   }
      // }
      // tmp_data_front.flow_ave = (num_node != 0) ? flows / num_node : flows;
      // tmp_data_front.num_node = num_node;
      // tmp_data_front.position = tmp_data[*itr_f].position + (double)(TRANSMISSION_RANGE / 1000);
      // for_rornt.push_back(tmp_data_front);
      if (num_node != 0) {
        speed_ave_est = (speeds - sum_speed) / num_node;
      } else {
        return IS_OBSERVED;
      }
      density_est = num_node / (double)(TRANSMISSION_RANGE / 1000);
      // speed_ave_est = (speeds - sum_speed) / num_node;
      tmp_data_front.flow_ave = speed_ave_est;

      // tmp_data_front.position = density_est;
      tmp_data_front.num_node = sender_density;
      datas_front.push_back(tmp_data_front);
    }


  } else {  //送信者が自分より後ろの場合
    for (auto itr_b = sequence_from_back.begin(); itr_b !=  sequence_from_back.end(); itr_b++){
      count = 0;
      sum_flow_ave = 0;
      // count_malliciaus = 0;
      sum_speed = 0;

      // 送信者の時だけ実行
      if(*itr_b != id_sender){
        continue;
      }
      // 送信者の時だけ実行
      // if(*itr_b == id_sender){
      //   at_sender = itr_b - sequence_from_back.begin();
      // }

      // 位置が自分より後ろになったらブレイクする
      if(position_own < tmp_data[*itr_b].position){
        break;
      }

      for (auto itr = sequence_from_back.begin(); itr != sequence_from_back.end(); itr++){
        if(tmp_data[*itr_b].position <= tmp_data[*itr].position && tmp_data[*itr].position <= tmp_data[*itr_b].position + ((double)TRANSMISSION_RANGE / 1000.0)){
          addr_string = RoutingHelper::Return_Ipv4_string_From_Id(*itr);
          count++;
          sum_flow_ave += tmp_data[*itr].density * tmp_data[*itr].speed;
          sum_speed += tmp_data[*itr].speed;
        }
      }
      num_node = tmp_data[*itr_b].density + 1 - count;
      // flows = (tmp_data[*itr_b].density + 1 - count_malliciaus) * tmp_data[*itr_b].flow_ave - sum_flow_ave;
      speeds = tmp_data[*itr_b].flow * (double)(TRANSMISSION_RANGE * 2.0 / 1000.0); // this means a sum of speed;
      // if(real_mallicias.find(addr_reciever_string) != real_mallicias.end()){
      //   if (ATTACKERS_PRIORITY == 0) {
      //     speeds = speeds * (double)FLOW_RATE_OF_MALLICIAS / 100.0;
      //   }
      // }
      // tmp_data_front.flow_ave = (num_node != 0) ? flows / num_node : flows;
      // tmp_data_front.position = tmp_data[*itr_b].position - (double)(TRANSMISSION_RANGE / 1000);
      if (num_node != 0) {
        speed_ave_est = (speeds - sum_speed) / num_node;
      } else {
        return IS_OBSERVED;
      }
      density_est = num_node / (double)(TRANSMISSION_RANGE / 1000);
      tmp_data_front.flow_ave = speed_ave_est;
      // tmp_data_front.position = density_est;
      tmp_data_front.num_node = sender_density;
      datas_front.push_back(tmp_data_front);
    }
  }

  if (fabs(sender_density - density_est) > 6) {
    return IS_OBSERVED;
  } else {
    tmp[id_sender] = datas_front;
    // for (int i = 0; i < 100000; i++){
    //   std::cout << "speed" << speed_ave_est << "density" << density_est << std::endl;
    // }
    if(real_mallicias.find(addr_reciever_string) != real_mallicias.end()){
      std::cout <<
        "speeds" << speeds << " " <<
        "sum_speed" << sum_speed << " " <<
        "num_node" << num_node << " " <<
        "sender_density" << num_node << " " << "giwaku" << (position_own - sender_position) <<
      std::endl;
    }
    return RoutingHelper::DecideToReceiveSpeed(reciever_id, sender_address, sender_flow, speed_ave_est, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now);
  }
}

int
RoutingHelper::ReturnMessageState(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now)
{
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();
  int real_message = IS_VICIOUS;

  //正規ノードか不正ノードかのステータス
  bool is_vicious_node = (RoutingHelper::FilterProperly(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position)) ? true : false;
  if (!is_vicious_node){
    return IS_MALICIOUS;
  }

  int message_statas = IS_VICIOUS;

  if (message_statas == IS_VICIOUS) {
    message_statas = RoutingHelper::DecideToReceiveDensity(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now);
  } else {
    return message_statas;
  }
  real_message = (message_statas != IS_VICIOUS) ? message_statas : real_message;

  if (message_statas == IS_VICIOUS) {
    message_statas = RoutingHelper::DecideToReceiveSpeed(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now);
  } else {
    return message_statas;
  }
  real_message = (message_statas != IS_VICIOUS) ? message_statas : real_message;

  // if (message_statas == IS_VICIOUS) {
  //   message_statas = RoutingHelper::DecideToReceiveFlowAve(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now);
  // } else {
  //   return message_statas;
  // }
  // real_message = (message_statas != IS_VICIOUS) ? message_statas : real_message;

  return real_message;
}

bool
RoutingHelper::ProposedAlgorthm (uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now)
{
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();
  int message_statas;

  //正規ノードか不正ノードかのステータス
  bool is_vicious_node = (RoutingHelper::FilterProperly(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position)) ? true : false;
  if (!is_vicious_node){
    return 0;
  }

  // is_EXECUTION_MODE_2のステータスで分岐
  bool is_accept_data_all_time = (EXECUTION_MODE == 2) ? true : false;
  if (is_accept_data_all_time) {
    message_statas = IS_VICIOUS;
  } else {
    message_statas = RoutingHelper::ReturnMessageState(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now);
  }

  if (message_statas == IS_MALICIOUS) {
    result_mallicias[addr] = 1;
  } else if (message_statas == IS_VICIOUS) {
    RoutingHelper::AcceptData(reciever_id, sender_address, sender_flow , sender_flow_ave, sender_speed, sender_density, now, sender_position);
  } else if (message_statas == IS_OBSERVED) {

  }

  return true;
}

int
RoutingHelper::DecideToReceiveFlowAve(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag, double now)
{
  // double reciever_flow = own_received_messages[reciever_id][reciever_id].flow;
  //周りの状況が似ている時
  if(false){
    // std::cout << "sender_flow_ave = " << sender_flow_ave << " reciever_flow_ave = " << reciever_flow_ave << std::endl;
    return IS_VICIOUS;
  } else { //周りの状況が異なる時
    uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
    double form_speed = own_received_messages[reciever_id][id_sender].speed;
    double form_time = own_received_messages[reciever_id][id_sender].received_time;
    double diff_time = now - form_time;
    double diff_speed = sender_speed - form_speed;

    std::unordered_map<uint32_t, received_message> tmp_data = own_received_messages[reciever_id];
    tmp_data[id_sender].received_time = now;
    tmp_data[id_sender].position = sender_position;
    tmp_data[id_sender].acceleration = diff_speed / diff_time;
    tmp_data[id_sender].density = sender_density;
    tmp_data[id_sender].speed = sender_speed;
    tmp_data[id_sender].flow_ave = sender_flow_ave;
    tmp_data[id_sender].flow = sender_flow;

    std::vector<uint32_t> sequence_from_front = RoutingHelper::ReturnSequenceFromFront(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now, tmp_data);
    std::vector<uint32_t> sequence_from_back = RoutingHelper::ReturnSequenceFromBack(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now, tmp_data);

    tmp_f[id_sender] = sequence_from_front;
    tmp_b[id_sender] = sequence_from_back;

    return RoutingHelper::ApproveFlowAve(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now, sequence_from_front, sequence_from_back, tmp_data);
  }

  return IS_VICIOUS;
}

int
RoutingHelper::ProposedScheme(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, int sender_hop_flag)
{
  // uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();
  double now = Simulator::Now ().GetSeconds ();
  access_log_for_IAR[reciever_id][addr] = now;

  //正規ノードか不正ノードかのステータス
  bool is_vicious_node = (RoutingHelper::FilterProperly(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position)) ? true : false;
  if (!is_vicious_node){
    return 0;
  }

  //density_flagのステータスを取得
  bool is_calc_density_flag_up = (my_data[reciever_id].calc_density_flag == 1) ? true : false;
  if (is_calc_density_flag_up) {
    RoutingHelper::ReadyForConventional(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, now);
  } else {
    RoutingHelper::ProposedAlgorthm(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, sender_hop_flag, now);
  }

  // 監視ノードが存在するかのステータス
  bool is_empty_observed_node = false;
  if (is_empty_observed_node) {

  }

  //flow_aveの更新
  RoutingHelper::UpdateFlowAVG(reciever_id, sender_address, sender_flow, sender_speed);
  return 0;
}

int
RoutingHelper::ReadyForConventional(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now)
{
  RoutingHelper::AcceptData(reciever_id, sender_address, sender_flow, sender_flow_ave, sender_speed, sender_density , now, sender_position);
  //十分にデータが集まったら密度を更新
  if(now - my_data[reciever_id].log_time_first >= INTER_ARRIVAL_RATE){
    RoutingHelper::UpdateByInterAreaRate(reciever_id, sender_address, sender_flow, sender_speed);
    RoutingHelper::UpdateSpeedAVG(reciever_id, sender_address, sender_flow, sender_speed);

    RoutingHelper::UpdateFlowAVG(reciever_id, sender_address, sender_flow, sender_speed);
    my_data[reciever_id].flow_own = my_data[reciever_id].speed_ave * my_data[reciever_id].density_calc;
    my_data[reciever_id].log_time_first = now;
    //　更新時間がすぎたらフラグを外し返信をするようにする
    if(my_data[reciever_id].log_time_first >= START_TIME){
      RoutingHelper::UpdateVfAndKj(reciever_id, sender_address, sender_flow, sender_speed);
      RoutingHelper::DecideThreshold(reciever_id);
      // シミュレーション開始時間になったらフラグを下げる
      my_data[reciever_id].calc_density_flag = 0;
    }
  }

  return 0;
}

bool
RoutingHelper::is_matched_model(double sender_speed, double sender_density, double sender_flow){
  // this algorithm verifies the informations is matched to the model;
  // if de difference between received frow and the one which is calculated by speed and density is out of threshold, this render is judged as mallicious;
  double calc_flow = sender_speed * sender_density;
  bool is_matched = (fabs(sender_flow - calc_flow) < THRESHOLD) ? true : false;

  return is_matched;
}

void
RoutingHelper::verifies_similarity(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now)
{
  std::stringstream ss_sender;
  ss_sender << sender_address;
  std::string sender_string = ss_sender.str();
  uint32_t id_sender = id_from_Ipv4adress[sender_string];
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();

  // 平均速度と自身のflowを更新
  RoutingHelper::UpdateSpeedAVG(reciever_id, sender_address, sender_flow, sender_speed);
  my_data[reciever_id].flow_own = my_data[reciever_id].speed_ave * my_data[reciever_id].density_calc;

  // 閾値による判定
  if(fabs(sender_flow - my_data[reciever_id].flow_recv) < THRESHOLD){
    // std::cout << "THRESHOLD = " << THRESHOLD << "diff = " << fabs(sender_flow - my_data[reciever_id].flow_recv)<< std::endl;
    // // シミュレーション用に扱うデータにフィルタをかける
    // if(RoutingHelper::FilterProperly(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string) == false){
    //   return 0;
    // }
    // 閾値以内なら取得
    RoutingHelper::AcceptData(reciever_id, sender_address, sender_flow, sender_flow_ave, sender_speed,sender_density, now, sender_position);
    RoutingHelper::UpdateFlowAVG(reciever_id, sender_address, sender_flow, sender_speed);
  } else if(RoutingHelper::IsObserved(id_sender, reciever_id) == false){
    // 監視ノードになければ監視ノードとして登録
    RoutingHelper::PushAsObservedNode(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, now);
  }

  // 監視ノードがあれば統計処理で判定する
  if(observed_nodes[reciever_id].empty() == false){
    // 観測ノードのIDを宣言
    uint32_t id_observed;
    for(auto itr = observed_nodes[reciever_id].begin(); itr != observed_nodes[reciever_id].end(); ++itr){
      std::stringstream ss_observed;
      ss_observed << itr->observed_address;
      std::string addr_observed = ss_observed.str();
      id_observed = itr->id;

      // 監視フラグが立っているものだけ統計処理
      if(itr->is_observed_flag == 1){

        //観測者が送信者自身である場合，もしくは送信者が観測されていない場合に限りデータを保存
        if(RoutingHelper::DecidePushDataForObservedNode(id_observed, reciever_id, sender_address, sender_flow, sender_speed, now)){
          struct viecle_data push_viecle_data = {now, sender_flow / VALUE_RATE, sender_speed};
          itr->tmp_v.push_back(push_viecle_data);
        }
        //
        if(/*RoutingHelper::DecidePushDataForCalcdNode(id_observed, reciever_id, sender_address, sender_flow, sender_speed, now)*/true){
          double calc_speed = my_data[reciever_id].speed_ave;
          double calc_flow = my_data[reciever_id].flow_recv;
          struct calc_data push_calc_data = {now, calc_flow / VALUE_RATE, calc_speed};
          itr->tmp_c.push_back(push_calc_data);
        }
        std::cout <<
          "THRESHOLD = " << THRESHOLD <<
          "diff = " << fabs(sender_flow - my_data[reciever_id].flow_recv) <<
          "observed_sample = " << itr->tmp_v.size() <<
          "calc_sample = " << itr->tmp_c.size() <<
        std::endl;
        // 十分なサンプルサイズが取得できれば統計処理を行う
        if((itr->tmp_v.size() > T_SAMPLES) && (itr->tmp_c.size() > (T_SAMPLES))){
          // 統計処理を行い，受理と棄却を判定
          if(RoutingHelper::StaticAnalisis(itr->tmp_v, itr->tmp_c, id_observed, reciever_id, sender_address, sender_flow, sender_speed) == true){
            RoutingHelper::AcceptData(reciever_id, sender_address, sender_flow, sender_flow_ave, sender_speed, sender_density, now, sender_position);
            RoutingHelper::UpdateFlowAVG(reciever_id, sender_address, sender_flow, sender_speed);
            itr->is_observed_flag = 0;
            // itr = observed_nodes[reciever_id].erase(itr);
            // itr--;
          } else {
            result_mallicias[addr_observed] = 1;
            itr->is_observed_flag = 0;
          }
        }
      }
    }
  }
}

int
RoutingHelper::ConventionalAlgorithm(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now)
{
  // this algorithm verifies the relations of speed, density and flow;
  // and also verifies a received flow is similar with the ones of neighbor nodes;
  std::stringstream ss_sender;
  ss_sender << sender_address;
  std::string sender_string = ss_sender.str();
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();

  if (!RoutingHelper::is_matched_model(sender_speed, sender_density, sender_flow)){
    result_mallicias[sender_string] = 1;
    return 0;
  }
  RoutingHelper::verifies_similarity(reciever_id,  sender_address,  sender_flow,  sender_speed,  sender_density,  sender_flow_ave, addr_reciever_string,  sender_position,  now);

  return 0;
}

int
RoutingHelper::ConventionalScheme(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position)
{
  double now = Simulator::Now ().GetSeconds ();
  now_in_simulation = now;
  std::stringstream ss_sender;
  ss_sender << sender_address;
  std::string sender_string = ss_sender.str();

  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();
  access_log_for_IAR[reciever_id][addr] = now;
  // //ログが空の時には最初の時間を保存
  // if(access_log[reciever_id].empty()){
  //   my_data[reciever_id].log_time_first = now;
  // }
  if(my_data[reciever_id].calc_density_flag == 1){
    // シミュレーション開始までの準備を行う
    RoutingHelper::ReadyForConventional(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, now);
  } else {
    // 密度の更新
    if(now - my_data[reciever_id].log_time_first >= INTER_ARRIVAL_RATE){
      RoutingHelper::UpdateByInterAreaRate(reciever_id, sender_address, sender_flow, sender_speed);
      my_data[reciever_id].log_time_first = Simulator::Now ().GetSeconds ();
    }
    // シミュレーション用に扱うデータにフィルタをかける
    if(RoutingHelper::FilterProperly(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position) == false){
      return 0;
    }

    // アルゴリズム開始
    RoutingHelper::ConventionalAlgorithm(reciever_id, sender_address, sender_flow, sender_speed, sender_density, sender_flow_ave, addr_reciever_string, sender_position, now);
  }
  return 0;
}

bool
RoutingHelper::DecidePushDataForCalcdNode(uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double now) {

  std::stringstream ss_sender;
  ss_sender << sender_address;
  std::string sender_string = ss_sender.str();
  uint32_t id_sender = id_from_Ipv4adress[sender_string];
  bool decider = true;

  // 送信者と観測者のidが等しい場合はtrueを返す
  if(id_observed == id_sender){
    decider = false;
  } else if(result_mallicias.find(sender_string) != result_mallicias.end()){
    // すでに攻撃者と認識されている場合は受理しない
    decider = false;
  } else {
    // そうでない場合，送信者が観測されている場合はfalseをdeciderに代入
    for(auto itr = observed_nodes[reciever_id].begin(); itr != observed_nodes[reciever_id].end(); ++itr){
      if((itr->is_observed_flag == 1) && (itr->id == id_sender)){
        decider = false;
      }
    }
  }

  return decider;
}


bool
RoutingHelper::DecidePushDataForObservedNode(uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double now){
  std::stringstream ss_sender;
  ss_sender << sender_address;
  std::string sender_string = ss_sender.str();
  uint32_t id_sender = id_from_Ipv4adress[sender_string];
  bool decider = true;

  // 送信者と観測者のidが等しい場合はtrueを返す
  if(id_observed == id_sender){
    decider = true;
  } else if(result_mallicias.find(sender_string) != result_mallicias.end()){
    // すでに攻撃者と認識されている場合は受理しない
    decider = false;
  } else {
    // そうでない場合，送信者が観測されている場合はfalseをdeciderに代入
    for(auto itr = observed_nodes[reciever_id].begin(); itr != observed_nodes[reciever_id].end(); ++itr){
      if((itr->is_observed_flag == 1) && (itr->id == id_sender)){
        decider = false;
      } else {
        continue;
      }
    }

    // 監視ノード以外はデータとして保存しない場合にコメントを外す．
    if (ONLY_OBSEREVED_NODE_HAVE_TO_BE_COLLECTED) {
      decider = false;
    }
  }

  return decider;
}
// int
// RoutingHelper::PushDataForObservedNode(auto itr_observed, uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, double now){
//   //観測者が送信者自身である場合，もしくは送信者が観測されていない場合に限りデータを保存
//   if(RoutingHelper::DecidePushDataForObservedNode(id_observed, reciever_id, sender_address, sender_flow, sender_speed, now)){
//     double density = sender_density;
//     double calc_speed = my_data[reciever_id].Vf - (density / my_data[reciever_id].Kj) * my_data[reciever_id].Vf;
//     double calc_flow = density * calc_speed;
//     struct viecle_data push_viecle_data = {now, sender_flow_ave, sender_speed};
//     itr_observed->tmp_v.push_back(push_viecle_data);
//     struct calc_data push_calc_data = {now, calc_flow, calc_speed};
//     itr_observed->tmp_c.push_back(push_calc_data);
//   }
//
//   return 0;
// }

void
RoutingHelper::DecideThreshold(uint32_t reciever_id)
{
  double iqr_flow = 0;
  double third_quartile_flow;
  double first_quartile_flow;
  double diff_flow_ave = 0;
  double max_diff = 0;
  bool is_median;
  std::string addr;
  // std::vector<double> diff_ave_owns;
  std::vector<double> flows;

  // collect flows.
  for (uint32_t id_node = 0; id_node < NUM_OF_NODES; id_node++){
    switch (EXECUTION_MODE) {
      case 0:
      case 3:
        flows.push_back(access_flow_log[reciever_id][addr]);
        break;

      case 1:
      case 2:
        flows.push_back(own_received_messages[reciever_id][id_node].flow);
        break;
    }

  }
  // remove obnormal values
  std::sort(flows.begin(), flows.end());
  third_quartile_flow = flows.at((int)(flows.size() * 0.75));
  first_quartile_flow = flows.at((int)(flows.size() * 0.25));
  iqr_flow = third_quartile_flow - first_quartile_flow;

  //collect differences between flow_ave and flow_own;
  for (uint32_t id_node = 0; id_node < NUM_OF_NODES; id_node++){
    addr = Return_Ipv4_string_From_Id(id_node);
    switch (EXECUTION_MODE) {
      case 0:
      case 3:
        is_median =
          (first_quartile_flow - 1.5 * iqr_flow < access_flow_log[reciever_id][addr] && access_flow_log[reciever_id][addr] < third_quartile_flow + 1.5 * iqr_flow) ?
          true : false;
        if (is_median){
          diff_flow_ave = fabs(my_data[reciever_id].flow_recv - access_flow_log[reciever_id][addr]);
          max_diff = (max_diff < diff_flow_ave) ? diff_flow_ave : max_diff;
          // diff_ave_owns.push_back(diff_flow_ave);
        }
        break;

      case 1:
      case 2:
        is_median =
          (first_quartile_flow - 1.5 * iqr_flow < own_received_messages[reciever_id][id_node].flow && own_received_messages[reciever_id][id_node].flow < third_quartile_flow + 1.5 * iqr_flow) ?
          true : false;
        if (is_median){
          diff_flow_ave = fabs(my_data[reciever_id].flow_recv - own_received_messages[reciever_id][id_node].flow);
          max_diff = (max_diff < diff_flow_ave) ? diff_flow_ave : max_diff;
          // diff_ave_owns.push_back(diff_flow_ave);
        }
        break;
    }
  }

  THRESHOLD = (THRESHOLD < max_diff) ? max_diff : THRESHOLD;
}

int
RoutingHelper::UpdateVfAndKj(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed){
  double b = 0;
  double a = 0;
  int count = 0;
  std::unordered_map<std::string, double> tmp_access_log = access_log[reciever_id];
  std::unordered_map<std::string, double> tmp_access_speed_log = access_speed_log[reciever_id];
  std::unordered_map<std::string, double> tmp_access_density_log = access_density_log[reciever_id];
  double mean_density = 0;
  double mean_speed = 0;
  double sum_density = 0;
  double sum_speed = 0;
  double sq2_density = 0;
  double sum_cross = 0;
  double density_diff_from_ave = 0;
  double speed_diff_from_ave = 0;
  double now = Simulator::Now ().GetSeconds ();

  // 平均を算出
  for(auto itr = tmp_access_log.begin(); itr != tmp_access_log.end(); ++itr) {
    if((now - itr->second <= INTER_ARRIVAL_RATE) && (tmp_access_speed_log[itr->first] != 0)){
      sum_density += tmp_access_density_log[itr->first];
      sum_speed += tmp_access_speed_log[itr->first];
      count++;
    }
  }
  mean_density = (count != 0) ? (sum_density / count) : my_data[reciever_id].density_calc;
  mean_speed = (count != 0) ? (sum_speed / count) : my_data[reciever_id].speed_ave;

  // 各種必要な値を算出(異常値は取らないようにする)
  for(auto itr = tmp_access_log.begin(); itr != tmp_access_log.end(); ++itr) {
    density_diff_from_ave = tmp_access_density_log[itr->first] - mean_density;
    speed_diff_from_ave = tmp_access_speed_log[itr->first] - mean_speed;

    if((now - itr->second <= INTER_ARRIVAL_RATE) && (tmp_access_speed_log[itr->first] != 0) && (density_diff_from_ave * speed_diff_from_ave <= 0)) {
      sum_cross += density_diff_from_ave * speed_diff_from_ave;
      sq2_density += density_diff_from_ave * density_diff_from_ave;
    }
    else {
    }
  }

  b = (sq2_density != 0) ? (sum_cross / sq2_density) : 0;
  a = mean_speed - (b * mean_density);
  my_data[reciever_id].Vf = a;
  my_data[reciever_id].Kj = (b != 0) ? (-1 * (a / b)) : my_data[reciever_id].density_calc * 100;

  return 0;
}

std::vector<uint32_t>
RoutingHelper::ReturnPosSequence(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_flow_ave, double sender_speed, double sender_density, double now, double sender_position)
{
  std::vector<uint32_t> pos_sequence;

  return pos_sequence;
}

int
RoutingHelper::AcceptAsObserved(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_flow_ave, double sender_speed, double sender_density, double now, double sender_position)
{
  uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
  double form_speed = own_received_messages[reciever_id][id_sender].speed;
  double form_time = own_received_messages[reciever_id][id_sender].received_time;
  double diff_time = now - form_time;
  double diff_speed = sender_speed - form_speed;
  struct observed_node_chain tmp_for_push;
  struct received_message observed_node;

  observed_node.received_time = now;
  observed_node.position = sender_position;
  observed_node.acceleration = diff_speed / diff_time;
  observed_node.density = sender_density;
  observed_node.speed = sender_speed;
  observed_node.flow_ave = sender_flow_ave;

  tmp_for_push.is_observed_flag = 1;
  tmp_for_push.id = id_sender;
  tmp_for_push.itr_sequence = 0;
  tmp_for_push.observed_node = observed_node;
  tmp_for_push.pos_sequence = RoutingHelper::ReturnPosSequence(reciever_id, sender_address, sender_flow, sender_flow_ave, sender_speed, sender_density ,now, sender_position);
  tmp_for_push.received_message_at_accident = own_received_messages[reciever_id];

  observed_node_chains[reciever_id].push_back(tmp_for_push);

  return 0;
}

int
RoutingHelper::AcceptData(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_flow_ave, double sender_speed, double sender_density, double now, double sender_position)
{
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();

  access_log[reciever_id][addr] = now;
  access_speed_log[reciever_id][addr] = sender_speed;
  access_density_log[reciever_id][addr] = sender_density;

  switch (EXECUTION_MODE) {
    case 0:
      access_flow_log[reciever_id][addr] = sender_flow;
      break;

    case 1:
    case 2:
      {
        uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
        double form_speed = own_received_messages[reciever_id][id_sender].speed;
        double form_time = own_received_messages[reciever_id][id_sender].received_time;
        double diff_time = now - form_time;
        double diff_speed = sender_speed - form_speed;

        own_received_messages[reciever_id][id_sender].received_time = now;
        own_received_messages[reciever_id][id_sender].position = sender_position;
        own_received_messages[reciever_id][id_sender].acceleration = diff_speed / diff_time;
        own_received_messages[reciever_id][id_sender].density = sender_density;
        own_received_messages[reciever_id][id_sender].speed = sender_speed;
        own_received_messages[reciever_id][id_sender].flow_ave = sender_flow_ave;
        own_received_messages[reciever_id][id_sender].flow = sender_flow;

        // 更新の後にFlowAveを保存しておく
        RoutingHelper::SaveFlowAve(reciever_id, now);
      }
      break;

    case 3:
      access_flow_log[reciever_id][addr] = sender_flow_ave;
      break;
  }

  return 0;
}
int
RoutingHelper::SaveFlowAve(uint32_t reciever_id, double now)
{
  struct observed_flow_ave tmp;
  if(now < time_count[reciever_id]){
    return 0;
  }
  time_count[reciever_id]++;

  // 観測者と送信者が同じか判定
  if(result_mallicias.find(RoutingHelper::Return_Ipv4_string_From_Id(reciever_id)) != result_mallicias.end()){
    return 0;
  }

  tmp.time = Simulator::Now ().GetSeconds ();
  tmp.flow_ave = own_received_messages[reciever_id][reciever_id].flow_ave;
  observed_flow_aves.push_back(tmp);

  return 0;
}

bool
RoutingHelper::FilterProperly(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position){
  // 送信者がすでに攻撃者と判定されている場合は密度だけ計算して以後は評価しない
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();

  if (EXECUTION_MODE == 1) {
    if(result_mallicias.find(addr) != result_mallicias.end()){
      uint32_t id_sender = Return_Id_From_Ipv4(sender_address);
      own_received_messages[reciever_id][id_sender].position = sender_position;
      // std::cout << "Reject data" << std::endl;
      return false;
    }
  }

  if(EXECUTION_MODE == 0 || EXECUTION_MODE == 3){
    // 送信者のフロウが０の場合は何もしない
    if(sender_flow == 0){
      return false;
    }
    // 送信者のスピードが０の場合何もしない
    if(sender_speed == 0){
      return false;
    }
    // // 受信者が攻撃者である場合は何もしない
    // if(real_mallicias.find(addr_reciever_string) != real_mallicias.end()){
    //   return false;
    // }
  }

  return true;
}

int
RoutingHelper::PushAsObservedNode(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed, double sender_density, double sender_flow_ave, std::string addr_reciever_string, double sender_position, double now)
{
  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();
  uint32_t id_observed = id_from_Ipv4adress[addr];
  std::vector<viecle_data> v_init;
  std::vector<calc_data> c_init;

  struct observed_node push_observed_node = {1, id_observed, sender_address, sender_flow, sender_speed, now, v_init, c_init};
  observed_nodes[reciever_id].push_back(push_observed_node);

  return 0;
}

bool
RoutingHelper::IsObserved(uint32_t id_sender, uint32_t reciever_id){
  uint32_t id_observed;
  bool decider = false;

  // 監視ノードがあるかないかを確認
  if(observed_nodes[reciever_id].empty() != true){
    for(auto itr = observed_nodes[reciever_id].begin(); itr != observed_nodes[reciever_id].end(); ++itr){
      std::stringstream ss_observed;
      ss_observed << itr->observed_address;
      std::string addr_observed = ss_observed.str();
      id_observed = itr->id;

      // すでに送信者のIDが監視されている場合は追加しない
      if(itr->is_observed_flag == 1 && id_observed == id_sender){
        decider = true;
        break;
      }
    }
  }

  return decider;
}

bool
RoutingHelper::StaticAnalisis(std::vector<viecle_data> &ob_v_dates, std::vector<calc_data> &ob_c_dates, uint32_t id_observed, uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed){
  std::vector<viecle_data> tmp_viecle_dates = ob_v_dates;
  std::vector<calc_data> tmp_calc_dates = ob_c_dates;

  double mean_viecle_datas = 0;
  double mean_calc_datas = 0;
  double sd_viecle = 0;
  double sd_calc = 0;
  double t = 0;
  // double tmp = 0;

  std::stringstream ss;
  ss << sender_address;
  std::string addr = ss.str();

  // 平均を算出
  for(auto itr = tmp_viecle_dates.begin(); itr != tmp_viecle_dates.end(); ++itr) {
    mean_viecle_datas += (itr->flow_ave / tmp_viecle_dates.size());
  }
  for(auto itr = tmp_calc_dates.begin(); itr != tmp_calc_dates.end(); ++itr) {
    mean_calc_datas += (itr->flow_ave / tmp_calc_dates.size());
  }

  // 平均差の和と2乗平均和を算出
  for(auto itr = tmp_viecle_dates.begin(); itr != tmp_viecle_dates.end(); ++itr) {
    sd_viecle += (itr->flow_ave - mean_viecle_datas) * (itr->flow_ave - mean_viecle_datas) / (tmp_viecle_dates.size() - 1);
  }

  for(auto itr = tmp_calc_dates.begin(); itr != tmp_calc_dates.end(); ++itr) {
    // tmp = sd_calc;
    sd_calc += (itr->flow_ave - mean_calc_datas) * (itr->flow_ave - mean_calc_datas) / (tmp_calc_dates.size() - 1);
  }

  // t値の計算し判定, マリシャスなら０を返す
  t = (sqrt((sd_viecle / tmp_viecle_dates.size()) + (sd_calc / tmp_calc_dates.size())) >= 0.001) ?
    fabs(mean_viecle_datas - mean_calc_datas) / sqrt((sd_viecle / tmp_viecle_dates.size()) + (sd_calc / tmp_calc_dates.size())) :
    fabs(mean_viecle_datas - mean_calc_datas) / sqrt((sd_viecle / tmp_viecle_dates.size()) + (sd_calc / tmp_calc_dates.size()));

  // if(T_VALUE < t){
  //   for (int i = 0; i < 100000; i++){
  //     std::cout << "I'm "<< addr
  //     << " flow " << sender_flow
  //     << " t = " << t
  //     << " diff = " << fabs(mean_viecle_datas - mean_calc_datas) << " shita = " << sqrt((sd_viecle / T_SAMPLES) + (sd_calc / T_SAMPLES))
  //     << " s2_viecle = " << sd_viecle
  //     << " s2_calc = " << sd_calc
  //     << " num_viecle = " << T_SAMPLES
  //     << " num_calc = " << T_SAMPLES
  //     << " mean_viecle_datas = " << mean_viecle_datas
  //     << " mean_calc_datas = " << mean_calc_datas
  //     // << " sd_viecle = " << sd_viecle
  //     // << " sd_calc = " << sd_calc
  //     << std::endl;
  //   }
  // }

  if(T_VALUE < t){
    my_data[id_observed].t = t;
    my_data[id_observed].id_judger = reciever_id;
    my_data[id_observed].sq2_viecle = sd_viecle;
    my_data[id_observed].sq2_calc = sd_calc;
    return false;
  } else {
    my_data[id_observed].t = t;
    my_data[id_observed].id_judger = reciever_id;
    my_data[id_observed].sq2_viecle = sd_viecle;
    my_data[id_observed].sq2_calc = sd_calc;
    return true;
  }
}

double
RoutingHelper::UpdateByInterAreaRate(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed){
  int count = 0;

  for (auto itr = access_log_for_IAR[reciever_id].begin(); itr != access_log_for_IAR[reciever_id].end(); ++itr){
    if(Simulator::Now ().GetSeconds () - itr->second <= INTER_ARRIVAL_RATE){
      count++;
    }
  }

  // モードで必要な値を保存
  switch (EXECUTION_MODE) {
    case 1:
    case 2:
      own_received_messages[reciever_id][reciever_id].density = count / (TRANSMISSION_RANGE * 2 / 1000);
      break;
  }

  my_data[reciever_id].density_calc = count / (TRANSMISSION_RANGE * 2 / 1000);
  return my_data[reciever_id].density_calc;
}

double
RoutingHelper::UpdateSpeedAVG(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed){
  double total_speed = 0;

  int count = 0;
  std::unordered_map<std::string, double> access_log_tmp = access_log[reciever_id];
  std::unordered_map<std::string, double> access_speed_log_tmp = access_speed_log[reciever_id];
  // １秒以内にログがあったデータならば速度に加算
  for(auto itr = access_log_tmp.begin(); itr != access_log_tmp.end(); ++itr) {
    if((Simulator::Now ().GetSeconds () - itr->second <= INTER_ARRIVAL_RATE) && (access_speed_log_tmp[itr->first] != 0)){
      total_speed += access_speed_log_tmp[itr->first];
      count++;
    }
  }

  if (count == 0){
    my_data[reciever_id].speed_ave = 0;
  } else {
    my_data[reciever_id].speed_ave = total_speed / count;
  }

  // flow_ownの頻度を保存
  if(START_TIME < Simulator::Now ().GetSeconds ()){
    double flow_own = my_data[reciever_id].speed_ave * my_data[reciever_id].density_calc;
    save_frequency(flow_own, flow_own_frequency);
  }

  return my_data[reciever_id].speed_ave;
}

int
RoutingHelper::UpdateFlowAVG(uint32_t reciever_id, Ipv4Address sender_address, double sender_flow, double sender_speed)
{
  double total_flow = 0;
  double now = Simulator::Now ().GetSeconds ();
  int count = 0;

  switch (EXECUTION_MODE) {
    case 0:
    case 3:
      {
        std::unordered_map<std::string, double> access_log_tmp = access_log[reciever_id];
        std::unordered_map<std::string, double> access_flow_log_tmp = access_flow_log[reciever_id];
        // １秒以内にログがあったデータかつ値が０以外ならば速度に加算
        for(auto itr = access_log_tmp.begin(); itr != access_log_tmp.end(); ++itr) {
          if((now - itr->second <= INTER_ARRIVAL_RATE) && (access_flow_log_tmp[itr->first] != 0)){
            total_flow += access_flow_log_tmp[itr->first];
            count++;
          }
        }
      }
      break;

    case 1:
    case 2:
      {
        std::unordered_map<uint32_t, struct received_message> tmp_received_message = own_received_messages[reciever_id];
        // １秒以内にログがあったデータかつ値が０以外ならば速度に加算
        for(auto itr = tmp_received_message.begin(); itr != tmp_received_message.end(); ++itr) {
          if((now - itr->second.received_time <= INTER_ARRIVAL_RATE)){
            total_flow += itr->second.flow;
            count++;
          }
        }
      }
      break;
  }

  if (count == 0){
    own_received_messages[reciever_id][reciever_id].flow_ave = 0;
    my_data[reciever_id].flow_recv = 0;
  } else {
    own_received_messages[reciever_id][reciever_id].flow_ave = total_flow / count;
    my_data[reciever_id].flow_recv = total_flow / count;
  }

  // flow_ave(flow_global)の頻度を保存
  if(START_TIME < Simulator::Now ().GetSeconds ()){
    double flow_global = my_data[reciever_id].flow_recv;
    save_frequency(flow_global, flow_ave_frequency);
  }

  return 0;
}
void
RoutingHelper::OnOffTrace (std::string context, Ptr<const Packet> packet)
{
  uint32_t pktBytes = packet->GetSize ();
  routingStats.IncTxBytes (pktBytes);
}

RoutingStats &
RoutingHelper::GetRoutingStats ()
{
  return routingStats;
}

void
RoutingHelper::SetLogging (int log)
{
  m_log = log;
}

/**
 * \ingroup wave
 * \brief The WifiPhyStats class collects Wifi MAC/PHY statistics
 */
class WifiPhyStats : public Object
{
public:
  /**
   * \brief Gets the class TypeId
   * \return the class TypeId
   */
  static TypeId GetTypeId (void);

  /**
   * \brief Constructor
   * \return none
   */
  WifiPhyStats ();

  /**
   * \brief Destructor
   * \return none
   */
  virtual ~WifiPhyStats ();

  /**
   * \brief Returns the number of bytes that have been transmitted
   * (this includes MAC/PHY overhead)
   * \return the number of bytes transmitted
   */
  uint32_t GetTxBytes ();

  /**
   * \brief Callback signiture for Phy/Tx trace
   * \param context this object
   * \param packet packet transmitted
   * \param mode wifi mode
   * \param preamble wifi preamble
   * \param txPower transmission power
   * \return none
   */
  void PhyTxTrace (std::string context, Ptr<const Packet> packet, WifiMode mode, WifiPreamble preamble, uint8_t txPower);

  /**
   * \brief Callback signiture for Phy/TxDrop
   * \param context this object
   * \param packet the tx packet being dropped
   * \return none
   */
  void PhyTxDrop (std::string context, Ptr<const Packet> packet);

  /**
   * \brief Callback signiture for Phy/RxDrop
   * \param context this object
   * \param packet the rx packet being dropped
   * \return none
   */
  void PhyRxDrop (std::string context, Ptr<const Packet> packet);

private:
  uint32_t m_phyTxPkts;
  uint32_t m_phyTxBytes;
};

NS_OBJECT_ENSURE_REGISTERED (WifiPhyStats);

TypeId
WifiPhyStats::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::WifiPhyStats")
    .SetParent<Object> ()
    .AddConstructor<WifiPhyStats> ();
  return tid;
}

WifiPhyStats::WifiPhyStats ()
  : m_phyTxPkts (0),
    m_phyTxBytes (0)
{
}

WifiPhyStats::~WifiPhyStats ()
{
}

void
WifiPhyStats::PhyTxTrace (std::string context, Ptr<const Packet> packet, WifiMode mode, WifiPreamble preamble, uint8_t txPower)
{
  NS_LOG_FUNCTION (this << context << packet << "PHYTX mode=" << mode );
  ++m_phyTxPkts;
  uint32_t pktSize = packet->GetSize ();
  m_phyTxBytes += pktSize;

  //NS_LOG_UNCOND ("Received PHY size=" << pktSize);
}

void
WifiPhyStats::PhyTxDrop (std::string context, Ptr<const Packet> packet)
{
  NS_LOG_UNCOND ("PHY Tx Drop");
}

void
WifiPhyStats::PhyRxDrop (std::string context, Ptr<const Packet> packet)
{
  NS_LOG_UNCOND ("PHY Rx Drop");
}

uint32_t
WifiPhyStats::GetTxBytes ()
{
  return m_phyTxBytes;
}

/**
 * \ingroup wave
 * \brief The WifiApp class enforces program flow for ns-3 wifi applications
 */
class WifiApp
{
public:
  /**
   * \brief Constructor
   * \return none
   */
  WifiApp ();

  /**
   * \brief Destructor
   * \return none
   */
  virtual ~WifiApp ();

  /**
   * \brief Enacts simulation of an ns-3 wifi application
   * \param argc program arguments count
   * \param argv program arguments
   * \return none
   */
  void Simulate (int argc, char **argv);

protected:
  /**
   * \brief Sets default attribute values
   * \return none
   */
  virtual void SetDefaultAttributeValues ();

  /**
   * \brief Process command line arguments
   * \param argc program arguments count
   * \param argv program arguments
   * \return none
   */
  virtual void ParseCommandLineArguments (int argc, char **argv);

  /**
   * \brief Configure nodes
   * \return none
   */
  virtual void ConfigureNodes ();

  /**
   * \brief Configure channels
   * \return none
   */
  virtual void ConfigureChannels ();

  /**
   * \brief Configure devices
   * \return none
   */
  virtual void ConfigureDevices ();

  /**
   * \brief Configure mobility
   * \return none
   */
  virtual void ConfigureMobility ();

  /**
   * \brief Configure applications
   * \return none
   */
  virtual void ConfigureApplications ();

  /**
   * \brief Configure tracing
   * \return none
   */
  virtual void ConfigureTracing ();

  /**
   * \brief Run the simulation
   * \return none
   */
  virtual void RunSimulation ();

  /**
   * \brief Process outputs
   * \return none
   */
  virtual void ProcessOutputs ();
};

WifiApp::WifiApp ()
{
}

WifiApp::~WifiApp ()
{
}

void
WifiApp::Simulate (int argc, char **argv)
{
  // Simulator Program Flow:
  // (source:  NS-3 Annual Meeting, May, 2014, session 2 slides 6, 28)
  //   (HandleProgramInputs:)
  //   SetDefaultAttributeValues
  //   ParseCommandLineArguments
  //   (ConfigureTopology:)
  //   ConfigureNodes
  //   ConfigureChannels
  //   ConfigureDevices
  //   ConfigureMobility
  //   ConfigureApplications
  //     e.g AddInternetStackToNodes
  //         ConfigureIpAddressingAndRouting
  //         configureSendMessages
  //   ConfigureTracing
  //   RunSimulation
  //   ProcessOutputs

  SetDefaultAttributeValues ();
  ParseCommandLineArguments (argc, argv);
  ConfigureNodes ();
  ConfigureChannels ();
  ConfigureDevices ();
  ConfigureMobility ();
  ConfigureApplications ();
  ConfigureTracing ();
  RunSimulation ();
  ProcessOutputs ();
}

void
WifiApp::SetDefaultAttributeValues ()
{
}

void
WifiApp::ParseCommandLineArguments (int argc, char **argv)
{
}

void
WifiApp::ConfigureNodes ()
{
}

void
WifiApp::ConfigureChannels ()
{
}

void
WifiApp::ConfigureDevices ()
{
}

void
WifiApp::ConfigureMobility ()
{
}

void
WifiApp::ConfigureApplications ()
{
}

void
WifiApp::ConfigureTracing ()
{
}

void
WifiApp::RunSimulation ()
{
}

void
WifiApp::ProcessOutputs ()
{
}

/**
 * \ingroup wave
 * \brief The ConfigStoreHelper class simplifies config-store raw text load and save
 */
class ConfigStoreHelper
{
public:
  /**
   * \brief Constructor
   * \return none
   */
  ConfigStoreHelper ();

  /**
   * \brief Loads a saved config-store raw text configuration from a given named file
   * \param configFilename the name of the config-store raw text file
   * \return none
   */
  void LoadConfig (std::string configFilename);

  /**
   * \brief Saves a configuration to a given named config-store raw text configuration file
   * \param configFilename the name of the config-store raw text file
   * \return none
   */
  void SaveConfig (std::string configFilename);
};

ConfigStoreHelper::ConfigStoreHelper ()
{
}

void
ConfigStoreHelper::LoadConfig (std::string configFilename)
{
  // Input config store from txt format
  Config::SetDefault ("ns3::ConfigStore::Filename", StringValue (configFilename));
  Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
  Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Load"));
  ConfigStore inputConfig;
  inputConfig.ConfigureDefaults ();
  //inputConfig.ConfigureAttributes ();
}

void
ConfigStoreHelper::SaveConfig (std::string configFilename)
{
  // only save if a non-empty filename has been specified
  if (configFilename.compare ("") != 0)
    {
      // Output config store to txt format
      Config::SetDefault ("ns3::ConfigStore::Filename", StringValue (configFilename));
      Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
      Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));
      ConfigStore outputConfig;
      outputConfig.ConfigureDefaults ();
      //outputConfig.ConfigureAttributes ();
    }
}

/**
 * \ingroup wave
 * \brief The VanetRoutingExperiment class implements a wifi app that
 * allows VANET routing experiments to be simulated
 */
class VanetRoutingExperiment : public WifiApp
{
public:
  /**
   * \brief Constructor
   * \return none
   */
  VanetRoutingExperiment ();

protected:
  /**
   * \brief Sets default attribute values
   * \return none
   */
  virtual void SetDefaultAttributeValues ();

  /**
   * \brief Process command line arguments
   * \param argc program arguments count
   * \param argv program arguments
   * \return none
   */
  virtual void ParseCommandLineArguments (int argc, char **argv);

  /**
   * \brief Configure nodes
   * \return none
   */
  virtual void ConfigureNodes ();

  /**
   * \brief Configure channels
   * \return none
   */
  virtual void ConfigureChannels ();

  /**
   * \brief Configure devices
   * \return none
   */
  virtual void ConfigureDevices ();

  /**
   * \brief Configure mobility
   * \return none
   */
  virtual void ConfigureMobility ();

  /**
   * \brief Configure applications
   * \return none
   */
  virtual void ConfigureApplications ();

  /**
   * \brief Configure tracing
   * \return none
   */
  virtual void ConfigureTracing ();

  /**
   * \brief Run the simulation
   * \return none
   */
  virtual void RunSimulation ();

  /**
   * \brief Process outputs
   * \return none
   */
  virtual void ProcessOutputs ();

private:
  /**
   * \brief Run the simulation
   * \return none
   */
  void Run ();

  /**
   * \brief Run the simulation
   * \return none
   */
  void CommandSetup (int argc, char **argv);

  /**
   * \brief Checks the throughput and outputs summary to CSV file1.
   * This is scheduled and called once per second
   * \return none
   */
  void CheckThroughput ();

  /**
   * \brief Set up log file
   * \return none
   */
  void SetupLogFile ();

  /**
   * \brief Set up logging
   * \return none
   */
  void SetupLogging ();

  /**
   * \brief Configure default attributes
   * \return none
   */
  void ConfigureDefaults ();

  /**
   * \brief Set up the adhoc mobility nodes
   * \return none
   */
  void SetupAdhocMobilityNodes ();

  /**
   * \brief Set up the adhoc devices
   * \return none
   */
  void SetupAdhocDevices ();

  /**
   * \brief Set up generation of IEEE 1609 WAVE messages,
   * as a Basic Safety Message (BSM).  The BSM is typically
   * a ~200-byte packets broadcast by all vehicles at a nominal
   * rate of 10 Hz
   * \return none
   */
  void SetupWaveMessages ();

  /**
   * \brief Set up generation of packets to be routed
   * through the vehicular network
   * \return none
   */
  void SetupRoutingMessages ();

  /**
   * \brief Set up a prescribed scenario
   * \return none
   */
  void SetupScenario ();

  /**
   * \brief Write the header line to the CSV file1
   * \return none
   */
  void WriteCsvHeader ();

  /**
   * \brief Set up configuration parameter from the global variables
   * \return none
   */
  void SetConfigFromGlobals ();

  /**
   * \brief Set up the global variables from the configuration parameters
   * \return none
   */
  void SetGlobalsFromConfig ();

  static void
  CourseChange (std::ostream *os, std::string foo, Ptr<const MobilityModel> mobility);

  uint32_t m_port;
  std::string m_CSVfileName;
  std::string m_CSVfileName2;
  uint32_t m_nSinks;
  std::string m_protocolName;
  double m_txp;
  bool m_traceMobility;
  uint32_t m_protocol;

  uint32_t m_lossModel;
  uint32_t m_fading;
  std::string m_lossModelName;

  std::string m_phyMode;
  uint32_t m_80211mode;

  std::string m_traceFile;
  std::string m_logFile;
  uint32_t m_mobility;
  uint32_t m_nNodes;
  double m_TotalSimTime;
  std::string m_rate;
  std::string m_phyModeB;
  std::string m_trName;
  int m_nodeSpeed; //in m/s
  int m_nodePause; //in s
  uint32_t m_wavePacketSize; // bytes
  double m_waveInterval; // seconds
  int m_verbose;
  std::ofstream m_os;
  NetDeviceContainer m_adhocTxDevices;
  Ipv4InterfaceContainer m_adhocTxInterfaces;
  uint32_t m_scenario;
  double m_gpsAccuracyNs;
  double m_txMaxDelayMs;
  int m_routingTables;
  int m_asciiTrace;
  int m_pcap;
  std::string m_loadConfigFilename;
  std::string m_saveConfigFilename;

  WaveBsmHelper m_waveBsmHelper;
  Ptr<RoutingHelper> m_routingHelper;
  Ptr<WifiPhyStats> m_wifiPhyStats;
  int m_log;
  // used to get consistent random numbers across scenarios
  int64_t m_streamIndex;
  NodeContainer m_adhocTxNodes;
  double m_txSafetyRange1;
  double m_txSafetyRange2;
  double m_txSafetyRange3;
  double m_txSafetyRange4;
  double m_txSafetyRange5;
  double m_txSafetyRange6;
  double m_txSafetyRange7;
  double m_txSafetyRange8;
  double m_txSafetyRange9;
  double m_txSafetyRange10;
  std::vector <double> m_txSafetyRanges;
  std::string m_exp;
  int m_cumulativeBsmCaptureStart;
};

VanetRoutingExperiment::VanetRoutingExperiment ()
  : m_port (9),
    m_CSVfileName ("vanet-routing.output.csv"),
    m_CSVfileName2 ("vanet-routing.output2.csv"),
    m_nSinks (NUM_OF_NODES),
    m_protocolName ("protocol"),
    m_txp (TXPOWER),
    m_traceMobility (false),
    // AODV
    m_protocol (2),
    // Two-Ray ground
    m_lossModel (3),
    m_fading (0),
    m_lossModelName (""),
    m_phyMode ("OfdmRate6MbpsBW10MHz"),
    // 1=802.11p
    m_80211mode (1),
    m_traceFile (""),
    m_logFile ("low99-ct-unterstrass-1day.filt.7.adj.log"),
    m_mobility (1),
    m_nNodes (156),
    m_TotalSimTime (300.01),
    m_rate ("2048bps"),
    m_phyModeB ("DsssRate11Mbps"),
    m_trName ("vanet-routing-compare"),
    m_nodeSpeed (20),
    m_nodePause (0),
    m_wavePacketSize (200),
    m_waveInterval (0.1),
    m_verbose (0),
    m_scenario (1),
    m_gpsAccuracyNs (40),
    m_txMaxDelayMs (10),
    m_routingTables (0),
    m_asciiTrace (0),
    m_pcap (0),
    m_loadConfigFilename ("load-config.txt"),
    m_saveConfigFilename (""),
    m_log (0),
    m_streamIndex (0),
    m_adhocTxNodes (),
    m_txSafetyRange1 (50.0),
    m_txSafetyRange2 (100.0),
    m_txSafetyRange3 (150.0),
    m_txSafetyRange4 (200.0),
    m_txSafetyRange5 (250.0),
    m_txSafetyRange6 (300.0),
    m_txSafetyRange7 (350.0),
    m_txSafetyRange8 (400.0),
    m_txSafetyRange9 (450.0),
    m_txSafetyRange10 (500.0),
    m_txSafetyRanges (),
    m_exp (""),
    m_cumulativeBsmCaptureStart (0)
{
  m_wifiPhyStats = CreateObject<WifiPhyStats> ();
  m_routingHelper = CreateObject<RoutingHelper> ();

  // set to non-zero value to enable
  // simply uncond logging during simulation run
  m_log = 1;
}

void
VanetRoutingExperiment::SetDefaultAttributeValues ()
{
  // handled in constructor
}

// important configuration items stored in global values
static ns3::GlobalValue g_port ("VRCport",
                                "Port",
                                ns3::UintegerValue (9),
                                ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nSinks ("VRCnSinks",
                                  "Number of sink nodes for routing non-BSM traffic",
                                  ns3::UintegerValue (10),
                                  ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_traceMobility ("VRCtraceMobility",
                                         "Trace mobility 1=yes;0=no",
                                         ns3::UintegerValue (0),
                                         ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_protocol ("VRCprotocol",
                                    "Routing protocol",
                                    ns3::UintegerValue (2),
                                    ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_lossModel ("VRClossModel",
                                     "Propagation Loss Model",
                                     ns3::UintegerValue (3),
                                     ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_fading ("VRCfading",
                                  "Fast Fading Model",
                                  ns3::UintegerValue (0),
                                  ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_80211mode ("VRC80211mode",
                                     "802.11 mode (0=802.11a;1=802.11p)",
                                     ns3::UintegerValue (1),
                                     ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_mobility ("VRCmobility",
                                    "Mobility mode 0=random waypoint;1=mobility trace file",
                                    ns3::UintegerValue (1),
                                    ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nNodes ("VRCnNodes",
                                  "Number of nodes (vehicles)",
                                  ns3::UintegerValue (156),
                                  ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nodeSpeed ("VRCnodeSpeed",
                                     "Node speed (m/s) for RWP model",
                                     ns3::UintegerValue (20),
                                     ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nodePause ("VRCnodePause",
                                     "Node pause time (s) for RWP model",
                                     ns3::UintegerValue (0),
                                     ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_wavePacketSize ("VRCwavePacketSize",
                                          "Size in bytes of WAVE BSM",
                                          ns3::UintegerValue (200),
                                          ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_verbose ("VRCverbose",
                                   "Verbose 0=no;1=yes",
                                   ns3::UintegerValue (0),
                                   ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_scenario ("VRCscenario",
                                    "Scenario",
                                    ns3::UintegerValue (1),
                                    ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_routingTables ("VRCroutingTables",
                                         "Dump routing tables at t=5 seconds 0=no;1=yes",
                                         ns3::UintegerValue (0),
                                         ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_asciiTrace ("VRCasciiTrace",
                                      "Dump ASCII trace 0=no;1=yes",
                                      ns3::UintegerValue (0),
                                      ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_pcap ("VRCpcap",
                                "Generate PCAP files 0=no;1=yes",
                                ns3::UintegerValue (0),
                                ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_cumulativeBsmCaptureStart ("VRCcumulativeBsmCaptureStart",
                                                     "Simulation starte time for capturing cumulative BSM",
                                                     ns3::UintegerValue (0),
                                                     ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_txSafetyRange1 ("VRCtxSafetyRange1",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (50.0),
                                          ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_txSafetyRange2 ("VRCtxSafetyRange2",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (100.0),
                                          ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_txSafetyRange3 ("VRCtxSafetyRange3",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (150.0),
                                          ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_txSafetyRange4 ("VRCtxSafetyRange4",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (200.0),
                                          ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_txSafetyRange5 ("VRCtxSafetyRange5",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (250.0),
                                          ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_txSafetyRange6 ("VRCtxSafetyRange6",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (300.0),
                                          ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_txSafetyRange7 ("VRCtxSafetyRange7",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (350.0),
                                          ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_txSafetyRange8 ("VRCtxSafetyRange8",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (400.0),
                                          ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_txSafetyRange9 ("VRCtxSafetyRange9",
                                          "BSM range for PDR inclusion",
                                          ns3::DoubleValue (450.0),
                                          ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_txSafetyRange10 ("VRCtxSafetyRange10",
                                           "BSM range for PDR inclusion",
                                           ns3::DoubleValue (500.0),
                                           ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_txp ("VRCtxp",
                               "Transmission power dBm",
                               ns3::DoubleValue (7.5),
                               ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_totalTime ("VRCtotalTime",
                                     "Total simulation time (s)",
                                     ns3::DoubleValue (300.01),
                                     ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_waveInterval ("VRCwaveInterval",
                                        "Interval (s) between WAVE BSMs",
                                        ns3::DoubleValue (0.1),
                                        ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_gpsAccuracyNs ("VRCgpsAccuracyNs",
                                         "GPS sync accuracy (ns)",
                                         ns3::DoubleValue (40),
                                         ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_txMaxDelayMs ("VRCtxMaxDelayMs",
                                        "Tx May Delay (ms)",
                                        ns3::DoubleValue (10),
                                        ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_CSVfileName ("VRCCSVfileName",
                                       "CSV filename (for time series data)",
                                       ns3::StringValue ("vanet-routing.output.csv"),
                                       ns3::MakeStringChecker ());
static ns3::GlobalValue g_CSVfileName2 ("VRCCSVfileName2",
                                        "CSV filename 2 (for overall simulation scenario results)",
                                        ns3::StringValue ("vanet-routing.output2.csv"),
                                        ns3::MakeStringChecker ());
static ns3::GlobalValue g_phyMode ("VRCphyMode",
                                   "PHY mode (802.11p)",
                                   ns3::StringValue ("OfdmRate6MbpsBW10MHz"),
                                   ns3::MakeStringChecker ());
static ns3::GlobalValue g_traceFile ("VRCtraceFile",
                                     "Mobility trace filename",
                                     ns3::StringValue ("./src/wave/examples/low99-ct-unterstrass-1day.filt.7.adj.mob"),
                                     ns3::MakeStringChecker ());
static ns3::GlobalValue g_logFile ("VRClogFile",
                                   "Log filename",
                                   ns3::StringValue ("low99-ct-unterstrass-1day.filt.7.adj.log"),
                                   ns3::MakeStringChecker ());
static ns3::GlobalValue g_rate ("VRCrate",
                                "Data rate",
                                ns3::StringValue ("2048bps"),
                                ns3::MakeStringChecker ());
static ns3::GlobalValue g_phyModeB ("VRCphyModeB",
                                    "PHY mode (802.11a)",
                                    ns3::StringValue ("DsssRate11Mbps"),
                                    ns3::MakeStringChecker ());
static ns3::GlobalValue g_trName ("VRCtrName",
                                  "Trace name",
                                  ns3::StringValue ("vanet-routing-compare"),
                                  ns3::MakeStringChecker ());

void
VanetRoutingExperiment::ParseCommandLineArguments (int argc, char **argv)
{
  // なくても良い
  CommandSetup (argc, argv);
  SetupScenario ();

  // user may specify up to 10 different tx distances
  // to be used for calculating different values of Packet
  // Delivery Ratio (PDR). Used to see the effects of
  // fading over distance
  m_txSafetyRanges.resize (10, 0);
  m_txSafetyRanges[0] = m_txSafetyRange1;
  m_txSafetyRanges[1] = m_txSafetyRange2;
  m_txSafetyRanges[2] = m_txSafetyRange3;
  m_txSafetyRanges[3] = m_txSafetyRange4;
  m_txSafetyRanges[4] = m_txSafetyRange5;
  m_txSafetyRanges[5] = m_txSafetyRange6;
  m_txSafetyRanges[6] = m_txSafetyRange7;
  m_txSafetyRanges[7] = m_txSafetyRange8;
  m_txSafetyRanges[8] = m_txSafetyRange9;
  m_txSafetyRanges[9] = m_txSafetyRange10;

  ConfigureDefaults ();

  // we are done with all configuration
  // save config-store, if requested
  SetGlobalsFromConfig ();
  ConfigStoreHelper configStoreHelper;
  configStoreHelper.SaveConfig (m_saveConfigFilename);

  m_waveBsmHelper.GetWaveBsmStats ()->SetLogging (m_log);
  m_routingHelper->SetLogging (m_log);
}

void
VanetRoutingExperiment::ConfigureNodes ()
{
  // ノードコンテナーを作成ごにグローバル変数に代入
  m_adhocTxNodes.Create (m_nNodes);
  global_node_container = m_adhocTxNodes;
  //解析に必要なデータをノードごとにストラクト構造で保存
  uint32_t node_id = 0;
  Ptr <Node> tmp;
  for (uint32_t key = 0; key < m_nNodes; key++){
    tmp = m_adhocTxNodes.Get(key);
    node_id = tmp->GetId();
    my_data[node_id].id = node_id;
  }
}

void
VanetRoutingExperiment::ConfigureChannels ()
{
  // set up channel and devices
  SetupAdhocDevices ();
}

void
VanetRoutingExperiment::ConfigureDevices ()
{
  // devices are set up in SetupAdhocDevices(),
  // called by ConfigureChannels()

  // every device will have PHY callback for tracing
  // which is used to determine the total amount of
  // data transmitted, and then used to calculate
  // the MAC/PHY overhead beyond the app-data
  Config::Connect ("/NodeList/*/DeviceList/*/Phy/State/Tx", MakeCallback (&WifiPhyStats::PhyTxTrace, m_wifiPhyStats));
  // TxDrop, RxDrop not working yet.  Not sure what I'm doing wrong.
  Config::Connect ("/NodeList/*/DeviceList/*/ns3::WifiNetDevice/Phy/PhyTxDrop", MakeCallback (&WifiPhyStats::PhyTxDrop, m_wifiPhyStats));
  Config::Connect ("/NodeList/*/DeviceList/*/ns3::WifiNetDevice/Phy/PhyRxDrop", MakeCallback (&WifiPhyStats::PhyRxDrop, m_wifiPhyStats));
}

void
VanetRoutingExperiment::ConfigureMobility ()
{
  SetupAdhocMobilityNodes ();
}

void
VanetRoutingExperiment::ConfigureApplications ()
{
  // Traffic mix consists of:
  // 1. routing data
  // 2. Broadcasting of Basic Safety Message (BSM)
  SetupRoutingMessages ();
  SetupWaveMessages ();

  // config trace to capture app-data (bytes) for
  // routing data, subtracted and used for
  // routing overhead
  std::ostringstream oss;
  oss.str ("");
  oss << "/NodeList/*/ApplicationList/*/$ns3::OnOffApplication/Tx";
  Config::Connect (oss.str (), MakeCallback (&RoutingHelper::OnOffTrace, m_routingHelper));
}

void
VanetRoutingExperiment::ConfigureTracing ()
{
  WriteCsvHeader ();
  SetupLogFile ();
  SetupLogging ();

  AsciiTraceHelper ascii;
  MobilityHelper::EnableAsciiAll (ascii.CreateFileStream (m_trName + ".mob"));
}

void
VanetRoutingExperiment::RunSimulation ()
{
  Run ();
}

void
VanetRoutingExperiment::ProcessOutputs ()
{
  // calculate and output final results
  double bsm_pdr1 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (1);
  double bsm_pdr2 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (2);
  double bsm_pdr3 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (3);
  double bsm_pdr4 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (4);
  double bsm_pdr5 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (5);
  double bsm_pdr6 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (6);
  double bsm_pdr7 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (7);
  double bsm_pdr8 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (8);
  double bsm_pdr9 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (9);
  double bsm_pdr10 = m_waveBsmHelper.GetWaveBsmStats ()->GetCumulativeBsmPdr (10);

  double averageRoutingGoodputKbps = 0.0;
  uint32_t totalBytesTotal = m_routingHelper->GetRoutingStats ().GetCumulativeRxBytes ();
  averageRoutingGoodputKbps = (((double) totalBytesTotal * 8.0) / m_TotalSimTime) / 1000.0;

  // calculate MAC/PHY overhead (mac-phy-oh)
  // total WAVE BSM bytes sent
  uint32_t cumulativeWaveBsmBytes = m_waveBsmHelper.GetWaveBsmStats ()->GetTxByteCount ();
  uint32_t cumulativeRoutingBytes = m_routingHelper->GetRoutingStats ().GetCumulativeTxBytes ();
  uint32_t totalAppBytes = cumulativeWaveBsmBytes + cumulativeRoutingBytes;
  uint32_t totalPhyBytes = m_wifiPhyStats->GetTxBytes ();
  // mac-phy-oh = (total-phy-bytes - total-app-bytes) / total-phy-bytes
  double mac_phy_oh = 0.0;
  if (totalPhyBytes > 0)
    {
      mac_phy_oh = (double) (totalPhyBytes - totalAppBytes) / (double) totalPhyBytes;
    }

  if (m_log != 0)
    {
      NS_LOG_UNCOND ("BSM_PDR1=" << bsm_pdr1 << " BSM_PDR2=" << bsm_pdr2 << " BSM_PDR3=" << bsm_pdr3 << " BSM_PDR4=" << bsm_pdr4 << " BSM_PDR5=" << bsm_pdr5 << " BSM_PDR6=" << bsm_pdr6 << " BSM_PDR7=" << bsm_pdr7 << " BSM_PDR8=" << bsm_pdr8 << " BSM_PDR9=" << bsm_pdr9 << " BSM_PDR10=" << bsm_pdr10 << " Goodput=" << averageRoutingGoodputKbps << "Kbps MAC/PHY-oh=" << mac_phy_oh);

    }

  std::ofstream out (m_CSVfileName2.c_str (), std::ios::app);

  // out << bsm_pdr1 << ","
  //     << bsm_pdr2 << ","
  //     << bsm_pdr3 << ","
  //     << bsm_pdr4 << ","
  //     << bsm_pdr5 << ","
  //     << bsm_pdr6 << ","
  //     << bsm_pdr7 << ","
  //     << bsm_pdr8 << ","
  //     << bsm_pdr9 << ","
  //     << bsm_pdr10 << ","
  //     << averageRoutingGoodputKbps << ","
  //     << mac_phy_oh << ""
  //     << std::endl;

  out.close ();

  m_os.close (); // close log file
}

void
VanetRoutingExperiment::Run ()
{
  NS_LOG_INFO ("Run Simulation.");

  CheckThroughput ();

  Simulator::Stop (Seconds (m_TotalSimTime));
  Simulator::Run ();
  Simulator::Destroy ();
  std::cout << "Finished simulation!" << std::endl;
}

// Prints actual position and velocity when a course change event occurs
void
VanetRoutingExperiment::
CourseChange (std::ostream *os, std::string foo, Ptr<const MobilityModel> mobility)
{
  Vector pos = mobility->GetPosition (); // Get position
  Vector vel = mobility->GetVelocity (); // Get velocity

  pos.z = 1.5;

  int nodeId = mobility->GetObject<Node> ()->GetId ();
  double t = (Simulator::Now ()).GetSeconds ();
  if (t >= 1.0)
    {
      WaveBsmHelper::GetNodesMoving ()[nodeId] = 1;
    }

  //NS_LOG_UNCOND ("Changing pos for node=" << nodeId << " at " << Simulator::Now () );

  // Prints position and velocities
  *os << Simulator::Now () << " POS: x=" << pos.x << ", y=" << pos.y
      << ", z=" << pos.z << "; VEL:" << vel.x << ", y=" << vel.y
      << ", z=" << vel.z << std::endl;
}

void
VanetRoutingExperiment::CheckThroughput ()
{
  uint32_t bytesTotal = m_routingHelper->GetRoutingStats ().GetRxBytes ();
  uint32_t packetsReceived = m_routingHelper->GetRoutingStats ().GetRxPkts ();
  double kbps = (bytesTotal * 8.0) / 1000;
  double wavePDR = 0.0;
  int wavePktsSent = m_waveBsmHelper.GetWaveBsmStats ()->GetTxPktCount ();
  int wavePktsReceived = m_waveBsmHelper.GetWaveBsmStats ()->GetRxPktCount ();
  if (wavePktsSent > 0)
    {
      int wavePktsReceived = m_waveBsmHelper.GetWaveBsmStats ()->GetRxPktCount ();
      wavePDR = (double) wavePktsReceived / (double) wavePktsSent;
    }

  int waveExpectedRxPktCount = m_waveBsmHelper.GetWaveBsmStats ()->GetExpectedRxPktCount (1);
  int waveRxPktInRangeCount = m_waveBsmHelper.GetWaveBsmStats ()->GetRxPktInRangeCount (1);
  double wavePDR1_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (1);
  double wavePDR2_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (2);
  double wavePDR3_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (3);
  double wavePDR4_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (4);
  double wavePDR5_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (5);
  double wavePDR6_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (6);
  double wavePDR7_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (7);
  double wavePDR8_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (8);
  double wavePDR9_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (9);
  double wavePDR10_2 = m_waveBsmHelper.GetWaveBsmStats ()->GetBsmPdr (10);

  // calculate MAC/PHY overhead (mac-phy-oh)
  // total WAVE BSM bytes sent
  uint32_t cumulativeWaveBsmBytes = m_waveBsmHelper.GetWaveBsmStats ()->GetTxByteCount ();
  uint32_t cumulativeRoutingBytes = m_routingHelper->GetRoutingStats ().GetCumulativeTxBytes ();
  uint32_t totalAppBytes = cumulativeWaveBsmBytes + cumulativeRoutingBytes;
  uint32_t totalPhyBytes = m_wifiPhyStats->GetTxBytes ();
  // mac-phy-oh = (total-phy-bytes - total-app-bytes) / total-phy-bytes
  double mac_phy_oh = 0.0;
  if (totalPhyBytes > 0)
    {
      mac_phy_oh = (double) (totalPhyBytes - totalAppBytes) / (double) totalPhyBytes;
    }

  std::ofstream out (m_CSVfileName.c_str (), std::ios::app);

  if (m_log != 0 )
    {
      NS_LOG_UNCOND ("At t=" << (Simulator::Now ()).GetSeconds () << "s BSM_PDR1=" << wavePDR1_2 << " BSM_PDR1=" << wavePDR2_2 << " BSM_PDR3=" << wavePDR3_2 << " BSM_PDR4=" << wavePDR4_2 << " BSM_PDR5=" << wavePDR5_2 << " BSM_PDR6=" << wavePDR6_2 << " BSM_PDR7=" << wavePDR7_2 << " BSM_PDR8=" << wavePDR8_2 << " BSM_PDR9=" << wavePDR9_2 << " BSM_PDR10=" << wavePDR10_2 << " Goodput=" << kbps << "Kbps" /*<< " MAC/PHY-OH=" << mac_phy_oh*/);
    }

  out << (Simulator::Now ()).GetSeconds () << ","
      << kbps << ","
      << packetsReceived << ","
      << m_nSinks << ","
      << m_protocolName << ","
      << m_txp << ","
      << wavePktsSent << ","
      << wavePktsReceived << ","
      << wavePDR << ","
      << waveExpectedRxPktCount << ","
      << waveRxPktInRangeCount << ","
      << wavePDR1_2 << ","
      << wavePDR2_2 << ","
      << wavePDR3_2 << ","
      << wavePDR4_2 << ","
      << wavePDR5_2 << ","
      << wavePDR6_2 << ","
      << wavePDR7_2 << ","
      << wavePDR8_2 << ","
      << wavePDR9_2 << ","
      << wavePDR10_2 << ","
      << mac_phy_oh << ""
      << std::endl;

  out.close ();

  m_routingHelper->GetRoutingStats ().SetRxBytes (0);
  m_routingHelper->GetRoutingStats ().SetRxPkts (0);
  m_waveBsmHelper.GetWaveBsmStats ()->SetRxPktCount (0);
  m_waveBsmHelper.GetWaveBsmStats ()->SetTxPktCount (0);
  for (int index = 1; index <= 10; index++)
    {
      m_waveBsmHelper.GetWaveBsmStats ()->SetExpectedRxPktCount (index, 0);
      m_waveBsmHelper.GetWaveBsmStats ()->SetRxPktInRangeCount (index, 0);
    }

  double currentTime = (Simulator::Now ()).GetSeconds ();
  if (currentTime <= (double) m_cumulativeBsmCaptureStart)
    {
      for (int index = 1; index <= 10; index++)
        {
          m_waveBsmHelper.GetWaveBsmStats ()->ResetTotalRxPktCounts (index);
        }
    }

  Simulator::Schedule (Seconds (1.0), &VanetRoutingExperiment::CheckThroughput, this);
}

void
VanetRoutingExperiment::SetConfigFromGlobals ()
{
  // get settings saved from config-store
  UintegerValue uintegerValue;
  DoubleValue doubleValue;
  StringValue stringValue;

  // This may not be the best way to manage program configuration
  // (directing them through global values), but management
  // through the config-store here is copied from
  // src/lte/examples/lena-dual-stripe.cc

  GlobalValue::GetValueByName ("VRCport", uintegerValue);
  m_port = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCnSinks", uintegerValue);
  m_nSinks = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCtraceMobility", uintegerValue);
  m_traceMobility = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCprotocol", uintegerValue);
  m_protocol = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRClossModel", uintegerValue);
  m_lossModel = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCfading", uintegerValue);
  m_fading = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRC80211mode", uintegerValue);
  m_80211mode = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCmobility", uintegerValue);
  m_mobility = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCnNodes", uintegerValue);
  m_nNodes = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCnodeSpeed", uintegerValue);
  m_nodeSpeed = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCnodePause", uintegerValue);
  m_nodePause = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCwavePacketSize", uintegerValue);
  m_wavePacketSize = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCverbose", uintegerValue);
  m_verbose = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCscenario", uintegerValue);
  m_scenario = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCroutingTables", uintegerValue);
  m_routingTables = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCasciiTrace", uintegerValue);
  m_asciiTrace = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCpcap", uintegerValue);
  m_pcap = uintegerValue.Get ();
  GlobalValue::GetValueByName ("VRCcumulativeBsmCaptureStart", uintegerValue);
  m_cumulativeBsmCaptureStart = uintegerValue.Get ();

  GlobalValue::GetValueByName ("VRCtxSafetyRange1", doubleValue);
  m_txSafetyRange1 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange2", doubleValue);
  m_txSafetyRange2 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange3", doubleValue);
  m_txSafetyRange3 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange4", doubleValue);
  m_txSafetyRange4 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange5", doubleValue);
  m_txSafetyRange5 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange6", doubleValue);
  m_txSafetyRange6 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange7", doubleValue);
  m_txSafetyRange7 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange8", doubleValue);
  m_txSafetyRange8 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange9", doubleValue);
  m_txSafetyRange9 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxSafetyRange10", doubleValue);
  m_txSafetyRange10 = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxp", doubleValue);
  m_txp = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtotalTime", doubleValue);
  m_TotalSimTime = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCwaveInterval", doubleValue);
  m_waveInterval = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCgpsAccuracyNs", doubleValue);
  m_gpsAccuracyNs = doubleValue.Get ();
  GlobalValue::GetValueByName ("VRCtxMaxDelayMs", doubleValue);
  m_txMaxDelayMs = doubleValue.Get ();

  GlobalValue::GetValueByName ("VRCCSVfileName", stringValue);
  m_CSVfileName = stringValue.Get ();
  GlobalValue::GetValueByName ("VRCCSVfileName2", stringValue);
  m_CSVfileName2 = stringValue.Get ();
  GlobalValue::GetValueByName ("VRCphyMode", stringValue);
  m_phyMode = stringValue.Get ();
  GlobalValue::GetValueByName ("VRCtraceFile", stringValue);
  m_traceFile = stringValue.Get ();
  GlobalValue::GetValueByName ("VRClogFile", stringValue);
  m_logFile = stringValue.Get ();
  GlobalValue::GetValueByName ("VRCrate", stringValue);
  m_rate = stringValue.Get ();
  GlobalValue::GetValueByName ("VRCphyModeB", stringValue);
  m_phyModeB = stringValue.Get ();
  GlobalValue::GetValueByName ("VRCtrName", stringValue);
  m_trName = stringValue.Get ();
}

void
VanetRoutingExperiment::SetGlobalsFromConfig ()
{
  // get settings saved from config-store
  UintegerValue uintegerValue;
  DoubleValue doubleValue;
  StringValue stringValue;

  g_port.SetValue (UintegerValue (m_port));
  g_nSinks.SetValue (UintegerValue (m_nSinks));
  g_traceMobility.SetValue (UintegerValue (m_traceMobility));
  g_protocol.SetValue (UintegerValue (m_protocol));
  g_lossModel.SetValue (UintegerValue (m_lossModel));
  g_fading.SetValue (UintegerValue (m_fading));
  g_80211mode.SetValue (UintegerValue (m_80211mode));
  g_mobility.SetValue (UintegerValue (m_mobility));
  g_nNodes.SetValue (UintegerValue (m_nNodes));
  g_nodeSpeed.SetValue (UintegerValue (m_nodeSpeed));
  g_nodePause.SetValue (UintegerValue (m_nodePause));
  g_wavePacketSize.SetValue (UintegerValue (m_wavePacketSize));
  g_verbose.SetValue (UintegerValue (m_verbose));
  g_scenario.SetValue (UintegerValue (m_scenario));
  g_routingTables.SetValue (UintegerValue (m_routingTables));
  g_asciiTrace.SetValue (UintegerValue (m_asciiTrace));
  g_pcap.SetValue (UintegerValue (m_pcap));
  g_cumulativeBsmCaptureStart.SetValue (UintegerValue (m_cumulativeBsmCaptureStart));

  g_txSafetyRange1.SetValue (DoubleValue (m_txSafetyRange1));
  g_txSafetyRange2.SetValue (DoubleValue (m_txSafetyRange2));
  g_txSafetyRange3.SetValue (DoubleValue (m_txSafetyRange3));
  g_txSafetyRange4.SetValue (DoubleValue (m_txSafetyRange4));
  g_txSafetyRange5.SetValue (DoubleValue (m_txSafetyRange5));
  g_txSafetyRange6.SetValue (DoubleValue (m_txSafetyRange6));
  g_txSafetyRange7.SetValue (DoubleValue (m_txSafetyRange7));
  g_txSafetyRange8.SetValue (DoubleValue (m_txSafetyRange8));
  g_txSafetyRange9.SetValue (DoubleValue (m_txSafetyRange9));
  g_txSafetyRange10.SetValue (DoubleValue (m_txSafetyRange10));
  g_txp.SetValue (DoubleValue (m_txp));
  g_totalTime.SetValue (DoubleValue (m_TotalSimTime));
  g_waveInterval.SetValue (DoubleValue (m_waveInterval));
  g_gpsAccuracyNs.SetValue (DoubleValue (m_gpsAccuracyNs));
  g_txMaxDelayMs.SetValue (DoubleValue (m_txMaxDelayMs));

  g_CSVfileName.SetValue (StringValue (m_CSVfileName));
  g_CSVfileName2.SetValue (StringValue (m_CSVfileName2));
  g_phyMode.SetValue (StringValue (m_phyMode));
  g_traceFile.SetValue (StringValue (m_traceFile));
  g_logFile.SetValue (StringValue (m_logFile));
  g_rate.SetValue (StringValue (m_rate));
  g_phyModeB.SetValue (StringValue (m_phyModeB));
  g_trName.SetValue (StringValue (m_trName));
  GlobalValue::GetValueByName ("VRCtrName", stringValue);
  m_trName = stringValue.Get ();
}

void
VanetRoutingExperiment::CommandSetup (int argc, char **argv)
{
  CommandLine cmd;
  double txDist1 = 50.0;
  double txDist2 = 100.0;
  double txDist3 = 150.0;
  double txDist4 = 200.0;
  double txDist5 = 250.0;
  double txDist6 = 300.0;
  double txDist7 = 350.0;
  double txDist8 = 350.0;
  double txDist9 = 350.0;
  double txDist10 = 350.0;

  // allow command line overrides
  cmd.AddValue ("START_TIME", "The tolerlent differrence", START_TIME);
  cmd.AddValue ("THRESHOLD", "The tolerlent differrence", THRESHOLD);
  cmd.AddValue ("FLOW_RATE_OF_MALLICIAS", "The rate of heavyness of attack", FLOW_RATE_OF_MALLICIAS);
  cmd.AddValue ("MALLICIAS_RATE", "The rate of MALLICIAS Node", MALLICIAS_RATE);
  cmd.AddValue ("CSVfileName", "The name of the CSV output file name", m_CSVfileName);
  cmd.AddValue ("CSVfileName2", "The name of the CSV output file name2", m_CSVfileName2);
  cmd.AddValue ("totaltime", "Simulation end time", m_TotalSimTime);
  cmd.AddValue ("nodes", "Number of nodes (i.e. vehicles)", m_nNodes);
  cmd.AddValue ("sinks", "Number of routing sinks", m_nSinks);
  cmd.AddValue ("txp", "Transmit power (dB), e.g. txp=7.5", m_txp);
  cmd.AddValue ("traceMobility", "Enable mobility tracing", m_traceMobility);
  cmd.AddValue ("protocol", "1=OLSR;2=AODV;3=DSDV;4=DSR", m_protocol);
  cmd.AddValue ("lossModel", "1=Friis;2=ItuR1411Los;3=TwoRayGround;4=LogDistance", m_lossModel);
  cmd.AddValue ("fading", "0=None;1=Nakagami;(buildings=1 overrides)", m_fading);
  cmd.AddValue ("phyMode", "Wifi Phy mode", m_phyMode);
  cmd.AddValue ("80211Mode", "1=802.11p; 2=802.11b; 3=WAVE-PHY", m_80211mode);
  cmd.AddValue ("traceFile", "Ns2 movement trace file", m_traceFile);
  cmd.AddValue ("logFile", "Log file", m_logFile);
  cmd.AddValue ("mobility", "1=trace;2=RWP", m_mobility);
  cmd.AddValue ("rate", "Rate", m_rate);
  cmd.AddValue ("phyModeB", "Phy mode 802.11b", m_phyModeB);
  cmd.AddValue ("speed", "Node speed (m/s)", m_nodeSpeed);
  cmd.AddValue ("pause", "Node pause (s)", m_nodePause);
  cmd.AddValue ("verbose", "0=quiet;1=verbose", m_verbose);
  cmd.AddValue ("bsm", "(WAVE) BSM size (bytes)", m_wavePacketSize);
  cmd.AddValue ("interval", "(WAVE) BSM interval (s)", m_waveInterval);
  cmd.AddValue ("scenario", "1=synthetic, 2=playback-trace", m_scenario);
  // User is allowed to have up to 10 different PDRs (Packet
  // Delivery Ratios) calculate, and so can specify up to
  // 10 different tx distances.
  cmd.AddValue ("txdist1", "Expected BSM tx range, m", txDist1);
  cmd.AddValue ("txdist2", "Expected BSM tx range, m", txDist2);
  cmd.AddValue ("txdist3", "Expected BSM tx range, m", txDist3);
  cmd.AddValue ("txdist4", "Expected BSM tx range, m", txDist4);
  cmd.AddValue ("txdist5", "Expected BSM tx range, m", txDist5);
  cmd.AddValue ("txdist6", "Expected BSM tx range, m", txDist6);
  cmd.AddValue ("txdist7", "Expected BSM tx range, m", txDist7);
  cmd.AddValue ("txdist8", "Expected BSM tx range, m", txDist8);
  cmd.AddValue ("txdist9", "Expected BSM tx range, m", txDist9);
  cmd.AddValue ("txdist10", "Expected BSM tx range, m", txDist10);
  cmd.AddValue ("gpsaccuracy", "GPS time accuracy, in ns", m_gpsAccuracyNs);
  cmd.AddValue ("txmaxdelay", "Tx max delay, in ms", m_txMaxDelayMs);
  cmd.AddValue ("routingTables", "Dump routing tables at t=5 seconds", m_routingTables);
  cmd.AddValue ("asciiTrace", "Dump ASCII Trace data", m_asciiTrace);
  cmd.AddValue ("pcap", "Create PCAP files for all nodes", m_pcap);
  cmd.AddValue ("loadconfig", "Config-store filename to load", m_loadConfigFilename);
  cmd.AddValue ("saveconfig", "Config-store filename to save", m_saveConfigFilename);
  cmd.AddValue ("exp", "Experiment", m_exp);
  cmd.AddValue ("BsmCaptureStart", "Start time to begin capturing pkts for cumulative Bsm", m_cumulativeBsmCaptureStart);
  cmd.Parse (argc, argv);

  m_txSafetyRange1 = txDist1;
  m_txSafetyRange2 = txDist2;
  m_txSafetyRange3 = txDist3;
  m_txSafetyRange4 = txDist4;
  m_txSafetyRange5 = txDist5;
  m_txSafetyRange6 = txDist6;
  m_txSafetyRange7 = txDist7;
  m_txSafetyRange8 = txDist8;
  m_txSafetyRange9 = txDist9;
  m_txSafetyRange10 = txDist10;
  // load configuration info from config-store
  ConfigStoreHelper configStoreHelper;
  configStoreHelper.LoadConfig (m_loadConfigFilename);
  // transfer config-store values to config parameters
  SetConfigFromGlobals ();

  // parse again so you can override input file default values via command line
  cmd.Parse (argc, argv);

  m_txSafetyRange1 = txDist1;
  m_txSafetyRange2 = txDist2;
  m_txSafetyRange3 = txDist3;
  m_txSafetyRange4 = txDist4;
  m_txSafetyRange5 = txDist5;
  m_txSafetyRange6 = txDist6;
  m_txSafetyRange7 = txDist7;
  m_txSafetyRange8 = txDist8;
  m_txSafetyRange9 = txDist9;
  m_txSafetyRange10 = txDist10;
}

void
VanetRoutingExperiment::SetupLogFile ()
{
  // open log file for output
  m_os.open (m_logFile.c_str ());
}

void VanetRoutingExperiment::SetupLogging ()
{

  // Enable logging from the ns2 helper
  LogComponentEnable ("Ns2MobilityHelper",LOG_LEVEL_DEBUG);

  Packet::EnablePrinting ();
}

void
VanetRoutingExperiment::ConfigureDefaults ()
{
  Config::SetDefault ("ns3::OnOffApplication::PacketSize",StringValue ("64"));
  Config::SetDefault ("ns3::OnOffApplication::DataRate",  StringValue (m_rate));

  //Set Non-unicastMode rate to unicast mode
  if (m_80211mode == 2)
    {
      Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",StringValue (m_phyModeB));
    }
  else
    {
      Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",StringValue (m_phyMode));
    }
}

void
VanetRoutingExperiment::SetupAdhocMobilityNodes ()
{
  if (m_mobility == 1)
    {
      // Create Ns2MobilityHelper with the specified trace log file as parameter
      Ns2MobilityHelper ns2 = Ns2MobilityHelper (m_traceFile);
      ns2.Install (); // configure movements for each node, while reading trace file
      // initially assume all nodes are not moving
      WaveBsmHelper::GetNodesMoving ().resize (m_nNodes, 0);
    }
  else if (m_mobility == 2)
    {
      MobilityHelper mobilityAdhoc;

      ObjectFactory pos;
      pos.SetTypeId ("ns3::RandomBoxPositionAllocator");
      pos.Set ("X", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1500.0]"));
      pos.Set ("Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=300.0]"));
      // we need antenna height uniform [1.0 .. 2.0] for loss model
      pos.Set ("Z", StringValue ("ns3::UniformRandomVariable[Min=1.0|Max=2.0]"));

      Ptr<PositionAllocator> taPositionAlloc = pos.Create ()->GetObject<PositionAllocator> ();
      m_streamIndex += taPositionAlloc->AssignStreams (m_streamIndex);

      std::stringstream ssSpeed;
      ssSpeed << "ns3::UniformRandomVariable[Min=0.0|Max=" << m_nodeSpeed << "]";
      std::stringstream ssPause;
      ssPause << "ns3::ConstantRandomVariable[Constant=" << m_nodePause << "]";
      mobilityAdhoc.SetMobilityModel ("ns3::RandomWaypointMobilityModel",
                                      "Speed", StringValue (ssSpeed.str ()),
                                      "Pause", StringValue (ssPause.str ()),
                                      "PositionAllocator", PointerValue (taPositionAlloc));
      mobilityAdhoc.SetPositionAllocator (taPositionAlloc);
      mobilityAdhoc.Install (m_adhocTxNodes);
      m_streamIndex += mobilityAdhoc.AssignStreams (m_adhocTxNodes, m_streamIndex);

      // initially assume all nodes are moving
      WaveBsmHelper::GetNodesMoving ().resize (m_nNodes, 1);
    }

  // Configure callback for logging
  Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange",
                   MakeBoundCallback (&VanetRoutingExperiment::CourseChange, &m_os));
}

void
VanetRoutingExperiment::SetupAdhocDevices ()
{
  if (m_lossModel == 1)
    {
      m_lossModelName = "ns3::FriisPropagationLossModel";
    }
  else if (m_lossModel == 2)
    {
      m_lossModelName = "ns3::ItuR1411LosPropagationLossModel";
    }
  else if (m_lossModel == 3)
    {
      m_lossModelName = "ns3::TwoRayGroundPropagationLossModel";
    }
  else if (m_lossModel == 4)
    {
      m_lossModelName = "ns3::LogDistancePropagationLossModel";
    }
  else
    {
      // Unsupported propagation loss model.
      // Treating as ERROR
      NS_LOG_ERROR ("Invalid propagation loss model specified.  Values must be [1-4], where 1=Friis;2=ItuR1411Los;3=TwoRayGround;4=LogDistance");
    }

  // frequency
  double freq = 0.0;
  if ((m_80211mode == 1)
      || (m_80211mode == 3))
    {
      // 802.11p 5.9 GHz
      freq = 5.9e9;
    }
  else
    {
      // 802.11b 2.4 GHz
      freq = 2.4e9;
    }

  // Setup propagation models
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  if (m_lossModel == 3)
    {
      // two-ray requires antenna height (else defaults to Friss)
      wifiChannel.AddPropagationLoss (m_lossModelName, "Frequency", DoubleValue (freq), "HeightAboveZ", DoubleValue (1.5));
    }
  else
    {
      wifiChannel.AddPropagationLoss (m_lossModelName, "Frequency", DoubleValue (freq));
    }

  // Propagation loss models are additive.
  if (m_fading != 0)
    {
      // if no obstacle model, then use Nakagami fading if requested
      wifiChannel.AddPropagationLoss ("ns3::NakagamiPropagationLossModel");
    }
  wifiChannel.AddPropagationLoss  ("ns3::RangePropagationLossModel", "MaxRange", DoubleValue(TRANSMISSION_RANGE));
  // the channel
  Ptr<YansWifiChannel> channel = wifiChannel.Create ();

  // The below set of helpers will help us to put together the wifi NICs we want
  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  wifiPhy.SetChannel (channel);
  // ns-3 supports generate a pcap trace
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);

  YansWavePhyHelper wavePhy =  YansWavePhyHelper::Default ();
  wavePhy.SetChannel (channel);
  wavePhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);

  // Setup WAVE PHY and MAC
  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default ();
  WaveHelper waveHelper = WaveHelper::Default ();
  Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default ();
  if (m_verbose)
    {
      wifi80211p.EnableLogComponents ();      // Turn on all Wifi 802.11p logging
      // likewise, turn on WAVE PHY logging
      waveHelper.EnableLogComponents ();
    }

  WifiHelper wifi;

  // Setup 802.11b stuff
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);

  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode",StringValue (m_phyModeB),
                                "ControlMode",StringValue (m_phyModeB));

  // Setup 802.11p stuff
  wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                      "DataMode",StringValue (m_phyMode),
                                      "ControlMode",StringValue (m_phyMode));

  // Setup WAVE-PHY stuff
  waveHelper.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                      "DataMode",StringValue (m_phyMode),
                                      "ControlMode",StringValue (m_phyMode));

  // Set Tx Power
  wifiPhy.Set ("TxPowerStart",DoubleValue (m_txp));
  wifiPhy.Set ("TxPowerEnd", DoubleValue (m_txp));
  wavePhy.Set ("TxPowerStart",DoubleValue (m_txp));
  wavePhy.Set ("TxPowerEnd", DoubleValue (m_txp));

  // Add an upper mac and disable rate control
  WifiMacHelper wifiMac;
  wifiMac.SetType ("ns3::AdhocWifiMac");
  QosWaveMacHelper waveMac = QosWaveMacHelper::Default ();

  // Setup net devices

  if (m_80211mode == 3)
    {
      m_adhocTxDevices = waveHelper.Install (wavePhy, waveMac, m_adhocTxNodes);
    }
  else if (m_80211mode == 1)
    {
      m_adhocTxDevices = wifi80211p.Install (wifiPhy, wifi80211pMac, m_adhocTxNodes);
    }
  else
    {
      m_adhocTxDevices = wifi.Install (wifiPhy, wifiMac, m_adhocTxNodes);
    }

  if (m_asciiTrace != 0)
    {
      AsciiTraceHelper ascii;
      Ptr<OutputStreamWrapper> osw = ascii.CreateFileStream ( (m_trName + ".tr").c_str ());
      wifiPhy.EnableAsciiAll (osw);
      wavePhy.EnableAsciiAll (osw);
    }
  if (m_pcap != 0)
    {
      wifiPhy.EnablePcapAll ("vanet-routing-compare-pcap");
      wavePhy.EnablePcapAll ("vanet-routing-compare-pcap");
    }
}

void
VanetRoutingExperiment::SetupWaveMessages ()
{
  // WAVE PHY mode
  // 0=continuous channel; 1=channel-switching
  int chAccessMode = 0;
  if (m_80211mode == 3)
    {
      chAccessMode = 1;
    }

  m_waveBsmHelper.Install (m_adhocTxInterfaces,
                           Seconds (m_TotalSimTime),
                           m_wavePacketSize,
                           Seconds (m_waveInterval),
                           // GPS accuracy (i.e, clock drift), in number of ns
                           m_gpsAccuracyNs,
                           m_txSafetyRanges,
                           chAccessMode,
                           // tx max delay before transmit, in ms
                           MilliSeconds (m_txMaxDelayMs));

  // fix random number streams
  m_streamIndex += m_waveBsmHelper.AssignStreams (m_adhocTxNodes, m_streamIndex);
}

void
VanetRoutingExperiment::SetupRoutingMessages ()
{
  m_routingHelper->Install (m_adhocTxNodes,
                            m_adhocTxDevices,
                            m_adhocTxInterfaces,
                            m_TotalSimTime,
                            m_protocol,
                            m_nSinks,
                            m_routingTables);
}

void
VanetRoutingExperiment::SetupScenario ()
{
  // member variable parameter use
  // defaults or command line overrides,
  // except where scenario={1,2,3,...}
  // have been specified, in which case
  // specify parameters are overwritten
  // here to setup for specific scenarios

  // certain parameters may be further overridden
  // i.e. specify a scenario, override tx power.

  if (m_scenario == 1)
    {
      // 40 nodes in RWP 300 m x 1500 m synthetic highway, 10s
      m_traceFile = (ACCIDENT_MODE == 0) ? "scratch/ns2mobility.tcl" : "src/wave/examples/ns2mobility_accident.tcl";
      m_logFile = "";
      m_mobility = M_MOBILITY;
      if (m_nNodes == 156)
        {
          m_nNodes = NUM_OF_NODES;
        }
      if (m_TotalSimTime == 300.01)
        {
          m_TotalSimTime = M_TOTAL_SIM_TIME;
        }
    }
  else if (m_scenario == 2)
    {
      // Realistic vehicular trace in 4.6 km x 3.0 km suburban Zurich
      // "low density, 99 total vehicles"
      m_traceFile = "src/wave/examples/low99-ct-unterstrass-1day.filt.7.adj.mob";
      m_logFile = "low99-ct-unterstrass-1day.filt.7.adj.log";
      m_mobility = 1;
      m_nNodes = 99;
      m_TotalSimTime = 300.01;
      m_nodeSpeed = 0;
      m_nodePause = 0;
      m_CSVfileName = "low_vanet-routing-compare.csv";
      m_CSVfileName = "low_vanet-routing-compare2.csv";
    }
}

void
VanetRoutingExperiment::WriteCsvHeader ()
{
  //blank out the last output file and write the column headers
  std::ofstream out (m_CSVfileName.c_str ());
  out << "SimulationSecond," <<
    "ReceiveRate," <<
    "PacketsReceived," <<
    "NumberOfSinks," <<
    "RoutingProtocol," <<
    "TransmissionPower," <<
    "WavePktsSent," <<
    "WavePtksReceived," <<
    "WavePktsPpr," <<
    "ExpectedWavePktsReceived," <<
    "ExpectedWavePktsInCoverageReceived," <<
    "BSM_PDR1," <<
    "BSM_PDR2," <<
    "BSM_PDR3," <<
    "BSM_PDR4," <<
    "BSM_PDR5," <<
    "BSM_PDR6," <<
    "BSM_PDR7," <<
    "BSM_PDR8," <<
    "BSM_PDR9," <<
    "BSM_PDR10," <<
    "MacPhyOverhead" <<
    std::endl;
  out.close ();

  std::ofstream out2 (m_CSVfileName2.c_str ());
  out2 << "BSM_PDR1,"
       << "BSM_PDR2,"
       << "BSM_PDR3,"
       << "BSM_PDR4,"
       << "BSM_PDR5,"
       << "BSM_PDR6,"
       << "BSM_PDR7,"
       << "BSM_PDR8,"
       << "BSM_PDR9,"
       << "BSM_PDR10,"
       << "AverageRoutingGoodputKbps,"
       << "MacPhyOverhead"
       << std::endl;
  out2.close ();
}

int
main (int argc, char *argv[])
{
  CommandLine private_cmd;
  private_cmd.AddValue ("START_TIME", "The tolerlent differrence", START_TIME);
  private_cmd.AddValue ("MALLICIAS_RATE", "The rate of MALLICIAS Node", MALLICIAS_RATE);
  private_cmd.AddValue ("FLOW_RATE_OF_MALLICIAS", "The rate of heavyness of attack", FLOW_RATE_OF_MALLICIAS);
  private_cmd.AddValue ("THRESHOLD", "The tolerlent differrence", THRESHOLD);
  private_cmd.Parse (argc, argv);

  VanetRoutingExperiment experiment;
  experiment.Simulate (argc, argv);

  switch (EXECUTION_MODE) {
    case 0:
    case 3:
      {
        // 結果をCSVファイルに出力
        make_result();
        // メモリをフリーにする
        mem_free();
        make_frequency_csv();
      }
      break;

    case 1:
    case 2:
      {
        // 観測結果を出力する
        for(int i = 0; i < NUM_OF_NODES; i++){
          std::cout << "\n" << std::endl;
          std::cout << " i'm = " << i << std::endl;
          for(auto itr = tmp[i].begin(); itr != tmp[i].end(); itr++){
            std::cout << "flow_ave = " << itr->flow_ave << " num_node = " << itr->num_node << std::endl;
          }
          // for(auto itr = tmp_f[i].begin(); itr != tmp_f[i].end(); itr++){
          //   std::cout << *itr << std::endl;
          // }
          // for(auto itr = tmp_b[i].begin(); itr != tmp_b[i].end(); itr++){
          //   std::cout << *itr << std::endl;
          // }
        }
        // 結果をCSVファイルに出力
        make_result();
        // フロウの平均値を出力
        make_result_accident();
      }
      break;
  }
}
