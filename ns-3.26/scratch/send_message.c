#include "common_header.h"
#include "client.c"
#include "socket.c"

int SEND_PORT = 50000;
int RECV_PORT = 50001;

int main(int argc, char** argv)
{

  Client c;
  Server s;

  c.SetConnection(SEND_PORT);
  s.SetConnection(RECV_PORT);

  for (int i = 0; i < 100000; i++){
    if (c.IsConnected() == false) {
      c.Connection();
    }
    c.SendMessage();
    s.WaitMessage();
  }

  c.CloseConnection();
  s.CloseConnection();
}
