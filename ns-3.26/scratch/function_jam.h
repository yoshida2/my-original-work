// 出力用のファイルを作成
FILE* make_result();
// 事故時のフロウの変化を記載()
void  make_result_accident();
// 観測データを出力
FILE* print_observed_data(FILE *fp);
// 自分で用意したメモリを食うものはここで空にする
void mem_free ();

void save_frequency(double flow, std::unordered_map<int, int> &frequency);
void make_frequency_csv();
void setConnectionToServer();
