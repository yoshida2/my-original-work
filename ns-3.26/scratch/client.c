#include "common_header.h"
#include "client.h"

#define PORT 8080

int Client::SetConnection(int send_port){
  // // IPv4 TCP のソケットを作成する
  // if((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
  //     fprintf( stdout, "socket error\n" );
  //     std::cout << "socket error\n" << std::endl;
  //     return -1;
  // }
  // // hostsからホスト情報をを読み出す
  // info = gethostbyname("localhost");
  // if ( !info ){
  //     fprintf( stdout, "gethostbyname error\n" );
  //     std::cout << "gethostbyname error\n" << std::endl;
  //     return -1;
  // }
  // // serv = getservbyname( "vboxd", "tcp" );
  // // serv = getservbyport(START_PORT + count, "tcp");
  // // count++;
  // // if ( !serv ){
  // //     close( fd );
  // //     fprintf( stdout, "getservbyname error\n" );
  // //     std::cout << "getservbyname error\n" << std::endl;
  // //     return -1;
  // // }
  // // 送信先アドレスとポート番号を設定する
  // addr.sin_family = AF_INET;
  // addr.sin_port = send_port;
  // // count++;
  // addr.sin_addr = *(struct in_addr *)(info->h_addr_list[0]);

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
      printf("\n Socket creation error \n");
      return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  info = gethostbyname("localhost");
  if ( !info ){
      fprintf( stdout, "gethostbyname error\n" );
      std::cout << "gethostbyname error\n" << std::endl;
      return -1;
  }
  std::cout << info << std::endl;

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = PORT;
  serv_addr.sin_addr = *(struct in_addr *)(info->h_addr_list[0]);

  // // Convert IPv4 and IPv6 addresses from text to binary form
  // if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
  // {
  //     printf("\nInvalid address/ Address not supported \n");
  //     return -1;
  // }

  return 0;
}

// void Client::FlagConnected(){
//   is_connected = true;
// }
//
// void Client::UnFlagConnected(){
//   is_connected = false;
// }
//
// bool Client::IsConnected(){
//   return is_connected;
// }

int Client::Connection(){
  // サーバ接続
  // if ( connect(fd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0 ) {
  //   std::cout << "connetion error" << std::endl;
  //   return -1;
  // }
  // FlagConnected();

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
      printf("\nConnection Failed \n");
      return -1;
  }

  return 0;
}

int Client::SendMessage(){
  // sprintf( buff, "This is send Message" );
  // // パケットを送信
  // std::cout << "send mesage\n" << std::cout;
  // // if ( send( fd, buff, strlen(buff), 0 ) < 0 ) {
  // //   fprintf( stdout, "send error\n" );
  // //   std::cout << "send error\n" << std::endl;
  // //   exit(0);
  // //   return -1;
  // // }
  //
  // if ( send( fd, buff, strlen(buff), 0 ) < 0 ) {
  //   fprintf( stdout, "send error\n" );
  //   std::cout << "send error\n" << std::endl;
  //   // exit(0);
  //   return -1;
  // }
  //
  // // int fd2;
  // // // パケット受信待ち状態とする。待ちうけるコネクト要求は１
  // // if ( listen( fd, 1 ) < 0 ) {
  // //     fprintf( stdout, "listen error\n" );
  // //     std::cout << "listen error" << std::cout;
  // //     return -1;
  // // }
  // // // クライアントからの接続を確立する
  // // if (( fd2 = accept(fd, (struct sockaddr *)&from_addr, &len )) < 0 ) {
  // //     fprintf( stdout, "accept error : fd2 = %d\n", fd2 );
  // //     return -1;
  // // }
  // std::cout << "!" << std::endl;
  // char buf[2048];
  // // 受信バッファを初期化する
  // memset(buf, 0, sizeof(buf));
  // // パケット受信
  // if ( recv( fd, buf, sizeof(buf), 0 ) > 0 ) {
  //     fprintf( stdout, "recv\n");
  //     // std::cout << "recv" << std::cout;
  //     std::cout << buf << std::endl;
  //     return 1;
  // }

  send(sock , hello , strlen(hello) , 0 );
  printf("Hello message sent\n");
  valread = read( sock , buffer, 1024);
  printf("%s\n",buffer );

  return 0;
}

int Client::CloseConnection(){
  close(sock);
  // UnFlagConnected();
  return 0;
}
