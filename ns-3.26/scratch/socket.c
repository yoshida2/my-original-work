#include "common_header.h"
#include "socket.h"

#define PORT 8080

int Server::SetConnection(int recv_port){
  // // ソケットを作成する
  // if (( fd = socket(AF_INET, SOCK_STREAM, 0 )) < 0 ) {
  //     fprintf( stdout, "socket error : fd = %d\n", fd );
  //     std::cout << "socket error" << std::cout;
  //     return -1;
  // }
  //
  // // serv = getservbyname( "vboxd", "tcp" );
  // // serv = getservbyport(START_PORT + count, "tcp");
  // // count++;
  // // if ( !serv ){
  // //     close( fd );
  // //     return -1;
  // // }
  // //
  // // fprintf(stderr, "%d\n", serv->s_port);
  // // IPアドレス、ポート番号を設定
  // addr.sin_family = AF_INET;
  // addr.sin_port = recv_port;
  // addr.sin_addr.s_addr = INADDR_ANY;
  // // バインドする
  // if ( bind( fd, (struct sockaddr *)&addr, sizeof(addr)) < 0 ) {
  //     std::cout << "bind error\n" << std::cout;
  //     return -1;
  // }
  //
  // // パケット受信待ち状態とする。待ちうけるコネクト要求は１
  // if ( listen( fd, 1 ) < 0 ) {
  //     fprintf( stdout, "listen error\n" );
  //     std::cout << "listen error" << std::cout;
  //     return -1;
  // }
  // // クライアントからの接続を確立する
  // if (( fd2 = accept(fd, (struct sockaddr *)&from_addr, &len )) < 0 ) {
  //     fprintf( stdout, "accept error : fd2 = %d\n", fd2 );
  //     return -1;
  // }
  // return 0;


  // Creating socket file descriptor
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
      perror("socket failed");
      exit(EXIT_FAILURE);
  }

  // // Forcefully attaching socket to the port 8080
  // if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
  //                                               &opt, sizeof(opt)))
  // {
  //     perror("setsockopt");
  //     exit(EXIT_FAILURE);
  // }
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = PORT;

  // Forcefully attaching socket to the port 8080
  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0)
  {
      perror("bind failed");
      exit(EXIT_FAILURE);
  }
  if (listen(server_fd, 1) < 0)
  {
      perror("listen");
      exit(EXIT_FAILURE);
  }
  sleep(10);
  if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)
  {
      perror("accept");
      exit(EXIT_FAILURE);
  }

  std::cout << "!" << std::endl;

  return 0;
}

int Server::WaitMessage(){
  // char buf[2048];
  // // 受信バッファを初期化する
  // memset(buf, 0, sizeof(buf));
  //
  // std::cout << "wait\n" << std::endl;
  //
  // // パケット受信
  // if ( recv( fd2, buf, sizeof(buf), 0 ) > 0 ) {
  //     fprintf( stdout, "recv\n");
  //     // std::cout << "recv" << std::cout;
  //     std::cout << buf << std::endl;
  //     // return 1;
  // }
  //
  // sleep(10);
  // char buff[2048];
  // // sprintf( buff, "This is send Message" );
  // std::cout << "!" << std::endl;
  // if ( send( fd2, buff, strlen(buff), 0 ) < 0 ) {
  //   fprintf( stdout, "send error\n" );
  //   std::cout << "send error\n" << std::endl;
  //   // exit(0);
  //   return -1;
  // }
  //
  // return 1;
  valread = read( new_socket , buffer, 1024);
  printf("%s\n",buffer );
  send(new_socket , hello , strlen(hello) , 0 );
  printf("Hello message sent\n");

  return 1;
}

int Server::CloseConnection(){
  // パケット送受信用ソケットクローズ
  close(server_fd);
  // 接続要求待ち受け用ソケットクローズ
  close(new_socket);
  // 受信データの出力
  // fprintf( stdout, "%s\n", buf );

  return 1;
}
