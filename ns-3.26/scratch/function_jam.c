#include "unique_int.c"

std::unordered_map<std::string, double> return_hash_true_positive_false_positive(){
  std::string true_positive = "true_positive";
  std::string false_positive = "false_positive";
  std::string accuracy = "accuracy";
  std::unordered_map<std::string, double> result;
  int num_malicias = real_mallicias.size();
  int num_normal = NUM_OF_NODES - num_malicias;
  int num_tp = 0;
  int num_fp = 0;

  for(auto itr = id_from_Ipv4adress.begin(); itr != id_from_Ipv4adress.end(); ++itr){
    if((real_mallicias.find(itr->first) != real_mallicias.end()) && (result_mallicias.find(itr->first) != result_mallicias.end())){
      num_tp++;
    } else if ((real_mallicias.find(itr->first) == real_mallicias.end()) && (result_mallicias.find(itr->first) != result_mallicias.end()))
      num_fp++;
  }

  if(num_malicias == 0){
    result[true_positive] = 0;
  } else {
    result[true_positive] = (double)num_tp / (double)num_malicias * 100;
  }

  if(num_normal == 0){
    result[false_positive] = 0;
  } else {
    result[false_positive] = (double)num_fp / (double)num_normal * 100;
  }

  if ((num_malicias + num_normal) == 0) {
    result[accuracy] = 0;
  } else {
    result[accuracy] = (double)(num_tp + (num_normal - num_fp)) / (double)(num_malicias + num_normal) * 100;
  }

  return result;
}
FILE* print_observed_data(FILE *fp){
  fprintf(fp, "\n");
  fprintf(fp, "mistakenly judged\n");

  int over255 = 0;
  int pointer = 0;
  // int real_flag = 0;
  // int result_flag = 0;
  std::string address_head = "10.1.";
  std::string string_pointer;
  std::string string_over255;
  std::string address_string;
  std::vector<viecle_data> tmp_viecle_dates;
  std::vector<calc_data> tmp_calc_dates;

  for(int i = 1; i <= NUM_OF_NODES; i++){
    // 桁の繰上げ
    // real_flag = 0;
    // result_flag = 0;
    pointer = i;
    over255 = 0;
    if(255 < pointer){
      over255++;
      pointer -= 255;
    }
    string_pointer = std::to_string(pointer);
    string_over255 = std::to_string(over255);
    address_string = address_head + string_over255 + "." + string_pointer;
    auto itr = id_from_Ipv4adress.find(address_string);
    uint32_t id_judger = my_data[itr->second].id_judger;

    // if(real_mallicias.find(itr->first) != real_mallicias.end()){
    //   real_flag = 1;
    // }
    // if(result_mallicias.find(itr->first) != result_mallicias.end()){
    //   result_flag = 1;
    // }
    // 誤検知した場合には観測データを出力
    if(/*result_flag == 1*/ true){
      fprintf(fp, "I'm = %s\n", itr->first.c_str());
      for(auto itr_observed = observed_nodes[id_judger].begin(); itr_observed != observed_nodes[id_judger].end(); ++itr_observed){
        if(itr_observed->id == itr->second){
          tmp_viecle_dates = itr_observed->tmp_v;
          tmp_calc_dates = itr_observed->tmp_c;

          fprintf(fp, "viecle_data");
          for(auto itr_vd = tmp_viecle_dates.begin(); itr_vd != tmp_viecle_dates.end(); ++itr_vd) {
            fprintf(fp, ", %d", (int)itr_vd->flow_ave);
            if((itr_vd - tmp_viecle_dates.begin()) == (signed int)(tmp_viecle_dates.size() - 1)){
              break;
            }
          }
          fprintf(fp, "\n");

          fprintf(fp, "calc_data");
          for(auto itr_cd = tmp_calc_dates.begin(); itr_cd != tmp_calc_dates.end(); ++itr_cd) {
            fprintf(fp, ", %d", (int)itr_cd->flow_ave);
            if((itr_cd - tmp_calc_dates.begin()) == (signed int)(tmp_calc_dates.size() - 1)){
              break;
            }
          }
          fprintf(fp, "\n");
          fprintf(fp, "\n");
          break;
        }
      }
    }
  }

  return fp;
}

void save_frequency(double flow, std::unordered_map<int, int> &frequency){

  // 十刻みint
  int tmp = ((int)flow / 10) * 10;
  frequency[tmp] = frequency[tmp] + 1;
}

void make_frequency_csv(){

  char filepath[256];
  char file_name[256];

  sprintf(file_name, "flow_aves_frequency_mallicious_rate_%lf", MALLICIAS_RATE);
  sprintf(filepath, "./result/%s", file_name);
  FILE *fp;
  fp = fopen(filepath, "w");

  fprintf(fp, "flow_ave, times\n");
  // グローバルパラメータ
  fprintf(fp, "\n");
  fprintf(fp, "flow_ave(global)\n");
  for(auto itr = flow_ave_frequency.begin(); itr != flow_ave_frequency.end(); itr++){
    fprintf(fp, "%d, %d\n", itr->first, itr->second);
  }
  // 改行
  fprintf(fp, "\n");
  // ローカルパラメータ
  fprintf(fp, "\n");
  fprintf(fp, "flow_own(local)\n");
  for (auto itr = flow_own_frequency.begin(); itr != flow_own_frequency.end(); itr++){
    fprintf(fp, "%d, %d\n", itr->first, itr->second);
  }

  fclose(fp);
}

void make_result_accident(){

  char filepath[256];
  char file_name[256];

  sprintf(file_name, "flow_aves_time_series_EXECUTION_MODE_%d", EXECUTION_MODE);
  sprintf(filepath, "./result/%s", file_name);
  FILE *fp;
  fp = fopen(filepath, "w");

  fprintf(fp, "time, flow_ave\n");
  for(auto itr = observed_flow_aves.begin(); itr != observed_flow_aves.end(); itr++){
    fprintf(fp, "%lf, %lf\n", itr->time, itr->flow_ave);
  }

  fclose(fp);
}

FILE* make_result (){
  // ラベルをつける
  char filepath[256];
  char file_name[256];
  int real_flag = 0;
  int result_flag = 0;
  int over255 = 0;
  int pointer = 0;
  std::string address_head = "10.1.";
  std::string string_pointer;
  std::string string_over255;
  std::string address_string;
  std::unordered_map<std::string, double> result = return_hash_true_positive_false_positive();

  sprintf(file_name, "accuracy_%lf_tpr_%lf_fpr_%lf_node_%d_mallicias_rate_%lf_START_TIME_%d_attack_rate_%d.csv", result["accuracy"],
  result["true_positive"], result["false_positive"], NUM_OF_NODES ,MALLICIAS_RATE ,START_TIME, FLOW_RATE_OF_MALLICIAS);
  sprintf(filepath, "./result/%s", file_name);
  FILE *fp;
  fp = fopen(filepath, "w");

  fprintf(fp, "NUM_OF_NODES, %d, MALLICIAS_RATE, %lf\n",NUM_OF_NODES, MALLICIAS_RATE);
  fprintf(fp, "address, real, result, sd_viecle, sd_calc, Vf, Kj, flow_ave, flow_own, t, judger\n");

  for(int i = 1; i <= NUM_OF_NODES; i++){
    // 桁の繰上げ
    pointer = i;
    over255 = 0;
    if(255 < pointer){
      over255++;
      pointer -= 255;
    }
    string_pointer = std::to_string(pointer);
    string_over255 = std::to_string(over255);
    address_string = address_head + string_over255 + "." + string_pointer;
    auto itr = id_from_Ipv4adress.find(address_string);
    real_flag = 0;
    result_flag = 0;

    if(real_mallicias.find(itr->first) != real_mallicias.end()){
      real_flag = 1;
    }
    if(result_mallicias.find(itr->first) != result_mallicias.end()){
      result_flag = 1;
    }
    fprintf(fp, "%s, %d, %d, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %d\n",
        itr->first.c_str(),
        real_flag,
        result_flag,
        my_data[itr->second].sq2_viecle,
        my_data[itr->second].sq2_calc,
        my_data[itr->second].Vf,
        my_data[itr->second].Kj,
        my_data[itr->second].flow_recv,
        my_data[itr->second].flow_own,
        my_data[itr->second].t,
        my_data[itr->second].id_judger
    );
  }
  fprintf(fp, "\n");
  if(EXECUTION_MODE == 0 || EXECUTION_MODE == 3){
    print_observed_data(fp);
  }
  fclose(fp);

  std::cout << " file_name = " << file_name << std::endl;

  return fp;

}

void mem_free (){
  real_mallicias.clear();
  result_mallicias.clear();
  id_from_Ipv4adress.clear();
  for (int i = 0; i < NUM_OF_NODES; i++){

    my_data[i].id = 0;
    my_data[i].calc_density_flag = 1;
    my_data[i].log_time_first = 1;
    my_data[i].flow_own = 0;
    my_data[i].flow_recv = 0;
    my_data[i].speed_ave = 0;
    my_data[i].density_calc = 0;
    my_data[i].Vf = 100;
    my_data[i].Kj = 10;
    my_data[i].speed_past.clear();
    my_data[i].density_past.clear();

    observed_nodes[i].clear();
    access_log[i].clear();
    access_speed_log[i].clear();
    access_flow_log[i].clear();
    access_density_log[i].clear();
  }
}

void setConnectionToServer() {

  struct sockaddr_in serv_addr;
  // char hello[2048] = "Hello from client";
  // char buffer[1024] = {0};
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
      printf("\n Socket creation error \n");
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  struct hostent *info;
  info = gethostbyname("localhost");
  if ( !info ){
      fprintf( stdout, "gethostbyname error\n" );
      std::cout << "gethostbyname error\n" << std::endl;
  }
  std::cout << info << std::endl;

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);
  serv_addr.sin_addr = *(struct in_addr *)(info->h_addr_list[0]);

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
      printf("\nConnection Failed \n");
  }
}

// socket -----

std::string SendCommand ( std::string command ) {
  char buffer[ BUFFER_SIZE ] = {0};

  send(sock, command.c_str(), strlen(command.c_str()) , 0);
  valread = read( sock , buffer, BUFFER_SIZE);

  return buffer;
}

std::string GetVehicleSpeed( std::string vehicle_ID ) {
  std::string command = GET_VEHICLE_SPEED + "," + vehicle_ID;
  return SendCommand( command );
}

std::string nextStep () {
  return SendCommand( NEXT_STEP );
}

std::string GetLoadedVehicles() {
  // std::cout << SendCommand( LOADED_VEHICLES ) << std::endl;
  return SendCommand( LOADED_VEHICLES );
}
