#include "unique_int.h"

UniqueInt::UniqueInt() {
  unique_int = 0;
}

int UniqueInt::get(){
  unique_int++;
  return unique_int - 1;
}

void UniqueInt::show(){
  std::cout << unique_int << std::endl;
}
