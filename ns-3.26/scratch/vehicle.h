#include "common_header.h"
#include "ns3_header.h"
//#include "vars.h"

struct position {
  double x;
  double y;
};

struct info {
  double speed;
  double accele;
  struct position pos;
};

class Vehicle {
public:
  // vars
  std::unordered_map<double, info> time_stamp;
  struct info state;
  NodeContainer node;
  NetDeviceContainer device;

  // ----- constractor -----
  Vehicle();
  Vehicle( std::string vehicle_ID );
  Vehicle(const Vehicle& r) {}

  // ----- instance method -----
  double AdequateSpeed();
  Wifi80211pHelper setWifi80211p();
  YansWifiPhyHelper setupWifiPhy();
  NqosWaveMacHelper setWifi80211pMac ();
  YansWifiChannelHelper setWifiChannel();
};
