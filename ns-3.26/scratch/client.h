#include "common_header.h"

class Client {
public:
  // int fd;
  // bool is_connected = false;
  struct hostent *info;
  // struct servent *serv;
  // struct sockaddr_in addr;
  // char buff[2048];
  // socklen_t len = sizeof(struct sockaddr_in);
  // struct sockaddr_in from_addr;

  struct sockaddr_in address;
  int sock = 0, valread;
  struct sockaddr_in serv_addr;
  char hello[2048] = "Hello from client";
  char buffer[1024] = {0};

  int SetConnection(int send_port);
  int Connection();
  int SendMessage();
  int CloseConnection();
  // void FlagConnected();
  // void UnFlagConnected();
  // bool IsConnected();
};
