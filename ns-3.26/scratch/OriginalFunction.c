#include "common_header.h"
#include "ns3_header.h"

std::string SendCommand ( std::string command ) {
  char buffer[ BUFFER_SIZE ] = {0};

  send(sock, command.c_str(), strlen(command.c_str()) , 0);
  valread = read( sock , buffer, BUFFER_SIZE);

  return buffer;
}

std::string nextStep () {
  return SendCommand( NEXT_STEP );
}

std::string GetLoadedVehicles() {
  // std::cout << SendCommand( LOADED_VEHICLES ) << std::endl;
  return SendCommand( LOADED_VEHICLES );
}

std::string GetVehicleSpeed( std::string vehicle_ID ) {
  std::string command = GET_VEHICLE_SPEED + "," + vehicle_ID;
  return SendCommand( command );
}

std::string GetVehicleAccele( std::string vehicle_ID ) {
  std::string command = GET_VEHICLE_ACCELE + "," + vehicle_ID;
  return SendCommand( command );
}

std::string GetVehiclePosition( std::string vehicle_ID ) {
  std::string command = GET_VEHICLE_POSITION + "," + vehicle_ID;
  return SendCommand( command );
}


void newVehicle( std::string vehicle_ID ){
  vehicles_list.push_back( vehicle_ID );
  vehicles[ vehicle_ID ] = new Vehicle(vehicle_ID);
  // vehicles[ vehicle_ID ]->setWirelessDevice();
}

void deleteVehicle ( std::string vehicle_ID ) {
  auto index = std::find( vehicles_list.begin(), vehicles_list.end(), vehicle_ID );
  if ( index != vehicles_list.end() ){
    vehicles_list.erase( index );
  }

  vehicles.erase( vehicle_ID );
}

void SetLoadedVehicles( std::string vehicles_string ) {
  std::stringstream ss{ vehicles_string };
  std::string vehicle_ID;

  if ( ((std::string)vehicles_string).find(NO_LOADED_VEHICLE) != std::string::npos ) {
  } else {
    while ( std::getline(ss, vehicle_ID, ',') ) {
      newVehicle( vehicle_ID );
    }
  }
}

void SetVehicleSpeed (std::string vehicle_ID, std::string speed) {
  vehicles[ vehicle_ID ]->state.speed = std::stod( speed );
  // std::cout << std::stod( speed ) << std::endl;
}

void SetVehicleAccele (std::string vehicle_ID, std::string accele) {
  vehicles[ vehicle_ID ]->state.accele = std::stod( accele );
}

void SetVehiclePosition (std::string vehicle_ID, std::string positions) {
  // std::cout << position << std::endl;
  std::stringstream ss{ positions };
  std::string position;
  int index = 0;

  while ( std::getline(ss, position, ',') ) {
    if ( index == 0 ) {
      vehicles[ vehicle_ID ]->state.pos.x = std::stod( position );
    } else {
      vehicles[ vehicle_ID ]->state.pos.y = std::stod( position );
    }
    index++;
  }

  const Vector pos(
    vehicles[ vehicle_ID ]->state.pos.x,
    vehicles[ vehicle_ID ]->state.pos.y,
    1
  );
  vehicles[ vehicle_ID ]->node.Get(0)->GetObject<MobilityModel>()->SetPosition( pos );
  // std::cout
  //   << vehicles[ vehicle_ID ]->node.Get(0)->GetObject<MobilityModel>()->GetPosition()
  //   << std::endl;
}

void removeVehicleID ( std::string vehicle_ID ) {
  auto index = std::find( vehicles_list.begin(), vehicles_list.end(), vehicle_ID );
  if ( index != vehicles_list.end() ) {
    vehicles_list.erase( index );
  }
}

void eraseVehicles ( std::vector<std::string> unexist_vehicles_list ) {
  for ( std::string vehicle_ID : unexist_vehicles_list ) {
    deleteVehicle( vehicle_ID );
  }
}


void checkVehiclesExist () {
  std::vector<std::string> unexist_vehicles_list;

  for ( std::string vehicle_ID : vehicles_list) {
    if ( GetVehicleSpeed( vehicle_ID ) == NO_EXISTANCE ) {
      unexist_vehicles_list.push_back( vehicle_ID );
      std::cout << "NO_EXISTANCE: " << vehicle_ID << std::endl;
    }
  }

  eraseVehicles( unexist_vehicles_list );
}

void updateVehiclesList () {
  checkVehiclesExist();
  SetLoadedVehicles( GetLoadedVehicles () );
}

void setVehicleState ( std::string vehicle_ID ) {
  SetVehicleSpeed( vehicle_ID, GetVehicleSpeed( vehicle_ID ) );
  SetVehicleAccele( vehicle_ID, GetVehicleAccele( vehicle_ID ) );
  SetVehiclePosition( vehicle_ID, GetVehiclePosition( vehicle_ID ));
}

std::string updateVehicleSpeed ( std::string vehicle_ID ) {
  std::string adequate_speed = std::to_string( vehicles[ vehicle_ID ]->AdequateSpeed() );
  std::string command = UPDATE_VEHICLE_SPEED + "," + vehicle_ID + "," + adequate_speed;
  return SendCommand( command );
}
