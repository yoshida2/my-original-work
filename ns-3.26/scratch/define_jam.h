
#define ONLY_OBSEREVED_NODE_HAVE_TO_BE_COLLECTED 0

#define EXECUTION_MODE 1//0: 従来手法 1:提案手法 2: ただ取得 3: 従来手法の一つ前
#define ACCIDENT_MODE 0 //0: 無事故　1:事故　トレーズファイルを変更する
#define ATTACK_PATTERN 0
/*
0: normal
1: intelligent
2: randam
3: 事故時
4: 閾値を超えない攻撃
5: 良性車両を陥れる攻撃
*/

double  RELATIVE_SPEED_THRESHOLD = 15;
double  THRESHOLD_NUM = 7;
double  MALLICIAS_RATE = 20; //リシャスノードの割合(0 ~ 100)
#define ATTACKERS_PRIORITY 1 /* ０:フロウ優先，１：車両密度優先 ２：スピード優先*/
int     FLOW_RATE_OF_MALLICIAS = 90; //マリシャスノードがフローを変える割合(10 ~ 100)
#define DENSITY_RATE_OF_MALLICIOUS 70 //マリシャスノードが密度を変える割合(10 ~ 100)
#define SPEED_RATE_OF_MALLICIOUS 95

#define RATE 1.0 // 0.0 ~ 1.0

#define NUM_OF_NODES 100 // 40 + 100
int     START_TIME = NUM_OF_NODES * 5;
int     START_ATTACK_TIME = START_TIME + 1;
int     long_simulation = (
  ACCIDENT_MODE == 1 ||
  ATTACK_PATTERN == 4 ||
  ATTACK_PATTERN == 5
  ) ? true : false;
int     M_TOTAL_SIM_TIME = START_ATTACK_TIME + (long_simulation ? START_TIME : 10);

int     THRESHOLD_SPEED = 100000;

double  TRANSMISSION_RANGE = 500;
double  COLLISION_AVOIDANCE_RANGE = 100;
#define T_VALUE 3.045 //a = 0.01 自由度 7+7-2=12
#define M_MOBILITY 2
#define INTER_ARRIVAL_RATE 1
#define MAX_PACKETSIZE_OF_SPEED 10
#define MAX_PACKETSIZE_OF_FLOW  11
#define MAX_PACKETSIZE_OF_DENSITY 11
#define MAX_PACKETSIZE_OF_FLOW_AVE 11
#define MAX_PACKETSIZE_OF_POSITION 11
#define MAX_PACKETSIZE_OF_HOP_FLAG 1
#define TRANSMISSION_RATE 0.1
#define CONVERT_Ms_KMh 3.6
#define CONVERT_M_KM 0.001
#define TXPOWER 20

int IS_MALICIOUS = 0;
int IS_VICIOUS = 1;
int IS_OBSERVED = 2;

double  ACCELE_THRESHOLD = 100;
double  DENGER_SLOPE = - RELATIVE_SPEED_THRESHOLD / ACCELE_THRESHOLD;
double  THRESHOLD = 500; //RELATIVE_SPEED_THRESHOLD * 250.0 / 4.0;  // RELATIVE_SPEED_THRESHOLD * Kj / 4
int     NODE_OBSERVED_FOR_FLOW_AVE = (int)(NUM_OF_NODES / 2);

unsigned int T_SAMPLES = (ONLY_OBSEREVED_NODE_HAVE_TO_BE_COLLECTED) ? 7 : 7 * NUM_OF_NODES;
#define VALUE_RATE 1.0
#define DENSITY_DIFF 2
int     TRUST_SD_CALC = NUM_OF_NODES; //分散値として信頼できる値（主観的な値）
// int     TRUST_SD_VIECLE = T_SAMPLES; //分散値として信頼できる値（主観的な値）
int     TRUST_SD_VIECLE = NUM_OF_NODES; //分散値として信頼できる値（主観的な値）
NodeContainer GNC;
int time_count[NUM_OF_NODES] = {0};



// true_positeveとfalse_positeveをハッシュで返す
std::unordered_map<std::string, double> return_hash_true_positive_false_positive();

struct data_front{
  double flow_ave;
  double speed_ave;
  double position;
  int num_node;
};
struct own_data {
  uint32_t id = 0;
  uint32_t id_judger = 0;
  int calc_density_flag = 1;
  double log_time_first = 1;
  double flow_own = 0;
  double flow_recv = 0;
  double speed_ave = 0;
  double density_calc = 0;
  double Vf = 25.29093517;
  double Kj = 101.5159708;
  double t = 0;
  double sq2_viecle = 0;
  double sq2_calc = 0;
  std::vector<double> speed_past;
  std::vector<double> density_past;
};

// 統計用に車のデータ構造を定義
struct calc_data {
  double received_time;
  double flow_ave;
  double speed;
};
struct viecle_data {
  double received_time;
  double flow_ave;
  double speed;
};
struct observed_node {
  int is_observed_flag;
  uint32_t id;
  Ipv4Address observed_address;
  double flow_ave;
  double speed;
  double received_time;
  std::vector<viecle_data> tmp_v;
  std::vector<calc_data> tmp_c;
};
struct received_message{
  double received_time;
  double position;
  double acceleration;
  double density;
  double speed;
  double flow_ave;
  double flow;
  int    hop_flag = 0; //0:無事故　1:範囲内の事故　2:範囲外の事故
};
struct varification{
  int    varificationer_id;
  double received_time;
  double effective_time;
  int    varification_flag;
};

struct observed_node_chain{
  int is_observed_flag;
  uint32_t id;
  uint32_t itr_sequence;
  struct received_message observed_node;
  std::vector<uint32_t> pos_sequence;
  std::unordered_map<uint32_t, received_message> received_message_at_accident;
};
struct observed_flow_ave{
  double time;
  double flow_ave;
};
struct low_flow_ave_node{
  uint32_t sender_id;
  double   sender_position;
  double   sender_speed;
};
struct own_data my_data[NUM_OF_NODES];
std::vector<observed_node> observed_nodes[NUM_OF_NODES];
std::vector<observed_flow_ave> observed_flow_aves;

// 観測用データフロントを保存
std::vector<struct data_front> tmp[NUM_OF_NODES];
std::vector<uint32_t> tmp_f[NUM_OF_NODES];
std::vector<uint32_t> tmp_b[NUM_OF_NODES];

// 第一引数に自分のid, 第二引数に送信者のidを入れて管理, 自分のidには自分の情報を送信時に格納
std::unordered_map<uint32_t, received_message> own_received_messages[NUM_OF_NODES];
std::vector<observed_node_chain> observed_node_chains[NUM_OF_NODES];
std::unordered_map<uint32_t, varification> varifications[NUM_OF_NODES];
std::unordered_map<uint32_t, low_flow_ave_node> low_flow_nodes[NUM_OF_NODES];

// 実際の悪性ノードと出力の悪性ノード
std::unordered_map<std::string, int> real_mallicias;
std::unordered_map<std::string, int> result_mallicias;
// 種々の値決定に使用
std::unordered_map<std::string, double> access_log[NUM_OF_NODES];
// INTER_ARRIVAL_RATEから密度を計算するために使用
std::unordered_map<std::string, double> access_log_for_IAR[NUM_OF_NODES];
std::unordered_map<std::string, double> access_speed_log[NUM_OF_NODES];
std::unordered_map<std::string, double> access_flow_log[NUM_OF_NODES];
std::unordered_map<std::string, double> access_density_log[NUM_OF_NODES];
// アドレスとノードIDの対応
std::unordered_map<std::string, uint32_t> id_from_Ipv4adress;
double now_in_simulation = 0;

NS_LOG_COMPONENT_DEFINE ("vanet-routing-compare");

std::unordered_map<int, int> flow_ave_frequency;
std::unordered_map<int, int> flow_own_frequency;
// ns3 -----
#define SYNC_INTERVAL 1

// socket -----
#define PORT 8080
#define BUFFER_SIZE 1024

// api command -----
std::string HELLO = "Hello from client";
std::string NEXT_STEP = "next step";
std::string LOADED_VEHICLES = "loaded vehicles";
std::string GET_VEHICLE_SPEED = "get vehicle speed";
std::string GET_VEHICLE_ACCELE = "get vehicle accele";
std::string GET_VEHICLE_POSITION = "get vehicle position";
std::string UPDATE_VEHICLE_SPEED = "update vehicle speed";

std::string NO_EXISTANCE = "NO_EXISTANCE";
std::string NO_LOADED_VEHICLE = "NO_LOADED_VEHICLE";

// sumo
struct position {
  double x = 100000000;
  double y = 100000000;
};

struct vehicle {
  int isloaded = 0;
  double speed = 0;
  double accele = 0;
  struct position pos;
};

const Vector init_pos (
    100000,
    100000,
    1.5
);

#define NUM_OF_RSU 10
#define TRANSMISSION_RANGE_OF_RSU 100
