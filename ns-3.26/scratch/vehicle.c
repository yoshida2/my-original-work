#include "common_header.h"
#include "ns3_header.h"
#include "vehicle.h"

YansWifiChannelHelper Vehicle::setWifiChannel(){
  // Setup propagation models
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  // two-ray requires antenna height (else defaults to Friss)
  wifiChannel.AddPropagationLoss (m_lossModelName, "Frequency", DoubleValue (freq), "HeightAboveZ", DoubleValue (1.5));
  wifiChannel.AddPropagationLoss  ("ns3::RangePropagationLossModel", "MaxRange", DoubleValue( TRANSMISSION_RANGE ));

  return wifiChannel;
}

Wifi80211pHelper Vehicle::setWifi80211p() {
  Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default ();
  // Setup 802.11p stuff
  wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                      "DataMode",StringValue (m_phyMode),
                                      "ControlMode",StringValue (m_phyMode));

  return wifi80211p;
}

YansWifiPhyHelper Vehicle::setupWifiPhy(){
  YansWifiChannelHelper wifiChannel = setWifiChannel();
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss  ("ns3::RangePropagationLossModel"
    ,"MaxRange", DoubleValue( TRANSMISSION_RANGE )
  );

  Ptr<YansWifiChannel> channel = wifiChannel.Create ();
  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  wifiPhy.SetChannel (channel);
  // ns-3 supports generate a pcap trace
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);
    // Set Tx Power
  wifiPhy.Set ("TxPowerStart",DoubleValue ( TXPOWER ));
  wifiPhy.Set ("TxPowerEnd", DoubleValue ( TXPOWER ));

  return wifiPhy;
}

double Vehicle::AdequateSpeed() {
  return 10;
}

NqosWaveMacHelper Vehicle::setWifi80211pMac () {
  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default ();
  // NqosWaveMacHelper wifi80211pMac;
  // wifi80211pMac.SetType( "ns3::WifiMac",
  //   "BeaconGeneration", BooleanValue( true ),
  //   "BeaconInterval", TimeValue( Seconds( BEACON_INTERVAL ) )
  // );

  return wifi80211pMac;
}

void setMobility( NodeContainer node ) {
  MobilityHelper mobilityAdhoc;
  ObjectFactory pos;
  pos.SetTypeId ("ns3::RandomBoxPositionAllocator");
  pos.Set ("X", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1500.0]"));
  pos.Set ("Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=300.0]"));
  // we need antenna height uniform [1.0 .. 2.0] for loss model
  pos.Set ("Z", StringValue ("ns3::UniformRandomVariable[Min=1.0|Max=2.0]"));

  Ptr<PositionAllocator> taPositionAlloc = pos.Create ()->GetObject<PositionAllocator> ();
  // m_streamIndex += taPositionAlloc->AssignStreams (m_streamIndex);

  std::stringstream ssSpeed;
  ssSpeed << "ns3::UniformRandomVariable[Min=0.0|Max=30.0]";
  // std::stringstream ssPause;
  // ssPause << "ns3::ConstantRandomVariable[Constant=" << m_nodePause << "]";

  mobilityAdhoc.SetMobilityModel ("ns3::RandomWaypointMobilityModel",
                                  "Speed", StringValue (ssSpeed.str ()),
                                  // "Pause", StringValue (ssPause.str ()),
                                  "PositionAllocator", PointerValue (taPositionAlloc));
  mobilityAdhoc.SetPositionAllocator (taPositionAlloc);
  mobilityAdhoc.Install ( node );
}

int ctoi(const char c){
  if('0' <= c && c <= '9') return (c-'0');
  return -1;
}

Ipv4Address getUniqueAdress(std::string vehicle_ID) {
  std::string address_head = "10";

  std::string second_head;
  if ( vehicle_ID.find("left") != std::string::npos ) {
    second_head = "1";
  } else if ( vehicle_ID.find("left") != std::string::npos ) {
    second_head = "2";
  } else if ( vehicle_ID.find("down") != std::string::npos ) {
    second_head = "3";
  } else {
    second_head = "4";
  }

  int number = 0;
  for (int i = 0; i < (int)vehicle_ID.size(); i++) {
    if (i == 0){ continue; }

    if ( ctoi(vehicle_ID[ (int)vehicle_ID.size() - i ]) != -1 ) {
      number += ( ctoi(vehicle_ID[ (int)vehicle_ID.size() - i ]) * std::pow( 10, i - 1) );
    } else {
      break;
    }
  }

  int over255 = 0;
  while ( 255 < number ) {
    over255++;
    number -= 255;
  }

  std::string third_head = std::to_string( over255 );;
  std::string tail = std::to_string( number );

  std::string adress = address_head + "." + second_head + "." + third_head + "." + tail;

  return Ipv4Address( adress.c_str() );
}

//ビーコンからブロードキャスト信号を生成
void GenerateTraffic(Ptr<Socket> source, InetSocketAddress broad_addr){

  //ブロードキャスト信号か無線信号かのフラグを立てる
  uint8_t  buf[1024];
  std::string message = "unti";
  uint16_t bufsize = message.size();
  memcpy(buf, message.c_str(),bufsize);
  Ptr<Packet> beacon_packet = Create<Packet> (buf, bufsize);

  source->SetAllowBroadcast(true);
  source->Connect (broad_addr);
  source->Send (beacon_packet);

  Simulator::Schedule (Seconds (TRANSMISSION_RATE), &GenerateTraffic, source, broad_addr);
}

void setApplication( NodeContainer node, std::string vehicle_ID ) {
  Ptr<UniformRandomVariable> var = CreateObject<UniformRandomVariable> ();
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> source = Socket::CreateSocket ( node.Get(0), tid );
  InetSocketAddress local = InetSocketAddress ( getUniqueAdress( vehicle_ID ), SEND_PORT );
  InetSocketAddress broad_addr = InetSocketAddress (Ipv4Address ("255.255.255.255"), SEND_PORT);
  source->Bind (local);
  double delay = Simulator::Now ().GetSeconds () + var->GetValue (1.0,2.0);

  Simulator::Schedule( Seconds ( delay ), &GenerateTraffic, source, broad_addr);
}

void RecvHelloPacket( Ptr<Socket> socket ) {
  Ptr<Packet> packet;
  Address srcAddress;
  uint8_t buf[1024];
  memset(buf, 0, sizeof(buf));

  while ((packet = socket->RecvFrom (srcAddress))) {
    std::cout << "!!!" << std::cout;
  }
}

void setSinkSocket( NodeContainer node, std::string vehicle_ID ) {
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> sink = Socket::CreateSocket ( node.Get(0), tid );
  InetSocketAddress local = InetSocketAddress ( getUniqueAdress( vehicle_ID ), RECV_PORT );
  sink->Bind ( local );

  sink->SetRecvCallback( MakeCallback( &RecvHelloPacket) );
}

Vehicle::Vehicle() {
  // Wifi80211pHelper wifi80211p = setWifi80211p();
  // node.Create(1);
  // device = wifi80211p.Install ( setupWifiPhy(), setWifi80211pMac(), node );
  // setMobility ( node );
  // setApplication ( node );
  // setAdress( node );
}

void setBase( NetDeviceContainer device ) {
  Ipv4InterfaceContainer adhocTxInterfaces;
  Ipv4AddressHelper addressAdhoc;

  addressAdhoc.SetBase ("10.1.0.0", "255.255.0.0");
  adhocTxInterfaces = addressAdhoc.Assign ( device );
}

Vehicle::Vehicle( std::string vehicle_ID ) {
  Wifi80211pHelper wifi80211p = setWifi80211p();
  node.Create(1);
  device = wifi80211p.Install ( setupWifiPhy(), setWifi80211pMac(), node );
  setMobility ( node );
  std::cout << vehicle_ID << std::endl;

  InternetStackHelper internet;
  internet.Install ( node );
  // setBase( device );
  // PacketSocketHelper packetSocket;
  // packetSocket.Install ( node );
  setSinkSocket( node, vehicle_ID );
  setApplication( node, vehicle_ID );
  // node.Get(0)->GetObject<socket>();
  // setApplication ( node, vehicle_ID );
}
