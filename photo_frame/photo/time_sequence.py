#-*- coding: utf-8 -*-
from Tkinter import *
import os
import glob
import cv2
import shutil
import numpy as np
import sys

CANVAS_WIDTH = 1824
CANVAN_HEIGHT = 984


FPS = 10
INTERVAL = 1 #sec
RENDER_INTERVAL = 10 #sec
ORIGINAL_FIGURE_PATH = "./figure/"
FIGURES_DIR = "./figure_processing"
FIGURES_PATH = FIGURES_DIR + "/"
#----------------------------------------------------------------------

class MainWindow():

    #----------------

    def __init__(self, main):
        # canvas for image
        self.canvas = Canvas(main, width = CANVAS_WIDTH, height = CANVAN_HEIGHT)
        self.canvas.grid(row=0, column=0)
        #figures list
        self.setFigureslist()
        # images
        self.setFigures()
        # set first image on canvas
        self.image_on_canvas = self.canvas.create_image(0, 0, anchor = NW, image = self.my_images[self.my_image_number])

        self.startPhoto()
    #----------------
    def setFigureslist(self):
        figures_list = glob.glob(ORIGINAL_FIGURE_PATH + "*")
        sorted(figures_list, key=lambda f: os.stat(f).st_mtime, reverse=True)
        # append figures
        self.figures_list = []
        self.figures_list = figures_list
        self.figure_index = 0

    def makeFigures(self, start_figure, goal_figure):
        start_img = cv2.imread(start_figure)
        goal_img = cv2.imread(goal_figure)

        for index in range(0, FPS * INTERVAL):
            img = cv2.addWeighted(start_img, (float(FPS * INTERVAL - 1 - index) / float(FPS * INTERVAL - 1)), goal_img, (float(index) / float(FPS * INTERVAL - 1)), 2.2)
            cv2.imwrite("./figure_processing/" + str(index) + ".ppm", img)


    def resize(self, figure_name):
        img = cv2.imread(figure_name)
        original_hight = img.shape[0]
        original_width = img.shape[1]
        width = 0
        height = 0
        if (original_width * (float(CANVAN_HEIGHT) / float(CANVAS_WIDTH))) <= original_hight:
            width = original_width * (float(CANVAN_HEIGHT) / float(original_hight))
            height = CANVAN_HEIGHT
        else:
            width = CANVAS_WIDTH
            height = original_hight * (float(CANVAS_WIDTH) / float(original_width))
        img2 = cv2.resize(img, (int(width), int(height)))

        imageArray = np.zeros((CANVAN_HEIGHT, CANVAS_WIDTH, 3), np.uint8)
        if img2.shape[1] * (float(CANVAN_HEIGHT) / float(CANVAS_WIDTH)) < img2.shape[0]:
            for h in range(0, img2.shape[0]):
                for w in range(0, img2.shape[1]):
                    imageArray[h, int(w + (float(CANVAS_WIDTH) / 2.0) - (float(img2.shape[1]) / 2.0))] = img2[h, w]
        else:
            for h in range(0, img2.shape[0]):
                for w in range(0, img2.shape[1]):
                    imageArray[int(h + (float(CANVAN_HEIGHT) / 2.0) - (float(img2.shape[0]) / 2.0)), w] = img2[h, w]
        cv2.imwrite(figure_name, imageArray)

    def isFiguresUpdated(self):
        figures = glob.glob(ORIGINAL_FIGURE_PATH + "*")
        if (len(figures) != len(self.figures_list)):
            return True
        else:
            return False

    def setFigures(self):
        original_start_figure = str(self.figures_list[self.figure_index % len(self.figures_list)])
        original_goal_figure = str(self.figures_list[(self.figure_index + 1) % len(self.figures_list)])

        # get figures
        if not(os.path.isdir(FIGURES_DIR)):
            os.mkdir(FIGURES_DIR)

        while not(os.path.isfile(original_start_figure)):
            self.figure_index += 1
            original_start_figure = str(self.figures_list[self.figure_index % len(self.figures_list)])

        while not(os.path.isfile(original_goal_figure)):
            self.figure_index += 1
            original_goal_figure = str(self.figures_list[(self.figure_index + 1) % len(self.figures_list)])

        # オリジナル画像の拡張子取
        root, start_figure_ext = os.path.splitext(original_start_figure)
        root, goal_figure_ext = os.path.splitext(original_goal_figure)
        # tikinterはppmしか表示できないため，オリジナル画像をppmに変換する．
        if start_figure_ext == "ppm":
            shutil.copy(original_start_figure, FIGURES_PATH + "start.ppm")
        else:
            img = cv2.imread(original_start_figure)
            cv2.imwrite(FIGURES_PATH + "start.ppm", img)

        if goal_figure_ext == "ppm":
            shutil.copy(original_goal_figure, FIGURES_PATH + "goal.ppm")
        else:
            img = cv2.imread(original_goal_figure)
            cv2.imwrite(FIGURES_PATH + "goal.ppm", img)

        self.resize(FIGURES_PATH + "start.ppm")
        self.resize(FIGURES_PATH + "goal.ppm")
        self.makeFigures(FIGURES_PATH + "start.ppm", FIGURES_PATH + "goal.ppm")

        # get figures
        figures = glob.glob(FIGURES_PATH + "*")
        # append figures
        self.my_images = []
        for index in range(0, len(figures)):
            figure_name = FIGURES_PATH + str(index) + ".ppm"
            if os.path.isfile(figure_name):
                self.my_images.append(PhotoImage(file = figure_name))

        self.my_image_number = 0

    def startPhoto(self):
        # フォルダ内の要素数を確認することで更新があったかを確認する．
        if self.isFiguresUpdated():
            self.setFigureslist()

        # next image
        self.my_image_number += 1

        # return to first image
        if self.my_image_number == len(self.my_images):
        # if True:
            self.figure_index = (self.figure_index + 1) % len(self.figures_list)
            self.setFigures()
            self.canvas.itemconfig(self.image_on_canvas, image = self.my_images[0])
            root.after(RENDER_INTERVAL * 1000, self.startPhoto)
            # root.after(1, self.startPhoto)

        else:
            self.canvas.itemconfig(self.image_on_canvas, image = self.my_images[self.my_image_number])
            root.after(int(float(INTERVAL * 1000) / float(FPS)), self.startPhoto)

#----------------------------------------------------------------------

root = Tk()
MainWindow(root)
root.mainloop()
