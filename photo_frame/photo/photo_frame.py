#-*- coding: utf-8 -*-

import tkinter as tk
import time

root = tk.Tk()

canvas = tk.Canvas(
    root,               # 親要素をメインウィンドウに設定
    width = 500,        # 幅を設定
    height = 300,       # 高さを設定
    relief=tk.RIDGE,    # 枠線を表示
    bd=2                # 枠線の幅を設定
)
canvas.place(x=0, y=0)                # メインウィンドウ上に配置

img = tk.PhotoImage(file = 'sample1.ppm')  # 表示するイメージを用意
canvas.create_image(                    # キャンバス上にイメージを配置
    0,                                  # x座標
    0,                                  # y座標
    image = img,                        # 配置するイメージオブジェクトを指定
    anchor = tk.NW                      # 配置の起点となる位置を左上隅に指定
)

root.after(1000)

img = tk.PhotoImage(file = 'sample2.ppm')
# img = tk.PhotoImage(file = 'sample2.ppm')  # 表示するイメージを用意
# canvas.create_image(                    # キャンバス上にイメージを配置
#     0,                                  # x座標
#     0,                                  # y座標
#     image = img,                        # 配置するイメージオブジェクトを指定
#     anchor = tk.NW                      # 配置の起点となる位置を左上隅に指定
# )

root.mainloop()
