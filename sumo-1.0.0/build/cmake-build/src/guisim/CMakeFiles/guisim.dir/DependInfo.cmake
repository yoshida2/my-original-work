# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/guisim/GUIBaseVehicle.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIBaseVehicle.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIBusStop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIBusStop.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUICalibrator.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUICalibrator.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIChargingStation.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIChargingStation.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIContainer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIContainer.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIContainerStop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIContainerStop.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIDetectorWrapper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIDetectorWrapper.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIE2Collector.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIE2Collector.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIE3Collector.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIE3Collector.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIEdge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIEdge.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIEventControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIEventControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIInductLoop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIInductLoop.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIInstantInductLoop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIInstantInductLoop.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIJunctionWrapper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIJunctionWrapper.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUILane.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUILane.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUILaneSpeedTrigger.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUILaneSpeedTrigger.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUINet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUINet.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIParkingArea.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIParkingArea.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIPerson.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIPerson.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUITrafficLightLogicWrapper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUITrafficLightLogicWrapper.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUITransportableControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUITransportableControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUITriggeredRerouter.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUITriggeredRerouter.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIVehicle.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIVehicle.cpp.o"
  "/vagrant/sumo-1.0.0/src/guisim/GUIVehicleControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/guisim/CMakeFiles/guisim.dir/GUIVehicleControl.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
