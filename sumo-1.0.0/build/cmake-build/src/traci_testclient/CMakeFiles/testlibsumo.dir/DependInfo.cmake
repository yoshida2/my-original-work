# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/microsim/MSMoveReminder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/MSMoveReminder.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSNet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/MSNet.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSE2Collector.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSE2Collector.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSE3Collector.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSE3Collector.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSInductLoop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSInductLoop.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSMeanData_Net.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSMeanData_Net.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSRouteProbe.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSRouteProbe.cpp.o"
  "/vagrant/sumo-1.0.0/src/netload/NLBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/__/netload/NLBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci_testclient/testlibsumo_main.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci_testclient/CMakeFiles/testlibsumo.dir/testlibsumo_main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/cfmodels/CMakeFiles/microsim_cfmodels.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/lcmodels/CMakeFiles/microsim_lcmodels.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/output/CMakeFiles/microsim_output.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/pedestrians/CMakeFiles/microsim_pedestrians.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/trigger/CMakeFiles/microsim_trigger.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/actions/CMakeFiles/microsim_actions.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/mesosim/CMakeFiles/mesosim.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/libsumo/CMakeFiles/libsumostatic.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/PHEMlight/cpp/CMakeFiles/foreign_phemlight.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/vehicle/CMakeFiles/utils_vehicle.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/distribution/CMakeFiles/utils_distribution.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/shapes/CMakeFiles/utils_shapes.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/options/CMakeFiles/utils_options.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/geom/CMakeFiles/utils_geom.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/importio/CMakeFiles/utils_importio.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/iodevices/CMakeFiles/utils_iodevices.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/tcpip/CMakeFiles/foreign_tcpip.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
