# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/emissions/HelpersEnergy.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/HelpersEnergy.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/emissions/HelpersHBEFA.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/HelpersHBEFA.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/emissions/HelpersHBEFA3.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/HelpersHBEFA3.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/emissions/HelpersHarmonoise.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/HelpersHarmonoise.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/emissions/HelpersPHEMlight.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/HelpersPHEMlight.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/emissions/PHEMCEP.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/PHEMCEP.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/emissions/PHEMCEPHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/PHEMCEPHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/emissions/PollutantsInterface.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/PollutantsInterface.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
