# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/common/FileHelpers.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/FileHelpers.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/IDSupplier.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/IDSupplier.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/MsgHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/MsgHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/Parameterised.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/Parameterised.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/RGBColor.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/RGBColor.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/RandHelper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/RandHelper.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/SUMOTime.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/SUMOTime.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/SUMOVehicleClass.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/SUMOVehicleClass.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/StdDefs.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/StdDefs.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/StringTokenizer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/StringTokenizer.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/StringUtils.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/StringUtils.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/SysUtils.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/SysUtils.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/common/SystemFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/SystemFrame.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
