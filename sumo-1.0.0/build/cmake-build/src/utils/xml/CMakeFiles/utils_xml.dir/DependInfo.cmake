# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/xml/GenericSAXHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/GenericSAXHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SAXWeightsHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SAXWeightsHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMORouteHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMORouteHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMORouteLoader.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMORouteLoader.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMORouteLoaderControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMORouteLoaderControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributes.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributes.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributesImpl_Binary.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributesImpl_Binary.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributesImpl_Cached.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributesImpl_Cached.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributesImpl_Xerces.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributesImpl_Xerces.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXReader.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXReader.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOVehicleParserHelper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOVehicleParserHelper.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOXMLDefinitions.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/SUMOXMLDefinitions.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/XMLSubSys.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/XMLSubSys.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
