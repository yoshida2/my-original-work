# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIDanielPerspectiveChanger.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIDanielPerspectiveChanger.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIDialog_EditViewport.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIDialog_EditViewport.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIDialog_GLObjChooser.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIDialog_GLObjChooser.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIDialog_Options.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIDialog_Options.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIDialog_ViewSettings.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIDialog_ViewSettings.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIGlChildWindow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIGlChildWindow.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIMainWindow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIMainWindow.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUIPerspectiveChanger.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUIPerspectiveChanger.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/windows/GUISUMOAbstractView.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/GUISUMOAbstractView.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
