# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/gui/globjects/GUIGLObjectPopupMenu.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/GUIGLObjectPopupMenu.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/globjects/GUIGlObject.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/GUIGlObject.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/globjects/GUIGlObjectStorage.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/GUIGlObjectStorage.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/globjects/GUIGlObject_AbstractAdd.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/GUIGlObject_AbstractAdd.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/globjects/GUIPointOfInterest.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/GUIPointOfInterest.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/globjects/GUIPolygon.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/GUIPolygon.cpp.o"
  "/vagrant/sumo-1.0.0/src/utils/gui/globjects/GUIShapeContainer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/GUIShapeContainer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
