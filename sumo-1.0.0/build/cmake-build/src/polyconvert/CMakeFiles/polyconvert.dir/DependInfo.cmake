# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderArcView.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderArcView.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderDlrNavteq.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderDlrNavteq.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderOSM.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderOSM.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderVisum.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderVisum.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderXML.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderXML.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCNetProjectionLoader.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCNetProjectionLoader.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCPolyContainer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCPolyContainer.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCTypeDefHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCTypeDefHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCTypeMap.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/PCTypeMap.cpp.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/polyconvert_main.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/polyconvert/CMakeFiles/polyconvert.dir/polyconvert_main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/distribution/CMakeFiles/utils_distribution.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/shapes/CMakeFiles/utils_shapes.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/options/CMakeFiles/utils_options.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/geom/CMakeFiles/utils_geom.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/importio/CMakeFiles/utils_importio.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/iodevices/CMakeFiles/utils_iodevices.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/tcpip/CMakeFiles/foreign_tcpip.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
