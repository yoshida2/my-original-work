# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServer.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Edge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Edge.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_InductionLoop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_InductionLoop.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Junction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Junction.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Lane.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Lane.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_LaneArea.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_LaneArea.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_MultiEntryExit.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_MultiEntryExit.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_POI.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_POI.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Person.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Person.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Polygon.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Polygon.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Route.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Route.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Simulation.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Simulation.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_TrafficLight.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_TrafficLight.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_Vehicle.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_Vehicle.cpp.o"
  "/vagrant/sumo-1.0.0/src/traci-server/TraCIServerAPI_VehicleType.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/traci-server/CMakeFiles/traciserver.dir/TraCIServerAPI_VehicleType.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
