# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Additional.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Additional.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Attribute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Attribute.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Connection.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Connection.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Crossing.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Crossing.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Edge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Edge.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Junction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Junction.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Lane.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Lane.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Shape.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Shape.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_TLS.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_TLS.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
