# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNEAdditionalDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNEAdditionalDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNECalibratorDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNECalibratorDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNECalibratorFlowDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNECalibratorFlowDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNECalibratorRouteDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNECalibratorRouteDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNECalibratorVehicleTypeDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNECalibratorVehicleTypeDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNEDialogACChooser.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNEDialogACChooser.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNEDialog_About.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNEDialog_About.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNEDialog_AllowDisallow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNEDialog_AllowDisallow.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNEDialog_FixAdditionalPositions.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNEDialog_FixAdditionalPositions.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNEGenericParameterDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNEGenericParameterDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNERerouterDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNERerouterDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNERerouterIntervalDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNERerouterIntervalDialog.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/dialogs/GNEVariableSpeedSignDialog.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/GNEVariableSpeedSignDialog.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
