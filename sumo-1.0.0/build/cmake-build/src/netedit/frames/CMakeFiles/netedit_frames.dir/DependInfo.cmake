# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEAdditionalFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNEAdditionalFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEConnectorFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNEConnectorFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNECrossingFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNECrossingFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEDeleteFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNEDeleteFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNEFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEInspectorFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNEInspectorFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEPolygonFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNEPolygonFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEProhibitionFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNEProhibitionFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNESelectorFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNESelectorFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNETLSEditorFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/GNETLSEditorFrame.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
