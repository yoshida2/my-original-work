# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEConnection.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEConnection.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNECrossing.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNECrossing.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEEdge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEEdge.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEInternalLane.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEInternalLane.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEJunction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEJunction.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNELane.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNELane.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNENetElement.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNENetElement.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEProhibition.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEProhibition.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
