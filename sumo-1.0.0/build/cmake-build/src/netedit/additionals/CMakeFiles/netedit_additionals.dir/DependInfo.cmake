# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEAccess.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEAccess.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEAdditional.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEAdditional.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEAdditionalHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEAdditionalHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEBusStop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEBusStop.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNECalibrator.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNECalibrator.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNECalibratorFlow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNECalibratorFlow.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNECalibratorRoute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNECalibratorRoute.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNECalibratorVehicleType.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNECalibratorVehicleType.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEChargingStation.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEChargingStation.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEClosingLaneReroute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEClosingLaneReroute.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEClosingReroute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEClosingReroute.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEContainerStop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEContainerStop.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDestProbReroute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDestProbReroute.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDetector.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDetector.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDetectorE1.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDetectorE1.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDetectorE1Instant.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDetectorE1Instant.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDetectorE2.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDetectorE2.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDetectorE3.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDetectorE3.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDetectorEntry.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDetectorEntry.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEDetectorExit.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEDetectorExit.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEPOI.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEPOI.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEParkingArea.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEParkingArea.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEParkingAreaReroute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEParkingAreaReroute.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEParkingSpace.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEParkingSpace.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEPoly.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEPoly.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNERerouter.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNERerouter.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNERerouterInterval.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNERerouterInterval.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNERouteProbReroute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNERouteProbReroute.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNERouteProbe.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNERouteProbe.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEShape.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEShape.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEStoppingPlace.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEStoppingPlace.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEVaporizer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEVaporizer.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEVariableSpeedSign.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEVariableSpeedSign.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/additionals/GNEVariableSpeedSignStep.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/GNEVariableSpeedSignStep.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
