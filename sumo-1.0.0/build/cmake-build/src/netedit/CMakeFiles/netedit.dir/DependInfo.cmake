# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/GNEApplicationWindow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/GNEApplicationWindow.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/GNEAttributeCarrier.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/GNEAttributeCarrier.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/GNELoadThread.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/GNELoadThread.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/GNENet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/GNENet.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/GNEUndoList.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/GNEUndoList.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/GNEViewNet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/GNEViewNet.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/GNEViewParent.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/GNEViewParent.cpp.o"
  "/vagrant/sumo-1.0.0/src/netedit/netedit_main.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/CMakeFiles/netedit.dir/netedit_main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/windows/CMakeFiles/utils_gui_windows.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/globjects/CMakeFiles/utils_gui_globjects.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/div/CMakeFiles/utils_gui_div.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/settings/CMakeFiles/utils_gui_settings.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/images/CMakeFiles/utils_gui_images.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/gui/tracker/CMakeFiles/utils_gui_tracker.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/foxtools/CMakeFiles/utils_foxtools.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/eulerspiral/CMakeFiles/foreign_eulerspiral.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/frames/CMakeFiles/netedit_frames.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/changes/CMakeFiles/netedit_changes.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/netelements/CMakeFiles/netedit_netelements.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/additionals/CMakeFiles/netedit_additionals.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netedit/dialogs/CMakeFiles/netedit_dialogs.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netwrite/CMakeFiles/netwrite.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/vissim/CMakeFiles/netimport_vissim.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/vissim/typeloader/CMakeFiles/netimport_vissim_typeloader.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/vissim/tempstructs/CMakeFiles/netimport_vissim_tempstructs.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/distribution/CMakeFiles/utils_distribution.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/shapes/CMakeFiles/utils_shapes.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/options/CMakeFiles/utils_options.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/geom/CMakeFiles/utils_geom.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/importio/CMakeFiles/utils_importio.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/iodevices/CMakeFiles/utils_iodevices.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/tcpip/CMakeFiles/foreign_tcpip.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
