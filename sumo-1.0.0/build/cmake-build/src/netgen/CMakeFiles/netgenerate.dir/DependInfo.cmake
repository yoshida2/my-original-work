# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netgen/NGEdge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netgen/CMakeFiles/netgenerate.dir/NGEdge.cpp.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netgen/CMakeFiles/netgenerate.dir/NGFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGNet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netgen/CMakeFiles/netgenerate.dir/NGNet.cpp.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGNode.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netgen/CMakeFiles/netgenerate.dir/NGNode.cpp.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGRandomNetBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netgen/CMakeFiles/netgenerate.dir/NGRandomNetBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/netgen/netgen_main.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netgen/CMakeFiles/netgenerate.dir/netgen_main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/netwrite/CMakeFiles/netwrite.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/distribution/CMakeFiles/utils_distribution.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/shapes/CMakeFiles/utils_shapes.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/options/CMakeFiles/utils_options.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/geom/CMakeFiles/utils_geom.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/importio/CMakeFiles/utils_importio.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/iodevices/CMakeFiles/utils_iodevices.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/tcpip/CMakeFiles/foreign_tcpip.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
