# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetFlowLoader.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFDetFlowLoader.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetector.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFDetector.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetectorFlow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFDetectorFlow.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetectorHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFDetectorHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFEdge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFEdge.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFEdgeBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFEdgeBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFNet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFNet.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFRouteCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/RODFRouteCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/dfrouter_main.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/dfrouter/CMakeFiles/dfrouter.dir/dfrouter_main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/build/cmake-build/src/router/CMakeFiles/router.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/emissions/CMakeFiles/utils_emissions.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/PHEMlight/cpp/CMakeFiles/foreign_phemlight.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/vehicle/CMakeFiles/utils_vehicle.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/distribution/CMakeFiles/utils_distribution.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/shapes/CMakeFiles/utils_shapes.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/options/CMakeFiles/utils_options.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/xml/CMakeFiles/utils_xml.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/geom/CMakeFiles/utils_geom.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/common/CMakeFiles/utils_common.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/importio/CMakeFiles/utils_importio.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/utils/iodevices/CMakeFiles/utils_iodevices.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/build/cmake-build/src/foreign/tcpip/CMakeFiles/foreign_tcpip.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
