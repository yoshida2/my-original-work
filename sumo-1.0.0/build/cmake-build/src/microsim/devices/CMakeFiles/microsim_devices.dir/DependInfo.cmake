# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_BTreceiver.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_BTreceiver.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_BTsender.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_BTsender.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Battery.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Battery.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Bluelight.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Bluelight.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_DriverState.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_DriverState.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Emissions.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Emissions.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Example.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Example.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_FCD.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_FCD.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Routing.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Routing.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_SSM.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_SSM.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_ToC.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_ToC.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Transportable.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Transportable.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Tripinfo.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Tripinfo.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Vehroutes.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Vehroutes.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
