# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/microsim/MSBaseVehicle.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSBaseVehicle.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSCModel_NonInteracting.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSCModel_NonInteracting.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSContainer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSContainer.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSDriverState.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSDriverState.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSEdge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSEdge.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSEdgeControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSEdgeControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSEdgeWeightsStorage.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSEdgeWeightsStorage.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSEventControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSEventControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSGlobals.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSGlobals.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSInsertionControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSInsertionControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSInternalJunction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSInternalJunction.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSJunction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSJunction.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSJunctionControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSJunctionControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSJunctionLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSJunctionLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSLane.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSLane.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSLaneChanger.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSLaneChanger.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSLaneChangerSublane.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSLaneChangerSublane.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSLeaderInfo.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSLeaderInfo.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSLink.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSLink.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSLinkCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSLinkCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSLogicJunction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSLogicJunction.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSMoveReminder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSMoveReminder.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSNet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSNet.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSNoLogicJunction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSNoLogicJunction.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSParkingArea.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSParkingArea.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSRightOfWayJunction.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSRightOfWayJunction.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSRoute.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSRoute.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSRouteHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSRouteHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSStateHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSStateHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSStoppingPlace.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSStoppingPlace.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSTransportable.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSTransportable.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSTransportableControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSTransportableControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSVehicle.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSVehicle.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSVehicleContainer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSVehicleContainer.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSVehicleControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSVehicleControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSVehicleTransfer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSVehicleTransfer.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSVehicleType.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/CMakeFiles/microsim.dir/MSVehicleType.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
