# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSActuatedTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSActuatedTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSDelayBasedTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSDelayBasedTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSDeterministicHiLevelTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSDeterministicHiLevelTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSOffTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSOffTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSPhasedTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSPhasedTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSPushButton.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSPushButton.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSRailCrossing.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSRailCrossing.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSRailSignal.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSRailSignal.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLCongestionPolicy.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLCongestionPolicy.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLE2Sensors.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLE2Sensors.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLHiLevelTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLHiLevelTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLMarchingPolicy.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLMarchingPolicy.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPhasePolicy.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPhasePolicy.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPhaseTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPhaseTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPlatoonPolicy.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPlatoonPolicy.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPlatoonTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPlatoonTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPolicy.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPolicy.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPolicy3DStimulus.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPolicy3DStimulus.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPolicy5DFamilyStimulus.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPolicy5DFamilyStimulus.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPolicy5DStimulus.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPolicy5DStimulus.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPolicyBasedTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPolicyBasedTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLPolicyDesirability.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLPolicyDesirability.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLRequestPolicy.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLRequestPolicy.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLRequestTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLRequestTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLSensors.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLSensors.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSOTLWaveTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSOTLWaveTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSimpleTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSimpleTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSSwarmTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSSwarmTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSTLLogicControl.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSTLLogicControl.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/traffic_lights/MSTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/MSTrafficLightLogic.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
