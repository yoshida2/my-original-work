# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/microsim/actions/Command_SaveTLCoupledDet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/actions/CMakeFiles/microsim_actions.dir/Command_SaveTLCoupledDet.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/actions/Command_SaveTLCoupledLaneDet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/actions/CMakeFiles/microsim_actions.dir/Command_SaveTLCoupledLaneDet.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/actions/Command_SaveTLSState.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/actions/CMakeFiles/microsim_actions.dir/Command_SaveTLSState.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/actions/Command_SaveTLSSwitchStates.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/actions/CMakeFiles/microsim_actions.dir/Command_SaveTLSSwitchStates.cpp.o"
  "/vagrant/sumo-1.0.0/src/microsim/actions/Command_SaveTLSSwitches.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/microsim/actions/CMakeFiles/microsim_actions.dir/Command_SaveTLSSwitches.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
