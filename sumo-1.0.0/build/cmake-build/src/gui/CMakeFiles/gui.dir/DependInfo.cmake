# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/gui/GUIApplicationWindow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUIApplicationWindow.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/GUIGlobals.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUIGlobals.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/GUILoadThread.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUILoadThread.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/GUIManipulator.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUIManipulator.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/GUIRunThread.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUIRunThread.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/GUISUMOViewParent.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUISUMOViewParent.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/GUITLLogicPhasesTrackerWindow.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUITLLogicPhasesTrackerWindow.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/GUIViewTraffic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/GUIViewTraffic.cpp.o"
  "/vagrant/sumo-1.0.0/src/gui/TraCIServerAPI_GUI.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/gui/CMakeFiles/gui.dir/TraCIServerAPI_GUI.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
