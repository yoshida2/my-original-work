# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netimport/NIFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_ArcView.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_ArcView.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_DlrNavteq.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_DlrNavteq.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_ITSUMO.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_ITSUMO.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_MATSim.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_MATSim.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_OpenDrive.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_OpenDrive.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_OpenStreetMap.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_OpenStreetMap.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_RobocupRescue.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_RobocupRescue.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_SUMO.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_SUMO.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_VISUM.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIImporter_VISUM.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NILoader.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NILoader.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NINavTeqHelper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NINavTeqHelper.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIVisumTL.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIVisumTL.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLConnectionsHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIXMLConnectionsHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLEdgesHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIXMLEdgesHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLNodesHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIXMLNodesHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLPTHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIXMLPTHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLTrafficLightsHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIXMLTrafficLightsHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLTypesHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netimport/CMakeFiles/netimport.dir/NIXMLTypesHandler.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
