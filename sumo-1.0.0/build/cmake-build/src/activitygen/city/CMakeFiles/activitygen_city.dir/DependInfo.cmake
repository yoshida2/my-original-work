# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGAdult.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGAdult.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGBus.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGBus.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGBusLine.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGBusLine.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGCar.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGCar.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGChild.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGChild.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGCity.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGCity.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGDataAndStatistics.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGDataAndStatistics.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGHousehold.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGHousehold.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGPerson.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGPerson.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGPosition.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGPosition.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGSchool.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGSchool.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGStreet.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGStreet.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGTime.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGTime.cpp.o"
  "/vagrant/sumo-1.0.0/src/activitygen/city/AGWorkPosition.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/activitygen/city/CMakeFiles/activitygen_city.dir/AGWorkPosition.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
