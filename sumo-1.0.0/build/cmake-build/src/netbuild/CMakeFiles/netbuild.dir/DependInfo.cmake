# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netbuild/NBAlgorithms.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBAlgorithms.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBAlgorithms_Railway.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBAlgorithms_Railway.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBAlgorithms_Ramps.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBAlgorithms_Ramps.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBConnection.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBConnection.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBContHelper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBContHelper.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBDistrict.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBDistrict.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBDistrictCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBDistrictCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBEdge.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBEdge.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBEdgeCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBEdgeCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBFrame.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBFrame.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBHeightMapper.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBHeightMapper.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBHelpers.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBHelpers.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBLoadedSUMOTLDef.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBLoadedSUMOTLDef.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBLoadedTLDef.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBLoadedTLDef.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNetBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBNetBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNode.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBNode.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNodeCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBNodeCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNodeShapeComputer.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBNodeShapeComputer.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBOwnTLDef.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBOwnTLDef.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTLine.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBPTLine.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTLineCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBPTLineCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTPlatform.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBPTPlatform.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTStop.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBPTStop.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTStopCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBPTStopCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBParking.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBParking.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBRequest.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBRequest.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBSign.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBSign.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTrafficLightDefinition.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBTrafficLightDefinition.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBTrafficLightLogic.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTrafficLightLogicCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBTrafficLightLogicCont.cpp.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTypeCont.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netbuild/CMakeFiles/netbuild.dir/NBTypeCont.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
