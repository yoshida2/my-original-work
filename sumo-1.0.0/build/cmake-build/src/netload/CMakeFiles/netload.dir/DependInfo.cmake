# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netload/NLBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/NLBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/netload/NLDetectorBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/NLDetectorBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/netload/NLDiscreteEventBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/NLDiscreteEventBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/netload/NLEdgeControlBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/NLEdgeControlBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/netload/NLHandler.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/NLHandler.cpp.o"
  "/vagrant/sumo-1.0.0/src/netload/NLJunctionControlBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/NLJunctionControlBuilder.cpp.o"
  "/vagrant/sumo-1.0.0/src/netload/NLTriggerBuilder.cpp" "/vagrant/sumo-1.0.0/build/cmake-build/src/netload/CMakeFiles/netload.dir/NLTriggerBuilder.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "FLOAT_MATH_FUNCTIONS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/gdal"
  "src"
  "../../src"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
