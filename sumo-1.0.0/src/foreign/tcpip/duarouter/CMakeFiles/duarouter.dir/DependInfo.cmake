# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/duarouter/RODUAEdgeBuilder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/duarouter/CMakeFiles/duarouter.dir/RODUAEdgeBuilder.o"
  "/vagrant/sumo-1.0.0/src/duarouter/RODUAFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/duarouter/CMakeFiles/duarouter.dir/RODUAFrame.o"
  "/vagrant/sumo-1.0.0/src/duarouter/duarouter_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/duarouter/CMakeFiles/duarouter.dir/duarouter_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
