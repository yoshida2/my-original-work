# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netbuild/NBAlgorithms.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBAlgorithms.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBAlgorithms_Railway.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBAlgorithms_Railway.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBAlgorithms_Ramps.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBAlgorithms_Ramps.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBConnection.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBConnection.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBContHelper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBContHelper.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBDistrict.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBDistrict.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBDistrictCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBDistrictCont.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBEdge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBEdge.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBEdgeCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBEdgeCont.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBFrame.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBHeightMapper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBHeightMapper.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBHelpers.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBHelpers.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBLoadedSUMOTLDef.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBLoadedSUMOTLDef.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBLoadedTLDef.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBLoadedTLDef.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNetBuilder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBNetBuilder.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNode.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBNode.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNodeCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBNodeCont.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBNodeShapeComputer.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBNodeShapeComputer.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBOwnTLDef.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBOwnTLDef.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTLine.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBPTLine.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTLineCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBPTLineCont.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTPlatform.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBPTPlatform.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTStop.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBPTStop.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBPTStopCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBPTStopCont.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBParking.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBParking.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBRequest.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBRequest.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBSign.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBSign.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTrafficLightDefinition.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBTrafficLightDefinition.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTrafficLightLogic.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBTrafficLightLogic.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTrafficLightLogicCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBTrafficLightLogicCont.o"
  "/vagrant/sumo-1.0.0/src/netbuild/NBTypeCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/NBTypeCont.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
