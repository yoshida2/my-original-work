# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderArcView.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderArcView.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderDlrNavteq.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderDlrNavteq.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderOSM.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderOSM.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderVisum.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderVisum.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCLoaderXML.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCLoaderXML.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCNetProjectionLoader.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCNetProjectionLoader.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCPolyContainer.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCPolyContainer.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCTypeDefHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCTypeDefHandler.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/PCTypeMap.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/PCTypeMap.o"
  "/vagrant/sumo-1.0.0/src/polyconvert/polyconvert_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/CMakeFiles/polyconvert.dir/polyconvert_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
