# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Additional.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Additional.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Attribute.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Attribute.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Connection.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Connection.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Crossing.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Crossing.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Edge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Edge.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Junction.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Junction.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Lane.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Lane.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_Shape.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_Shape.o"
  "/vagrant/sumo-1.0.0/src/netedit/changes/GNEChange_TLS.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/changes/CMakeFiles/netedit_changes.dir/GNEChange_TLS.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
