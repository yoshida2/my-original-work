# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEConnection.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEConnection.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNECrossing.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNECrossing.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEEdge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEEdge.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEInternalLane.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEInternalLane.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEJunction.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEJunction.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNELane.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNELane.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNENetElement.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNENetElement.o"
  "/vagrant/sumo-1.0.0/src/netedit/netelements/GNEProhibition.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/netelements/CMakeFiles/netedit_netelements.dir/GNEProhibition.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
