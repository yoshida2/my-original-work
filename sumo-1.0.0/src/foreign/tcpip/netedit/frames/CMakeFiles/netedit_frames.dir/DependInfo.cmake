# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEAdditionalFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNEAdditionalFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEConnectorFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNEConnectorFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNECrossingFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNECrossingFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEDeleteFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNEDeleteFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNEFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEInspectorFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNEInspectorFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEPolygonFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNEPolygonFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNEProhibitionFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNEProhibitionFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNESelectorFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNESelectorFrame.o"
  "/vagrant/sumo-1.0.0/src/netedit/frames/GNETLSEditorFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/frames/CMakeFiles/netedit_frames.dir/GNETLSEditorFrame.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
