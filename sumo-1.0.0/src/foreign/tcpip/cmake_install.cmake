# Install script for directory: /vagrant/sumo-1.0.0/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/vagrant/sumo-1.0.0/src/foreign/tcpip/sumo")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo-gui" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo-gui")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo-gui"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/vagrant/sumo-1.0.0/src/foreign/tcpip/sumo-gui")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo-gui" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo-gui")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/sumo-gui")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/netconvert" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/netconvert")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/netconvert"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/vagrant/sumo-1.0.0/src/foreign/tcpip/netconvert")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/netconvert" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/netconvert")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/netconvert")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/od2trips" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/od2trips")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/od2trips"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/vagrant/sumo-1.0.0/src/foreign/tcpip/od2trips")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/od2trips" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/od2trips")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/od2trips")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/activitygen/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/duarouter/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/foreign/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/gui/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/guinetload/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/guisim/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/jtrrouter/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/marouter/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/mesogui/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/netedit/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/netgen/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/netload/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/od/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/osgview/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/polyconvert/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/router/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/tools/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/traci-server/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/cmake_install.cmake")
  include("/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/vagrant/sumo-1.0.0/src/foreign/tcpip/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
