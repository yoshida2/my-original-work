# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/marouter/ROMAAssignments.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/marouter/CMakeFiles/marouter.dir/ROMAAssignments.o"
  "/vagrant/sumo-1.0.0/src/marouter/ROMAEdge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/marouter/CMakeFiles/marouter.dir/ROMAEdge.o"
  "/vagrant/sumo-1.0.0/src/marouter/ROMAEdgeBuilder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/marouter/CMakeFiles/marouter.dir/ROMAEdgeBuilder.o"
  "/vagrant/sumo-1.0.0/src/marouter/ROMAFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/marouter/CMakeFiles/marouter.dir/ROMAFrame.o"
  "/vagrant/sumo-1.0.0/src/marouter/ROMARouteHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/marouter/CMakeFiles/marouter.dir/ROMARouteHandler.o"
  "/vagrant/sumo-1.0.0/src/marouter/marouter_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/marouter/CMakeFiles/marouter.dir/marouter_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/od/CMakeFiles/od.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
