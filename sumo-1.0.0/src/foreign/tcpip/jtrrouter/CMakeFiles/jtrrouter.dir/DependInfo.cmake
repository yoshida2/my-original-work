# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/jtrrouter/ROJTREdge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/jtrrouter/CMakeFiles/jtrrouter.dir/ROJTREdge.o"
  "/vagrant/sumo-1.0.0/src/jtrrouter/ROJTREdgeBuilder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/jtrrouter/CMakeFiles/jtrrouter.dir/ROJTREdgeBuilder.o"
  "/vagrant/sumo-1.0.0/src/jtrrouter/ROJTRFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/jtrrouter/CMakeFiles/jtrrouter.dir/ROJTRFrame.o"
  "/vagrant/sumo-1.0.0/src/jtrrouter/ROJTRRouter.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/jtrrouter/CMakeFiles/jtrrouter.dir/ROJTRRouter.o"
  "/vagrant/sumo-1.0.0/src/jtrrouter/ROJTRTurnDefLoader.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/jtrrouter/CMakeFiles/jtrrouter.dir/ROJTRTurnDefLoader.o"
  "/vagrant/sumo-1.0.0/src/jtrrouter/jtrrouter_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/jtrrouter/CMakeFiles/jtrrouter.dir/jtrrouter_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
