# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/libsumo/Edge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Edge.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Helper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Helper.o"
  "/vagrant/sumo-1.0.0/src/libsumo/InductionLoop.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/InductionLoop.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Junction.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Junction.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Lane.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Lane.o"
  "/vagrant/sumo-1.0.0/src/libsumo/LaneArea.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/LaneArea.o"
  "/vagrant/sumo-1.0.0/src/libsumo/MultiEntryExit.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/MultiEntryExit.o"
  "/vagrant/sumo-1.0.0/src/libsumo/POI.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/POI.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Person.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Person.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Polygon.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Polygon.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Route.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Route.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Simulation.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Simulation.o"
  "/vagrant/sumo-1.0.0/src/libsumo/TrafficLight.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/TrafficLight.o"
  "/vagrant/sumo-1.0.0/src/libsumo/Vehicle.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/Vehicle.o"
  "/vagrant/sumo-1.0.0/src/libsumo/VehicleType.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/VehicleType.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
