# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/microsim/MSMoveReminder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/MSMoveReminder.o"
  "/vagrant/sumo-1.0.0/src/microsim/MSNet.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/MSNet.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSE2Collector.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSE2Collector.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSE3Collector.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSE3Collector.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSInductLoop.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSInductLoop.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSMeanData_Net.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSMeanData_Net.o"
  "/vagrant/sumo-1.0.0/src/microsim/output/MSRouteProbe.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/microsim/output/MSRouteProbe.o"
  "/vagrant/sumo-1.0.0/src/netload/NLBuilder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/__/netload/NLBuilder.o"
  "/vagrant/sumo-1.0.0/src/traci_testclient/testlibsumo_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci_testclient/CMakeFiles/testlibsumo.dir/testlibsumo_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netload/CMakeFiles/netload.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/CMakeFiles/microsim.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/cfmodels/CMakeFiles/microsim_cfmodels.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/lcmodels/CMakeFiles/microsim_lcmodels.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/output/CMakeFiles/microsim_output.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/pedestrians/CMakeFiles/microsim_pedestrians.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/trigger/CMakeFiles/microsim_trigger.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/actions/CMakeFiles/microsim_actions.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/traffic_lights/CMakeFiles/microsim_traffic_lights.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim/CMakeFiles/mesosim.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/traci-server/CMakeFiles/traciserver.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/libsumo/CMakeFiles/libsumostatic.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
