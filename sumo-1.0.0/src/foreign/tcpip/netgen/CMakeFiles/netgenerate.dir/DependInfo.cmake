# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netgen/NGEdge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netgen/CMakeFiles/netgenerate.dir/NGEdge.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netgen/CMakeFiles/netgenerate.dir/NGFrame.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGNet.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netgen/CMakeFiles/netgenerate.dir/NGNet.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGNode.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netgen/CMakeFiles/netgenerate.dir/NGNode.o"
  "/vagrant/sumo-1.0.0/src/netgen/NGRandomNetBuilder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netgen/CMakeFiles/netgenerate.dir/NGRandomNetBuilder.o"
  "/vagrant/sumo-1.0.0/src/netgen/netgen_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netgen/CMakeFiles/netgenerate.dir/netgen_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
