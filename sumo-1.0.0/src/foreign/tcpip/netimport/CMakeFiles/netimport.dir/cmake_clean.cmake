file(REMOVE_RECURSE
  "typemap.h"
  "CMakeFiles/netimport.dir/NIFrame.o"
  "CMakeFiles/netimport.dir/NILoader.o"
  "CMakeFiles/netimport.dir/NIImporter_ArcView.o"
  "CMakeFiles/netimport.dir/NIImporter_DlrNavteq.o"
  "CMakeFiles/netimport.dir/NIImporter_ITSUMO.o"
  "CMakeFiles/netimport.dir/NIImporter_MATSim.o"
  "CMakeFiles/netimport.dir/NIImporter_OpenDrive.o"
  "CMakeFiles/netimport.dir/NIImporter_OpenStreetMap.o"
  "CMakeFiles/netimport.dir/NIImporter_RobocupRescue.o"
  "CMakeFiles/netimport.dir/NIImporter_SUMO.o"
  "CMakeFiles/netimport.dir/NIImporter_VISUM.o"
  "CMakeFiles/netimport.dir/NIVisumTL.o"
  "CMakeFiles/netimport.dir/NIXMLConnectionsHandler.o"
  "CMakeFiles/netimport.dir/NIXMLEdgesHandler.o"
  "CMakeFiles/netimport.dir/NIXMLNodesHandler.o"
  "CMakeFiles/netimport.dir/NIXMLTypesHandler.o"
  "CMakeFiles/netimport.dir/NIXMLPTHandler.o"
  "CMakeFiles/netimport.dir/NIXMLTrafficLightsHandler.o"
  "CMakeFiles/netimport.dir/NINavTeqHelper.o"
  "libnetimport.pdb"
  "libnetimport.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/netimport.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
