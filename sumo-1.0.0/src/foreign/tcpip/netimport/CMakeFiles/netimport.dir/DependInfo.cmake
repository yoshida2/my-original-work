# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netimport/NIFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIFrame.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_ArcView.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_ArcView.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_DlrNavteq.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_DlrNavteq.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_ITSUMO.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_ITSUMO.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_MATSim.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_MATSim.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_OpenDrive.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_OpenDrive.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_OpenStreetMap.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_OpenStreetMap.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_RobocupRescue.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_RobocupRescue.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_SUMO.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_SUMO.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIImporter_VISUM.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIImporter_VISUM.o"
  "/vagrant/sumo-1.0.0/src/netimport/NILoader.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NILoader.o"
  "/vagrant/sumo-1.0.0/src/netimport/NINavTeqHelper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NINavTeqHelper.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIVisumTL.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIVisumTL.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLConnectionsHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIXMLConnectionsHandler.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLEdgesHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIXMLEdgesHandler.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLNodesHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIXMLNodesHandler.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLPTHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIXMLPTHandler.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLTrafficLightsHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIXMLTrafficLightsHandler.o"
  "/vagrant/sumo-1.0.0/src/netimport/NIXMLTypesHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/NIXMLTypesHandler.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
