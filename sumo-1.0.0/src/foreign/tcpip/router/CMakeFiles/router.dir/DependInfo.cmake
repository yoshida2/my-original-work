# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/router/ROEdge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/ROEdge.o"
  "/vagrant/sumo-1.0.0/src/router/ROFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/ROFrame.o"
  "/vagrant/sumo-1.0.0/src/router/ROHelper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/ROHelper.o"
  "/vagrant/sumo-1.0.0/src/router/ROLoader.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/ROLoader.o"
  "/vagrant/sumo-1.0.0/src/router/RONet.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/RONet.o"
  "/vagrant/sumo-1.0.0/src/router/RONetHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/RONetHandler.o"
  "/vagrant/sumo-1.0.0/src/router/RONode.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/RONode.o"
  "/vagrant/sumo-1.0.0/src/router/ROPerson.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/ROPerson.o"
  "/vagrant/sumo-1.0.0/src/router/RORoute.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/RORoute.o"
  "/vagrant/sumo-1.0.0/src/router/RORouteDef.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/RORouteDef.o"
  "/vagrant/sumo-1.0.0/src/router/RORouteHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/RORouteHandler.o"
  "/vagrant/sumo-1.0.0/src/router/ROVehicle.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/ROVehicle.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
