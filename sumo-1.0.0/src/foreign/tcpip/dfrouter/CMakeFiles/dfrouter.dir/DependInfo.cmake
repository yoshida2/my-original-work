# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetFlowLoader.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFDetFlowLoader.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetector.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFDetector.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetectorFlow.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFDetectorFlow.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFDetectorHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFDetectorHandler.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFEdge.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFEdge.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFEdgeBuilder.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFEdgeBuilder.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFFrame.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFNet.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFNet.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/RODFRouteCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/RODFRouteCont.o"
  "/vagrant/sumo-1.0.0/src/dfrouter/dfrouter_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/dfrouter/CMakeFiles/dfrouter.dir/dfrouter_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/router/CMakeFiles/router.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
