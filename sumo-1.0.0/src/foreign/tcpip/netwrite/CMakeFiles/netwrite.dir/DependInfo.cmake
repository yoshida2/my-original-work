# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netwrite/NWFrame.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/NWFrame.o"
  "/vagrant/sumo-1.0.0/src/netwrite/NWWriter_Amitran.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/NWWriter_Amitran.o"
  "/vagrant/sumo-1.0.0/src/netwrite/NWWriter_DlrNavteq.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/NWWriter_DlrNavteq.o"
  "/vagrant/sumo-1.0.0/src/netwrite/NWWriter_MATSim.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/NWWriter_MATSim.o"
  "/vagrant/sumo-1.0.0/src/netwrite/NWWriter_OpenDrive.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/NWWriter_OpenDrive.o"
  "/vagrant/sumo-1.0.0/src/netwrite/NWWriter_SUMO.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/NWWriter_SUMO.o"
  "/vagrant/sumo-1.0.0/src/netwrite/NWWriter_XML.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/NWWriter_XML.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
