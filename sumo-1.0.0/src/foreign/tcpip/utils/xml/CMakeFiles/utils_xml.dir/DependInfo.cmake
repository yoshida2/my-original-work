# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/xml/GenericSAXHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/GenericSAXHandler.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SAXWeightsHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SAXWeightsHandler.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMORouteHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMORouteHandler.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMORouteLoader.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMORouteLoader.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMORouteLoaderControl.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMORouteLoaderControl.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributes.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributes.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributesImpl_Binary.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributesImpl_Binary.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributesImpl_Cached.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributesImpl_Cached.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXAttributesImpl_Xerces.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXAttributesImpl_Xerces.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXHandler.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOSAXReader.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOSAXReader.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOVehicleParserHelper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOVehicleParserHelper.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/SUMOXMLDefinitions.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/SUMOXMLDefinitions.o"
  "/vagrant/sumo-1.0.0/src/utils/xml/XMLSubSys.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/xml/CMakeFiles/utils_xml.dir/XMLSubSys.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
