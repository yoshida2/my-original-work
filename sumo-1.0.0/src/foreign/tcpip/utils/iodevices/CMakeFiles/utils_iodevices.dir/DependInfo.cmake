# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/iodevices/BinaryFormatter.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/BinaryFormatter.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/BinaryInputDevice.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/BinaryInputDevice.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/OutputDevice.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/OutputDevice.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/OutputDevice_CERR.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/OutputDevice_CERR.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/OutputDevice_COUT.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/OutputDevice_COUT.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/OutputDevice_File.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/OutputDevice_File.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/OutputDevice_Network.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/OutputDevice_Network.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/OutputDevice_String.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/OutputDevice_String.o"
  "/vagrant/sumo-1.0.0/src/utils/iodevices/PlainXMLFormatter.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/iodevices/CMakeFiles/utils_iodevices.dir/PlainXMLFormatter.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
