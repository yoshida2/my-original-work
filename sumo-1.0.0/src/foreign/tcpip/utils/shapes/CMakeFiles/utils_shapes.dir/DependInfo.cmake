# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/shapes/SUMOPolygon.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/shapes/CMakeFiles/utils_shapes.dir/SUMOPolygon.o"
  "/vagrant/sumo-1.0.0/src/utils/shapes/Shape.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/shapes/CMakeFiles/utils_shapes.dir/Shape.o"
  "/vagrant/sumo-1.0.0/src/utils/shapes/ShapeContainer.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/shapes/CMakeFiles/utils_shapes.dir/ShapeContainer.o"
  "/vagrant/sumo-1.0.0/src/utils/shapes/ShapeHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/shapes/CMakeFiles/utils_shapes.dir/ShapeHandler.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
