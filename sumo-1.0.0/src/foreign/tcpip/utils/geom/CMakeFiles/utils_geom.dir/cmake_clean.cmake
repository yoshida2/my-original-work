file(REMOVE_RECURSE
  "CMakeFiles/utils_geom.dir/Boundary.o"
  "CMakeFiles/utils_geom.dir/Bresenham.o"
  "CMakeFiles/utils_geom.dir/GeomConvHelper.o"
  "CMakeFiles/utils_geom.dir/GeoConvHelper.o"
  "CMakeFiles/utils_geom.dir/GeomHelper.o"
  "CMakeFiles/utils_geom.dir/Position.o"
  "CMakeFiles/utils_geom.dir/PositionVector.o"
  "CMakeFiles/utils_geom.dir/bezier.o"
  "libutils_geom.pdb"
  "libutils_geom.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/utils_geom.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
