# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/utils/geom/Boundary.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/Boundary.o"
  "/vagrant/sumo-1.0.0/src/utils/geom/Bresenham.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/Bresenham.o"
  "/vagrant/sumo-1.0.0/src/utils/geom/GeoConvHelper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/GeoConvHelper.o"
  "/vagrant/sumo-1.0.0/src/utils/geom/GeomConvHelper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/GeomConvHelper.o"
  "/vagrant/sumo-1.0.0/src/utils/geom/GeomHelper.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/GeomHelper.o"
  "/vagrant/sumo-1.0.0/src/utils/geom/Position.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/Position.o"
  "/vagrant/sumo-1.0.0/src/utils/geom/PositionVector.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/PositionVector.o"
  "/vagrant/sumo-1.0.0/src/utils/geom/bezier.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/geom/CMakeFiles/utils_geom.dir/bezier.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
