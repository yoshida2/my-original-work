# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/od/ODAmitranHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/od/CMakeFiles/od.dir/ODAmitranHandler.o"
  "/vagrant/sumo-1.0.0/src/od/ODDistrict.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/od/CMakeFiles/od.dir/ODDistrict.o"
  "/vagrant/sumo-1.0.0/src/od/ODDistrictCont.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/od/CMakeFiles/od.dir/ODDistrictCont.o"
  "/vagrant/sumo-1.0.0/src/od/ODDistrictHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/od/CMakeFiles/od.dir/ODDistrictHandler.o"
  "/vagrant/sumo-1.0.0/src/od/ODMatrix.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/od/CMakeFiles/od.dir/ODMatrix.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
