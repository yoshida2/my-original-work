# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /vagrant/sumo-1.0.0/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /vagrant/sumo-1.0.0/src/foreign/tcpip

# Include any dependencies generated for this target.
include mesosim/CMakeFiles/mesosim.dir/depend.make

# Include the progress variables for this target.
include mesosim/CMakeFiles/mesosim.dir/progress.make

# Include the compile flags for this target's objects.
include mesosim/CMakeFiles/mesosim.dir/flags.make

mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o: mesosim/CMakeFiles/mesosim.dir/flags.make
mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o: ../../mesosim/MEInductLoop.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/mesosim.dir/MEInductLoop.o -c /vagrant/sumo-1.0.0/src/mesosim/MEInductLoop.cpp

mesosim/CMakeFiles/mesosim.dir/MEInductLoop.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mesosim.dir/MEInductLoop.i"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /vagrant/sumo-1.0.0/src/mesosim/MEInductLoop.cpp > CMakeFiles/mesosim.dir/MEInductLoop.i

mesosim/CMakeFiles/mesosim.dir/MEInductLoop.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mesosim.dir/MEInductLoop.s"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /vagrant/sumo-1.0.0/src/mesosim/MEInductLoop.cpp -o CMakeFiles/mesosim.dir/MEInductLoop.s

mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.requires:
.PHONY : mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.requires

mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.provides: mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.requires
	$(MAKE) -f mesosim/CMakeFiles/mesosim.dir/build.make mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.provides.build
.PHONY : mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.provides

mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.provides.build: mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o

mesosim/CMakeFiles/mesosim.dir/MELoop.o: mesosim/CMakeFiles/mesosim.dir/flags.make
mesosim/CMakeFiles/mesosim.dir/MELoop.o: ../../mesosim/MELoop.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object mesosim/CMakeFiles/mesosim.dir/MELoop.o"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/mesosim.dir/MELoop.o -c /vagrant/sumo-1.0.0/src/mesosim/MELoop.cpp

mesosim/CMakeFiles/mesosim.dir/MELoop.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mesosim.dir/MELoop.i"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /vagrant/sumo-1.0.0/src/mesosim/MELoop.cpp > CMakeFiles/mesosim.dir/MELoop.i

mesosim/CMakeFiles/mesosim.dir/MELoop.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mesosim.dir/MELoop.s"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /vagrant/sumo-1.0.0/src/mesosim/MELoop.cpp -o CMakeFiles/mesosim.dir/MELoop.s

mesosim/CMakeFiles/mesosim.dir/MELoop.o.requires:
.PHONY : mesosim/CMakeFiles/mesosim.dir/MELoop.o.requires

mesosim/CMakeFiles/mesosim.dir/MELoop.o.provides: mesosim/CMakeFiles/mesosim.dir/MELoop.o.requires
	$(MAKE) -f mesosim/CMakeFiles/mesosim.dir/build.make mesosim/CMakeFiles/mesosim.dir/MELoop.o.provides.build
.PHONY : mesosim/CMakeFiles/mesosim.dir/MELoop.o.provides

mesosim/CMakeFiles/mesosim.dir/MELoop.o.provides.build: mesosim/CMakeFiles/mesosim.dir/MELoop.o

mesosim/CMakeFiles/mesosim.dir/MESegment.o: mesosim/CMakeFiles/mesosim.dir/flags.make
mesosim/CMakeFiles/mesosim.dir/MESegment.o: ../../mesosim/MESegment.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object mesosim/CMakeFiles/mesosim.dir/MESegment.o"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/mesosim.dir/MESegment.o -c /vagrant/sumo-1.0.0/src/mesosim/MESegment.cpp

mesosim/CMakeFiles/mesosim.dir/MESegment.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mesosim.dir/MESegment.i"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /vagrant/sumo-1.0.0/src/mesosim/MESegment.cpp > CMakeFiles/mesosim.dir/MESegment.i

mesosim/CMakeFiles/mesosim.dir/MESegment.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mesosim.dir/MESegment.s"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /vagrant/sumo-1.0.0/src/mesosim/MESegment.cpp -o CMakeFiles/mesosim.dir/MESegment.s

mesosim/CMakeFiles/mesosim.dir/MESegment.o.requires:
.PHONY : mesosim/CMakeFiles/mesosim.dir/MESegment.o.requires

mesosim/CMakeFiles/mesosim.dir/MESegment.o.provides: mesosim/CMakeFiles/mesosim.dir/MESegment.o.requires
	$(MAKE) -f mesosim/CMakeFiles/mesosim.dir/build.make mesosim/CMakeFiles/mesosim.dir/MESegment.o.provides.build
.PHONY : mesosim/CMakeFiles/mesosim.dir/MESegment.o.provides

mesosim/CMakeFiles/mesosim.dir/MESegment.o.provides.build: mesosim/CMakeFiles/mesosim.dir/MESegment.o

mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o: mesosim/CMakeFiles/mesosim.dir/flags.make
mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o: ../../mesosim/METriggeredCalibrator.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/mesosim.dir/METriggeredCalibrator.o -c /vagrant/sumo-1.0.0/src/mesosim/METriggeredCalibrator.cpp

mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mesosim.dir/METriggeredCalibrator.i"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /vagrant/sumo-1.0.0/src/mesosim/METriggeredCalibrator.cpp > CMakeFiles/mesosim.dir/METriggeredCalibrator.i

mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mesosim.dir/METriggeredCalibrator.s"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /vagrant/sumo-1.0.0/src/mesosim/METriggeredCalibrator.cpp -o CMakeFiles/mesosim.dir/METriggeredCalibrator.s

mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.requires:
.PHONY : mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.requires

mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.provides: mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.requires
	$(MAKE) -f mesosim/CMakeFiles/mesosim.dir/build.make mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.provides.build
.PHONY : mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.provides

mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.provides.build: mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o

mesosim/CMakeFiles/mesosim.dir/MEVehicle.o: mesosim/CMakeFiles/mesosim.dir/flags.make
mesosim/CMakeFiles/mesosim.dir/MEVehicle.o: ../../mesosim/MEVehicle.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object mesosim/CMakeFiles/mesosim.dir/MEVehicle.o"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/mesosim.dir/MEVehicle.o -c /vagrant/sumo-1.0.0/src/mesosim/MEVehicle.cpp

mesosim/CMakeFiles/mesosim.dir/MEVehicle.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mesosim.dir/MEVehicle.i"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /vagrant/sumo-1.0.0/src/mesosim/MEVehicle.cpp > CMakeFiles/mesosim.dir/MEVehicle.i

mesosim/CMakeFiles/mesosim.dir/MEVehicle.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mesosim.dir/MEVehicle.s"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /vagrant/sumo-1.0.0/src/mesosim/MEVehicle.cpp -o CMakeFiles/mesosim.dir/MEVehicle.s

mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.requires:
.PHONY : mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.requires

mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.provides: mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.requires
	$(MAKE) -f mesosim/CMakeFiles/mesosim.dir/build.make mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.provides.build
.PHONY : mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.provides

mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.provides.build: mesosim/CMakeFiles/mesosim.dir/MEVehicle.o

mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o: mesosim/CMakeFiles/mesosim.dir/flags.make
mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o: ../../mesosim/MEVehicleControl.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles $(CMAKE_PROGRESS_6)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/mesosim.dir/MEVehicleControl.o -c /vagrant/sumo-1.0.0/src/mesosim/MEVehicleControl.cpp

mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mesosim.dir/MEVehicleControl.i"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /vagrant/sumo-1.0.0/src/mesosim/MEVehicleControl.cpp > CMakeFiles/mesosim.dir/MEVehicleControl.i

mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mesosim.dir/MEVehicleControl.s"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /vagrant/sumo-1.0.0/src/mesosim/MEVehicleControl.cpp -o CMakeFiles/mesosim.dir/MEVehicleControl.s

mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.requires:
.PHONY : mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.requires

mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.provides: mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.requires
	$(MAKE) -f mesosim/CMakeFiles/mesosim.dir/build.make mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.provides.build
.PHONY : mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.provides

mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.provides.build: mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o

# Object files for target mesosim
mesosim_OBJECTS = \
"CMakeFiles/mesosim.dir/MEInductLoop.o" \
"CMakeFiles/mesosim.dir/MELoop.o" \
"CMakeFiles/mesosim.dir/MESegment.o" \
"CMakeFiles/mesosim.dir/METriggeredCalibrator.o" \
"CMakeFiles/mesosim.dir/MEVehicle.o" \
"CMakeFiles/mesosim.dir/MEVehicleControl.o"

# External object files for target mesosim
mesosim_EXTERNAL_OBJECTS =

mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o
mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/MELoop.o
mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/MESegment.o
mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o
mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/MEVehicle.o
mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o
mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/build.make
mesosim/libmesosim.a: mesosim/CMakeFiles/mesosim.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX static library libmesosim.a"
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && $(CMAKE_COMMAND) -P CMakeFiles/mesosim.dir/cmake_clean_target.cmake
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/mesosim.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
mesosim/CMakeFiles/mesosim.dir/build: mesosim/libmesosim.a
.PHONY : mesosim/CMakeFiles/mesosim.dir/build

mesosim/CMakeFiles/mesosim.dir/requires: mesosim/CMakeFiles/mesosim.dir/MEInductLoop.o.requires
mesosim/CMakeFiles/mesosim.dir/requires: mesosim/CMakeFiles/mesosim.dir/MELoop.o.requires
mesosim/CMakeFiles/mesosim.dir/requires: mesosim/CMakeFiles/mesosim.dir/MESegment.o.requires
mesosim/CMakeFiles/mesosim.dir/requires: mesosim/CMakeFiles/mesosim.dir/METriggeredCalibrator.o.requires
mesosim/CMakeFiles/mesosim.dir/requires: mesosim/CMakeFiles/mesosim.dir/MEVehicle.o.requires
mesosim/CMakeFiles/mesosim.dir/requires: mesosim/CMakeFiles/mesosim.dir/MEVehicleControl.o.requires
.PHONY : mesosim/CMakeFiles/mesosim.dir/requires

mesosim/CMakeFiles/mesosim.dir/clean:
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim && $(CMAKE_COMMAND) -P CMakeFiles/mesosim.dir/cmake_clean.cmake
.PHONY : mesosim/CMakeFiles/mesosim.dir/clean

mesosim/CMakeFiles/mesosim.dir/depend:
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /vagrant/sumo-1.0.0/src /vagrant/sumo-1.0.0/src/mesosim /vagrant/sumo-1.0.0/src/foreign/tcpip /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim /vagrant/sumo-1.0.0/src/foreign/tcpip/mesosim/CMakeFiles/mesosim.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : mesosim/CMakeFiles/mesosim.dir/depend

