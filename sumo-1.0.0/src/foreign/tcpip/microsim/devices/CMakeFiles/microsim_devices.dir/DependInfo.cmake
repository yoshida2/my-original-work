# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_BTreceiver.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_BTreceiver.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_BTsender.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_BTsender.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Battery.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Battery.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Bluelight.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Bluelight.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_DriverState.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_DriverState.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Emissions.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Emissions.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Example.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Example.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_FCD.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_FCD.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Routing.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Routing.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_SSM.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_SSM.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_ToC.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_ToC.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Transportable.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Transportable.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Tripinfo.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Tripinfo.o"
  "/vagrant/sumo-1.0.0/src/microsim/devices/MSDevice_Vehroutes.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/microsim/devices/CMakeFiles/microsim_devices.dir/MSDevice_Vehroutes.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
