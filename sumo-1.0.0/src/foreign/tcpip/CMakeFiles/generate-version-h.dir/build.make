# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /vagrant/sumo-1.0.0/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /vagrant/sumo-1.0.0/src/foreign/tcpip

# Utility rule file for generate-version-h.

# Include the progress variables for this target.
include CMakeFiles/generate-version-h.dir/progress.make

CMakeFiles/generate-version-h: src/version.h

src/version.h:
	$(CMAKE_COMMAND) -E cmake_progress_report /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating src/version.h"
	../../tools/build/version.py /vagrant/sumo-1.0.0/src/foreign/tcpip/src

generate-version-h: CMakeFiles/generate-version-h
generate-version-h: src/version.h
generate-version-h: CMakeFiles/generate-version-h.dir/build.make
.PHONY : generate-version-h

# Rule to build all files generated by this target.
CMakeFiles/generate-version-h.dir/build: generate-version-h
.PHONY : CMakeFiles/generate-version-h.dir/build

CMakeFiles/generate-version-h.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/generate-version-h.dir/cmake_clean.cmake
.PHONY : CMakeFiles/generate-version-h.dir/clean

CMakeFiles/generate-version-h.dir/depend:
	cd /vagrant/sumo-1.0.0/src/foreign/tcpip && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /vagrant/sumo-1.0.0/src /vagrant/sumo-1.0.0/src /vagrant/sumo-1.0.0/src/foreign/tcpip /vagrant/sumo-1.0.0/src/foreign/tcpip /vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles/generate-version-h.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/generate-version-h.dir/depend

