# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/netconvert_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/CMakeFiles/netconvert.dir/netconvert_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netwrite/CMakeFiles/netwrite.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/CMakeFiles/netimport.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netbuild/CMakeFiles/netbuild.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/vissim/CMakeFiles/netimport_vissim.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/vissim/typeloader/CMakeFiles/netimport_vissim_typeloader.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/netimport/vissim/tempstructs/CMakeFiles/netimport_vissim_tempstructs.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
