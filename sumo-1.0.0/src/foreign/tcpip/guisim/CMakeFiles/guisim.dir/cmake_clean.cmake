file(REMOVE_RECURSE
  "CMakeFiles/guisim.dir/GUIBusStop.o"
  "CMakeFiles/guisim.dir/GUIContainer.o"
  "CMakeFiles/guisim.dir/GUIContainerStop.o"
  "CMakeFiles/guisim.dir/GUIParkingArea.o"
  "CMakeFiles/guisim.dir/GUIDetectorWrapper.o"
  "CMakeFiles/guisim.dir/GUIE2Collector.o"
  "CMakeFiles/guisim.dir/GUIE3Collector.o"
  "CMakeFiles/guisim.dir/GUIEdge.o"
  "CMakeFiles/guisim.dir/GUIEventControl.o"
  "CMakeFiles/guisim.dir/GUIInductLoop.o"
  "CMakeFiles/guisim.dir/GUIInstantInductLoop.o"
  "CMakeFiles/guisim.dir/GUIJunctionWrapper.o"
  "CMakeFiles/guisim.dir/GUILane.o"
  "CMakeFiles/guisim.dir/GUILaneSpeedTrigger.o"
  "CMakeFiles/guisim.dir/GUIPerson.o"
  "CMakeFiles/guisim.dir/GUINet.o"
  "CMakeFiles/guisim.dir/GUITrafficLightLogicWrapper.o"
  "CMakeFiles/guisim.dir/GUITriggeredRerouter.o"
  "CMakeFiles/guisim.dir/GUICalibrator.o"
  "CMakeFiles/guisim.dir/GUIChargingStation.o"
  "CMakeFiles/guisim.dir/GUIBaseVehicle.o"
  "CMakeFiles/guisim.dir/GUIVehicle.o"
  "CMakeFiles/guisim.dir/GUIVehicleControl.o"
  "CMakeFiles/guisim.dir/GUITransportableControl.o"
  "libguisim.pdb"
  "libguisim.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/guisim.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
