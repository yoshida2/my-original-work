# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/vagrant/sumo-1.0.0/src/tools/TrajectoriesHandler.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/tools/CMakeFiles/emissionsDrivingCycle.dir/TrajectoriesHandler.o"
  "/vagrant/sumo-1.0.0/src/tools/emissionsDrivingCycle_main.cpp" "/vagrant/sumo-1.0.0/src/foreign/tcpip/tools/CMakeFiles/emissionsDrivingCycle.dir/emissionsDrivingCycle_main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/utils/emissions/CMakeFiles/utils_emissions.dir/DependInfo.cmake"
  "/vagrant/sumo-1.0.0/src/foreign/tcpip/foreign/PHEMlight/cpp/CMakeFiles/foreign_phemlight.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
