file(REMOVE_RECURSE
  "CMakeFiles/traciserver.dir/TraCIServer.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Edge.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_InductionLoop.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Junction.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Lane.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_MultiEntryExit.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_LaneArea.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Person.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_POI.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Polygon.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Route.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Simulation.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_TrafficLight.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_Vehicle.o"
  "CMakeFiles/traciserver.dir/TraCIServerAPI_VehicleType.o"
  "libtraciserver.pdb"
  "libtraciserver.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/traciserver.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
