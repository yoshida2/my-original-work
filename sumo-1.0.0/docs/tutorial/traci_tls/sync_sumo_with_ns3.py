# coding: UTF-8

#!/usr/bin/env python
# Eclipse SUMO, Simulation of Urban MObility; see https://eclipse.org/sumo
# Copyright (C) 2009-2018 German Aerospace Center (DLR) and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# SPDX-License-Identifier: EPL-2.0

# @file    runner.py
# @author  Lena Kalleske
# @author  Daniel Krajzewicz
# @author  Michael Behrisch
# @author  Jakob Erdmann
# @date    2009-03-26
# @version $Id$

from __future__ import absolute_import
from __future__ import print_function

# default modules
import os
import sys
import optparse
import random
import socket

# class -------------------------------------------------------

# class API:
#     host = "localhost" #お使いのサーバーのホスト名を入れます
#     port = 8080 #クライアントと同じPORTをしてあげます
#
#     serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     serversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#     serversock.bind((host,port)) #IPとPORTを指定してバインドします
#     serversock.listen(10) #接続の待ち受けをします（キューの最大数を指定）
#
#     print ('Waiting for connections...')
#     clientsock, client_address = serversock.accept() #接続されればデータを格納
#
#     def __init__(self):
#         print('コンストラクタが呼ばれました')
#
#     def __del__(self):
#         print('デストラクタが呼ばれました')
#
#         clientsock.close()
#         clientsock.sendall(s_msg) #メッセージを返します
#
#     def server():
#         while True:
#             rcvmsg = self.clientsock.recv(1024)
#             # api do its own task by the received messages
#             if rcvmsg == 'Hello from client':
#                 print_message(rcvmsg)
#             else:
#                 break
#
#     def print_message(rcvmsg):
#         print ('Received -> %s' % (rcvmsg))
#
#     def step_simulation():
#         traci.simulationStep()
#         print (traci.simulation.getTime())
#         print ('Type message...')
#         print ('Wait...')
#         clientsock.sendall(s_msg) #メッセージを返します



# main -------------------------------------------------------

# we need to import python modules from the $SUMO_HOME/tools directory
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa


def generate_routefile():
    random.seed(42)  # make tests reproducible
    N = 3600  # number of time steps
    # demand per second from different directions
    pWE = 1. / 10
    pEW = 1. / 11
    pNS = 1. / 30
    with open("data/cross.rou.xml", "w") as routes:
        print("""<routes>
        <vType id="typeWE" accel="0.8" decel="4.5" sigma="0.5" length="5" minGap="2.5" maxSpeed="16.67" \
guiShape="passenger"/>
        <vType id="typeNS" accel="0.8" decel="4.5" sigma="0.5" length="7" minGap="3" maxSpeed="25" guiShape="bus"/>

        <route id="right" edges="51o 1i 2o 52i" />
        <route id="left" edges="52o 2i 1o 51i" />
        <route id="down" edges="54o 4i 3o 53i" />""", file=routes)
        vehNr = 0
        for i in range(N):
            if random.uniform(0, 1) < pWE:
                print('    <vehicle id="right_%i" type="typeWE" route="right" depart="%i" />' % (
                    vehNr, i), file=routes)
                vehNr += 1
            if random.uniform(0, 1) < pEW:
                print('    <vehicle id="left_%i" type="typeWE" route="left" depart="%i" />' % (
                    vehNr, i), file=routes)
                vehNr += 1
            if random.uniform(0, 1) < pNS:
                print('    <vehicle id="down_%i" type="typeNS" route="down" depart="%i" color="1,0,0"/>' % (
                    vehNr, i), file=routes)
                vehNr += 1
        print("</routes>", file=routes)

# The program looks like this
#    <tlLogic id="0" type="static" programID="0" offset="0">
# the locations of the tls are      NESW
#        <phase duration="31" state="GrGr"/>
#        <phase duration="6"  state="yryr"/>
#        <phase duration="31" state="rGrG"/>
#        <phase duration="6"  state="ryry"/>
#    </tlLogic>


def run():
    """execute the TraCI control loop"""
    step = 0
    # we start with phase 2 where EW has green
    traci.trafficlight.setPhase("0", 2)
    while traci.simulation.getMinExpectedNumber() > 0:
        traci.simulationStep()
        if traci.trafficlight.getPhase("0") == 2:
            # we are not already switching
            if traci.inductionloop.getLastStepVehicleNumber("0") > 0:
                # there is a vehicle from the north, switch
                traci.trafficlight.setPhase("0", 3)
            else:
                # otherwise try to keep green for EW
                traci.trafficlight.setPhase("0", 2)
        step += 1
    traci.close()
    sys.stdout.flush()


def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


# this is the main entry point of this script
if __name__ == "__main__":
    options = get_options()

    # this script has been called from the command line. It will start sumo as a
    # server, then connect and run
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo-gui')

    # first, generate the route file for this simulation
    generate_routefile()

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    traci.start([sumoBinary, "-c", "data/cross.sumocfg",
                             "--tripinfo-output", "tripinfo.xml"])


    host = "localhost" #お使いのサーバーのホスト名を入れます
    port = 8080 #クライアントと同じPORTをしてあげます

    serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversock.bind((host,port)) #IPとPORTを指定してバインドします
    serversock.listen(10) #接続の待ち受けをします（キューの最大数を指定）

    print ('Waiting for connections...')
    clientsock, client_address = serversock.accept() #接続されればデータを格納

    while True:
        rcvmsg = clientsock.recv(1024)
        # api do its own task by the received messages

        print (rcvmsg)
        if 'Hello from client' in rcvmsg:
            print ('Received -> %s' % (rcvmsg))
            s_msg = "server replied"
            clientsock.sendall(s_msg) #メッセージを返します
        elif 'next step' in rcvmsg:
            traci.simulationStep()
            print (traci.simulation.getTime())
            s_msg = "next step"
            clientsock.sendall(s_msg) #メッセージを返します
        elif 'loaded vehicles' in rcvmsg:
            # s_msg = traci.simulation.getLoadedIDList()
            s_msg = traci.simulation.getDepartedIDList()
            print (len(s_msg))
            if len(s_msg) != 0:
                s_msg = "".join(map(lambda x: x + ',', s_msg))
                s_msg = s_msg[:-1]
            else:
                s_msg = "NO_LOADED_VEHICLE"
            print (s_msg)
            clientsock.sendall(s_msg) #メッセージを返します
        elif 'get vehicle speed' in rcvmsg:
            try:
                message = rcvmsg.split(',')
                vehicle_ID = message[1]
                # print (str(traci.vehicle.getSpeed( vehicle_ID )))
                clientsock.sendall( str(traci.vehicle.getSpeed( vehicle_ID )) )
            except Exception:
                clientsock.sendall( "NO_EXISTANCE" )
        elif 'get vehicle accele' in rcvmsg:
            message = rcvmsg.split(',')
            vehicle_ID = message[1]
            clientsock.sendall( str(traci.vehicle.getAccel( vehicle_ID )) )
        elif 'get vehicle position' in rcvmsg:
            message = rcvmsg.split(',')
            vehicle_ID = message[1]
            x, y = traci.vehicle.getPosition( vehicle_ID )
            s_msg = str(x) + "," + str(y)
            clientsock.sendall( s_msg )
        elif 'update vehicle speed':
            message = rcvmsg.split(',')
            vehicle_ID = message[1]
            traci.vehicle.setSpeed( vehicle_ID, float(message[2]) )
            clientsock.sendall( 'update vehicle speed' )
        else:
            break

    print ('connection closed')
    clientsock.close()
    # clientsock.sendall(s_msg) #メッセージを返します

    # run()
